package com.algo.expert.classes.recursion;

import java.util.*;

public class PowerSetStrings {
    public static void main(String[] args) {
        String str = "kickstart";
        System.out.println(get_distinct_subsets(str).size());
    }

    public static List<String> get_distinct_subsets(String str) {
        LinkedHashSet<String> temp = new LinkedHashSet<>();
        char[] c = str.toCharArray();
        Arrays.sort(c);
        helper(c, 0, new ArrayList<>(), temp);
        return new ArrayList<>(temp);
    }

    private static void helper(char[] data, int idx, List<String> slate, LinkedHashSet<String> temp) {
        if (data.length == idx) {
            temp.add(String.join("", slate));
            return;
        }

        helper(data, idx + 1, slate, temp);
        slate.add(String.valueOf(data[idx]));
        helper(data, idx + 1, slate, temp);
        slate.remove(slate.size() - 1);
    }
}
