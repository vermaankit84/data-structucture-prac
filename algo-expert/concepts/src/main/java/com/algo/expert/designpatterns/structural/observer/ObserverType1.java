package com.algo.expert.designpatterns.structural.observer;

public class ObserverType1 implements Observer {

    private final String name;
    public ObserverType1(final String name) {
        this.name = name;
    }

    @Override
    public void update(int updatedValue) {
        System.out.println(this.name  + " has received an alert :  updated my value in subject is " + updatedValue);
    }
}
