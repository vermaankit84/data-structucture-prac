package com.algo.expert.classes.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class NodeDepth {
    public static void main(String[] args) {
        TreeNode root = TreeUtils.buildTree();
        System.out.println(nodeDepths(root));
    }

    public static int nodeDepths(TreeNode root) {
        Queue<BinaryLevel> queue = new LinkedList<>();

        int currentDepth = 0;
        queue.add(new BinaryLevel(root, currentDepth));
        List<Integer> list = new ArrayList<>();
        while (!queue.isEmpty()) {
            BinaryLevel curr_level = queue.poll();
            currentDepth = curr_level.level;
            TreeNode curr_node = curr_level.node;
            list.add(currentDepth);
            if (curr_node.getLeft()!=null) {
                queue.add(new BinaryLevel( curr_node.getLeft(), currentDepth + 1));
            }

            if (curr_node.getRight()!=null) {
                queue.add(new BinaryLevel( curr_node.getRight(), currentDepth + 1));
            }

        }

        return list.stream().mapToInt(Integer::intValue).sum();
    }


    static class BinaryLevel {
        int level;
        TreeNode node;
        BinaryLevel(TreeNode node, int level ) {
            this.level = level;
            this.node = node;
        }
    }
}
