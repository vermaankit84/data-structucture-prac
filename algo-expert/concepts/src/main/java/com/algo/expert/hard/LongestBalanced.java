package com.algo.expert.hard;

import java.util.Stack;

public class LongestBalanced {
    public static void main(String[] args) {

    }

    public int longestBalancedSubstring(String string) {
        int maxLength  = 0;
        Stack<Integer> idStack = new Stack<>();
        idStack.add(-1);
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '(') {
                idStack.push(i);
            } else  {
                idStack.pop();
                if (idStack.size() == 0) {
                    idStack.push(i);
                } else {
                    int balanceIdx = idStack.peek();
                    int currentLength = i - balanceIdx;
                    maxLength = Math.max(currentLength, maxLength);
                }
            }
        }
        return maxLength;
    }
}
