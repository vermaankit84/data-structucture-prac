package com.algo.expert.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ValidIPAddress {

    public static void main(String[] args) {
        System.out.println(new ValidIPAddress().validIPAddresses("1921680"));
    }

    public ArrayList<String> validIPAddresses(String string) {
        ArrayList<String> response = new ArrayList<>();
        for (int i = 1; i < Math.min(string.length(),4); i++) {
            String[] current_ip_address = new String[] {"","","",""};
            current_ip_address[0] = string.substring(0, i);
            if (!is_valid_ip_address(current_ip_address[0])) continue;

            for (int j = i + 1; j < i + Math.min(string.length() - i, 4); j = j + 1) {
                current_ip_address[1] = string.substring(i, j);
                if (!is_valid_ip_address(current_ip_address[1])) continue;

                for (int k = j + 1; k < j + Math.min(string.length() - j, 4); k++) {
                    current_ip_address[2] = string.substring(j, k);
                    current_ip_address[3] = string.substring(k);
                    if (is_valid_ip_address(current_ip_address[2]) && is_valid_ip_address(current_ip_address[3])) {
                        response.add(String.join(".", current_ip_address));
                    }
                }
            }
        }
        return response;
    }


    private boolean is_valid_ip_address(String str) {
        //if (str.isEmpty()) return false;
        int data = Integer.parseInt(str);
        if (data > 255) return false;

        return str.length() == Integer.toString(data).length();
    }
}
