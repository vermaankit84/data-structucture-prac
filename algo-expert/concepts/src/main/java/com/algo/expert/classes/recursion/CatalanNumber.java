package com.algo.expert.classes.recursion;

public class CatalanNumber {
    public static void main(String[] args) {
        System.out.println(how_many_BSTs(1));
        System.out.println(how_many_BSTs(2));
        System.out.println(how_many_BSTs(3));
        System.out.println(how_many_BSTs(4));
        System.out.println(how_many_BSTs(7));
        System.out.println(how_many_BSTs(11));
    }

    static long how_many_BSTs(int n) {
       int dp[] = new int[n + 1];
       dp[0] = 1;
       dp[1] = 1;

        for (int i = 2; i < n + 1; i++) {
            for(int j = 1; j < i + 1; j = j + 1) {
                dp[i] = dp[i] + dp[j - 1] * dp[i - j];
            }
        }

       return dp[n];
    }

    private static float factorial(int n) {
        if (n == 1) return 1;
        return n * factorial(n - 1);
    }


    
}
