package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FibonaaciSequence {
    public static void main(String[] args) {
        List<Integer> response = new ArrayList<>();
        response.add(0);
        response.add(1);
        generateFib(10, new HashMap<>(), response);
        System.out.println(response);

        System.out.println(addSeq(10, 0, 1));
    }


    public static int generateFib(int n, Map<Integer, Integer> memo, List<Integer> response) {
        if (memo.containsKey(n)) return memo.get(n);

        if (n == 1  || n == 0) {
            return n;
        }

        memo.put(n - 1, generateFib(n - 1, memo, response));
        memo.put(n - 2, generateFib(n - 2, memo, response));
        response.add(memo.get(n - 1) + memo.get(n - 2)) ;
        return memo.get(n - 1) + memo.get( n - 2);
    }


    public static int  addSeq(int n, int b1, int b2) {
        if (n == 0) return b1;
        return addSeq(n - 1, b2, b1 + b2);
    }
}
