package com.algo.expert.dp;

import java.util.Arrays;

public class MaxProfitsWithKTransactions {
    public static void main(String[] args) {
        int arr[] = {5, 11, 3, 50, 60, 90};
        System.out.println(maxProfitWithKTransactions(arr, 2));
    }

    public static int maxProfitWithKTransactions(int[] prices, int k) {
        int dp[][] = new int[k + 1][prices.length];
        for (int i = 1; i < k + 1; i++) {
            int max = Integer.MIN_VALUE;
            for (int j = 1; j < prices.length; j++) {
                max = Math.max(max, dp[i - 1][j - 1] - prices[j - 1]);
                dp[i][j] = Math.max(dp[i][j - 1], max+ prices[j]);
            }
        }

        for (int[] ints : dp) {
            System.out.println(Arrays.toString(ints));
        }
        return dp[k][prices.length - 1];
    }
}
