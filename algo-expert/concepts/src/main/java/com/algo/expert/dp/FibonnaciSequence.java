package com.algo.expert.dp;

import java.util.HashMap;
import java.util.Map;

public class FibonnaciSequence {
    public static void main(String[] args) {
        System.out.println(fibonnaciSequnece(0));
        System.out.println(fibonnaciSequnece(7));
        System.out.println(fibonnaciSequnece(1));
        System.out.println(fibonnaciSequnece(30));
    }

    // example of memoniztion
    private static int fibonnaciSequnece(int n) {
        if (n == 0) return 0;

        Map<Integer, Integer> memo = new HashMap<>();
        memo.put(0, 0);
        memo.put(1, 1);
        for (int i = 2; i < n + 1; i++) {
            memo.put(i, memo.get(i - 1) + memo.get(i - 2));
        }

        return memo.get(n);
    }
}
