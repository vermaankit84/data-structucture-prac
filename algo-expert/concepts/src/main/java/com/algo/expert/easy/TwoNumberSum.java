package com.algo.expert.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoNumberSum {
    public static void main(String[] args) {
        int [] request = {3, 5, -4, 8, 11, 1, -1, 6};
        System.out.println(Arrays.toString(twoNumberSum(request, 10)));
    }

    public static int[] twoNumberSum(int[] array, int targetSum) {
       int [] response =  null;
        Map<Integer, Boolean> map = new HashMap<>();
        for (int j : array) {
            map.put(j, true);
        }

        for (int element : array) {
            int remaining = targetSum - element;
            if (map.get(remaining) != null && map.get(remaining)) {
                if (response == null) response = new int[2];
                response[0] = element;
                response[1] = remaining;
                map.put(remaining, false);
            }
        }

        if (response == null) {
            response = new int[0];
        }
        return response;
    }
}
