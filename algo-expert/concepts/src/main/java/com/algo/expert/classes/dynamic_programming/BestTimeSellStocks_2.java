package com.algo.expert.classes.dynamic_programming;

public class BestTimeSellStocks_2 {
    public static void main(String[] args) {
        int prices[] = {7, 6, 4, 3, 2, 1};
        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) {
        int max_profit = 0;

        for (int i = 1; i < prices.length; i++) {
            int current_price = prices[i];
            int prev_price = prices[i - 1];

            if (prev_price < current_price) {
                max_profit = max_profit + (current_price - prev_price);
            }
        }

        return max_profit;
    }
}
