package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.List;

public class BinaryString {
    public static void main(String[] args) {
        int n = 3;
        List<String> binaryStrings = generateBinaryStrings(n);

        System.out.println(binaryStrings);
    }

    private static List<String> generateBinaryStrings(int n) {
        List<String> response = new ArrayList<>();
        helper("", n, response);
        return response;
    }

    private static void helper(String slate, int n, List<String> response) {
        if (n == 0) {
            response.add(slate);
            return;
        }

        helper(slate + "1" , n - 1, response);
        helper(slate + "0", n - 1, response);
    }
/*
    private static List<String> generateBinaryStrings(int n) {
        List<String> response = new ArrayList<>();
        response.add("0");
        response.add("1");

        for (int i = 2 ; i <= n ; i++) {
            List<String> temp = new ArrayList<>();
            for (String res: response ) {
                temp.add(res + "0");
                temp.add(res + "1");
            }
            response = temp;
        }
        return response;
    }
    */

}
