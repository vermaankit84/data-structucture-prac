package com.algo.expert.classes.recursion;
import java.util.*;
public class GetWordsFromPhoneNumbers {
    public static void main(String[] args) {
        String str = "1234567";
        System.out.println(getWordsFromPhoneNumber(str));
        System.out.println(getWordsFromPhoneNumber("1010101"));
        System.out.println(getWordsFromPhoneNumber("1237890"));
    }

    public static List<String> getWordsFromPhoneNumber(String phoneNumber) {
        String[] current = new String[phoneNumber.length()];
        List<String> found = new ArrayList<>();
        helper(0, phoneNumber, current, found);
        return found;
    }

    private static void helper(int i, String phoneNumber, String[] current, List<String> found) {
        if (i == phoneNumber.length()) {
            StringBuilder builder = new StringBuilder();
            for (String temp : current) {
                if (!temp.equalsIgnoreCase("1") && !temp.equalsIgnoreCase("0")) {
                    builder.append(temp);
                }
            }
            if (builder.length() == 0) {
                found.add("-1");
            } else  {
                found.add(builder.toString());
            }
            return;
        }
        char digit = phoneNumber.charAt(i);
        String[] letters = DIGIT_LIST.get(String.valueOf(digit));
        for (String letter : letters) {
            current[i] = letter;
            helper(i + 1, phoneNumber, current, found);
        }
    }

    private static final Map<String, String[]> DIGIT_LIST = new HashMap<>();

    static {
        DIGIT_LIST.put("0", new String[]{"0"});
        DIGIT_LIST.put("1", new String[]{"1"});
        DIGIT_LIST.put("2", new String[]{"a", "b", "c"});
        DIGIT_LIST.put("3", new String[]{"d", "e", "f"});
        DIGIT_LIST.put("4", new String[]{"g", "h", "i"});
        DIGIT_LIST.put("5", new String[]{"j", "k", "l"});
        DIGIT_LIST.put("6", new String[]{"m", "n", "o"});
        DIGIT_LIST.put("7", new String[]{"p", "q", "r", "s"});
        DIGIT_LIST.put("8", new String[]{"t", "u", "v"});
        DIGIT_LIST.put("9", new String[]{"w", "x", "y", "z"});
    }
}
