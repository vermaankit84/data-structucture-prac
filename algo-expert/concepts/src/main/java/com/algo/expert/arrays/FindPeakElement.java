package com.algo.expert.arrays;

public class FindPeakElement {
    public static void main(String[] args) {
        System.out.println(findPeakElement(new int[] {1,2,3,1}));
        System.out.println(findPeakElement(new int[] {1,2,1,3,5,6,4}));
        System.out.println(findPeakElement(new int[] {1,2, 3, 4,5, 6}));
    }

    public static int findPeakElement(int[] nums) {

        for (int i = 1; i < nums.length; i++) {
            int current_element = nums[i];
            int pre_element = nums[i - 1];

            if (current_element < pre_element) {
                return i - 1;
            }
        }

        return -1;

    }
}
