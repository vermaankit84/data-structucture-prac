package com.algo.expert.designpatterns.structural.visitor;

public interface Visitor {
    void visitTheElement(CompositeEmployee employee);
    void visitTheElement(SimpleEmployee employee);
}
