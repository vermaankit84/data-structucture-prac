package com.algo.expert.easy.goldman;

public class MyAtoi {
    public static void main(String[] args) {
        int expected = 105;
        int actual = convertToInt("105 test java ");
        System.out.println(actual);
    }

    private static int convertToInt(String number){
        int response = 0;
        int sign = 1;
        int i = 0;
        if (number.charAt(0) == '-') {
            i++;
            sign = -1;
        }

        for (; i < number.length(); i++) {
            if ((number.charAt(i) - '0') >=0 && (number.charAt(i) - '0') <= 9) {
                response = response * 10 + number.charAt(i) - '0';
            }
        }

        return sign * response;
    }
}
