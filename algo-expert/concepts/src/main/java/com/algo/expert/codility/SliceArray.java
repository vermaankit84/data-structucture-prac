package com.algo.expert.codility;

import java.util.*;

public class SliceArray {
    public static void main(String[] args) {
        int arr[] = {2, 2,2,2, 1, 2, -1, 2, 1, 3};
        List<pair> list = new ArrayList<>();
        for (int j = 1; j < arr.length; j++) {
            pair p = new pair();
            if (arr[j - 1] < arr[j]) {
                p.start = j - 1;
                while (j < arr.length && arr[j - 1] < arr[j]) {
                    j = j + 1;
                }
                p.end = j - 1;
                list.add(p);
            }
        }

        list.forEach(p -> System.out.println(p.start + " " + p.end));
    }

    static  class pair {
        int start;
        int end;
    }
}
