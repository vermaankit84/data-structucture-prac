package com.algo.expert.dp;

import java.util.Arrays;

public class LongestIncresingSubsequence {
    public static void main(String[] args) {

    }

    public int lengthOfLIS(int[] nums) {
        int dp[] = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int max = 0;
            for (int j = i; j > -1; j = j - 1) {
                if (dp[i] > dp[j]) {
                    max = Math.max(max, dp[j]);
                }
            }

            dp[i] = max + 1;
        }

        Arrays.sort(nums);
        return dp[dp.length - 1];
    }
}
