package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FindCombinations {
    public static void main(String[] args) {
        System.out.println(find_combinations(5, 2));
    }

    public static List<List<Integer>> find_combinations(int n, int k) {
        List<List<Integer>> response = new ArrayList<>();
        List<Integer> input = IntStream.rangeClosed(1, n).boxed().collect(Collectors.toList());
        helper(1, new ArrayList<>(), response, k, input);
        return response;
    }

    private static void helper( int idx, List<Integer> slate, List<List<Integer>> response, int k, List<Integer> input) {
        if (k == slate.size()) {

            response.add(new ArrayList<>(slate));
            return;
        }
       // helper(input, idx + 1, slate, response, k);
        for (int i = idx; i <= input.size(); i++) {
            slate.add(i);
            helper(i + 1, slate, response, k, input);
            slate.remove(slate.size() - 1);
        }

    }
}
