package com.algo.expert.classes.dynamic_programming;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

public class NoOfways {
    public static void main(String[] args) {
        List<Integer> coins = Lists.newArrayList(1, 2,3);
        System.out.println(number_of_ways(coins, 3));
    }

    public static int number_of_ways(List<Integer> coins, int amount) {
       int ways[] = new int[amount + 1];
       ways[0] = 1;
       for (int coin : coins) {
            for (int i = 1; i < amount + 1; i++) {
                if (coin <= i) {
                    ways[i] = ways[i] + ways[i - coin];
                }
            }
        }
        return ways[amount];
    }
}

