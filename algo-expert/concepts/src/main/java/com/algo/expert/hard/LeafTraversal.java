package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class LeafTraversal {
    public static void main(String[] args) {

    }

    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }

    public boolean compareLeafTraversal(BinaryTree tree1, BinaryTree tree2) {
        Stack<BinaryTree> treeStack_1 = new Stack<>();
        treeStack_1.push(tree1);
        Stack<BinaryTree> treeStack_2 = new Stack<>();
        treeStack_2.push(tree2);

        while (!treeStack_1.isEmpty() && !treeStack_2.isEmpty()) {
            BinaryTree tree1Leaf = getNextLeafNode(treeStack_1);
            BinaryTree tree2Leaf = getNextLeafNode(treeStack_2);

            if (tree1Leaf.value!=tree2Leaf.value) return false;
        }
        return treeStack_1.isEmpty() && treeStack_2.isEmpty();
    }

    private BinaryTree getNextLeafNode(Stack<BinaryTree> traversalStack) {
        BinaryTree currentNode = traversalStack.pop();

        while (!isLeafNode(currentNode)) {
            if (currentNode.right!=null) {
                traversalStack.push(currentNode.right);
            }

            if (currentNode.left!=null) {
                traversalStack.push(currentNode.left);
            }

            currentNode = traversalStack.pop();
        }

        return currentNode;
    }

    public boolean isLeafNode(BinaryTree node) {
        return ((node.left == null) &&( node.right == null));
    }


}
