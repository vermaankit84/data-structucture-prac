package com.algo.expert.test;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Tester {
    public static void main(String[] args) throws Exception {


        LocalDate date = LocalDate.parse("2012-12-10", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println(date);

        System.out.println("Axis Bluechip Fund-Reg(IDCW) \ntest");


        String dateTime = "01/10/2020 00:00:00";
        DateTimeFormatter formatDateTime = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.from(formatDateTime.parse(dateTime));
        Timestamp ts = Timestamp.valueOf(localDateTime);

        System.out.println(ts);

    }
}
