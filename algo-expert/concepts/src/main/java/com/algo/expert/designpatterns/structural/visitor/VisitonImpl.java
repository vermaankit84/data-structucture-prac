package com.algo.expert.designpatterns.structural.visitor;

import java.util.ArrayList;
import java.util.List;

public class VisitonImpl {
    public static void main(String[] args) {

        SimpleEmployee mathEmployee_1 = new SimpleEmployee("Math Teacher 1", "maths", 13);
        SimpleEmployee mathEmployee_2 = new SimpleEmployee("Math Teacher 2", "maths", 6);


        SimpleEmployee cs_1 = new SimpleEmployee("Computer Science 1", "CS", 10);
        SimpleEmployee cs_2 = new SimpleEmployee("Computer Science 2", "CS", 13);
        SimpleEmployee cs_3 = new SimpleEmployee("Computer Science 3", "CS", 7);

        CompositeEmployee hodMaths = new CompositeEmployee("Maths_1", "maths", 14);
        hodMaths.addEmployee(mathEmployee_1);
        hodMaths.addEmployee(mathEmployee_2);

        CompositeEmployee hodComputer = new CompositeEmployee("CS_1", "computer", 16);
        hodComputer.addEmployee(cs_1);
        hodComputer.addEmployee(cs_2);
        hodComputer.addEmployee(cs_3);

        CompositeEmployee principal = new CompositeEmployee("principal", "admin", 20);
        principal.addEmployee(hodComputer);
        principal.addEmployee(hodMaths);

        System.out.println("***************** Testing the overall Structure *****************");
        principal.printStructures();


        System.out.println("***************** implement the visitor pattern *****************");
        Visitor concrete = new ConcreteVisitor();
        List<Employee> empContainer = new ArrayList<>(principal.getControls());
        empContainer.addAll(hodMaths.getControls());
        empContainer.addAll(hodMaths.getControls());

        empContainer.forEach(e -> e.acceptVisitor(concrete));
    }
}
