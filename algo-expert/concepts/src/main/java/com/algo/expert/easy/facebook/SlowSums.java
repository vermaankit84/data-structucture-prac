package com.algo.expert.easy.facebook;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class SlowSums {
    public static void main(String[] args) {
        SlowSums slowSums = new SlowSums();
        int[] arr_1 = {4, 2, 1, 3};
        System.out.println(slowSums.getTotalTime(arr_1));
    }

    int getTotalTime(int[] arr) {
        Queue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());

        for (int i: arr) {
            queue.add(i);
        }
        int sum = 0;
        while (queue.size()!=2) {
            int temp = queue.poll() + queue.poll();
            sum = sum + temp ;
            queue.add(temp);
        }

        sum = sum + queue.poll() + queue.poll();

        return sum;
    }
}
