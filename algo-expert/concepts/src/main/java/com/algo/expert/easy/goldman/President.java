package com.algo.expert.easy.goldman;

public class President {
    public static void main(String[] args) {
        System.out.println(" president " + lastStandingStudent(13, 2));
    }

    public static int lastStandingStudent(int n, int k) {
       if (n == 1) return 1;

       return (lastStandingStudent(n - 1, k) + k - 1) % n + 1;
    }
}
