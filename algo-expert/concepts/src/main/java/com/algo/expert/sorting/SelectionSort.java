package com.algo.expert.sorting;

import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {
        int arr[] = {4, 5, 6, 3, 7, 8, 1, 6, 2, 8};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    // select the smallest element
    // put the element in the correct order
    public static void sort(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            int element = arr[i];
            int min_index = findMinIndex(arr, i );
            if (element > arr[min_index]) {
                swap(arr, i, min_index);
            }
        }
    }

    private static int findMinIndex(int[] arr, int j) {
        int min = Integer.MAX_VALUE;
        int idx = 0;
        for (int i = j; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
                idx = i;
            }
        }
        return idx;
    }

    private static void swap(int[] arr, int i, int min_index) {
        int temp = arr[i];
        arr[i] = arr[min_index];
        arr[min_index] = temp;
    }
}
