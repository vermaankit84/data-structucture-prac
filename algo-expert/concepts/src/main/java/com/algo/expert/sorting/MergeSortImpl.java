package com.algo.expert.sorting;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

// O(nlogn) 
public class MergeSortImpl {
    public static void main(String[] args) {
        List<Integer> data = Lists.newArrayList(4, 5, 6, 3, 7, 8, 1, 6, 2, 8);
        int[] arr = new int[data.size()];
        for (int i = 0; i < data.size(); i++) {
            arr[i] = data.get(i);
        }
        sort(arr);
        List<Integer> response = new ArrayList<>();
        for (int j : arr) {
            response.add(j);
        }
        System.out.println(response);
    }

    public static void sort(int arr[]) {
        mergeSort(arr, 0, arr.length - 1);
    }

    private static void mergeSort(int arr[], int start, int end) {
        if (start < end) {
            int mid = start + (end - start)/2;
            mergeSort(arr, start, mid);
            mergeSort(arr ,mid + 1, end);
            merge(arr, start , mid, end);
        }
    }

    private static void merge(int arr[], int start, int mid , int end) {

        int left[] = new int[mid - start + 1];
        int right[] = new int[end - mid];

        for (int i = 0; i < (mid - start + 1); i++) {
            left[i] = arr[start +i];
        }


        for (int i = 0; i < (end - mid) ; i++) {
            right[i] = arr[mid + 1 + i];
        }


        int i = 0;
        int j = 0;
        int counter = start;
        while (i < left.length && j < right.length) {
            if (left[i] > right[j]) {
                arr[counter]  = right[j];
                j = j + 1;
            } else {
                arr[counter]  = left[i];
                i = i + 1;
            }
            counter = counter + 1;
        }

        while (i < left.length) {
            arr[counter] = left[i];
            counter += 1;
            i += 1;
        }


        while (j < right.length) {
            arr[counter] = right[j];
            counter += 1;
            j += 1;
        }

    }
}
