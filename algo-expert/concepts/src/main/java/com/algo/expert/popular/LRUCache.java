package com.algo.expert.popular;

import java.util.*;

public class LRUCache {

    private final Map<Integer, Integer> cache = new HashMap<>();
    private final ArrayDeque<Integer> recentQueue = new ArrayDeque<>();

    private final int capacity;

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        int value = cache.getOrDefault(key, -1);
        if (value != -1) {
            if (recentQueue.contains(key)) {
                recentQueue.remove(key);
            }
            recentQueue.add(key);
        }
        return value;
    }

    public void put(int key, int value) {
        if (cache.containsKey(key)) {
            cache.put(key, value);
            recentQueue.remove(key);
            recentQueue.add(key);
            return;
        }
        if (cache.size() >= capacity) {
            evict();
        }
        cache.put(key, value);
        recentQueue.add(key);
    }

    private void evict() {
        int element = recentQueue.removeFirst();
        cache.remove(element);

    }

    public static void main(String[] args) {
        LRUCache lRUCache = new LRUCache(2);
        System.out.println(lRUCache.get(2));
        lRUCache.put(2, 6);
        System.out.println(lRUCache.cache);
        System.out.println(lRUCache.get(1));
        lRUCache.put(1, 5);
        System.out.println(lRUCache.cache);
        lRUCache.put(1, 2);
        System.out.println(lRUCache.cache);
        System.out.println(lRUCache.get(1));
        System.out.println(lRUCache.get(2));
    }

    //
    //["LRUCache","get","put","get","put","put","get","get"]
      //      [[2],[2],[2,6],[1],[1,5],[1,2],[1],[2]]
    //["LRUCache","put","get","put","get","get"]
    //[[1],[2,1],[2],[3,2],[2],[3]]
    //LRUCache lRUCache = new LRUCache(1);
    //        lRUCache.put(2, 1); // cache is {1=1}
    //        lRUCache.get(2);
    //        lRUCache.put(3, 2);
    //        lRUCache.get(2);
    //        lRUCache.get(3);
    //
}
