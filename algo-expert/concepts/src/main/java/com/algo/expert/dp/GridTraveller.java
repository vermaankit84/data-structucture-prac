package com.algo.expert.dp;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GridTraveller {
    public static void main(String[] args) {
        Map<Pair, Integer> memo = new HashMap<>();
        memo.put(new Pair(0, 0), 0);
        memo.put(new Pair(1, 1), 1);
        System.out.println(gridTraveller(20, 30, memo));
    }

    public static int gridTraveller(int start, int end, Map<Pair, Integer> memo) {
        if (start == 0 || end == 0) return memo.get(new Pair(0, 0));
        if (start == 1 && end == 1) return memo.get(new Pair(1, 1));
        if (memo.get(new Pair(start, end))!=null) return memo.get(new Pair(start, end));
        memo.put(new Pair(start, end), gridTraveller(start - 1, end, memo) + gridTraveller(start, end -1 , memo));
        return memo.get(new Pair(start, end));
    }

    static class Pair {
        int start = 0;
        int end = 0;

        public Pair(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pair)) return false;
            Pair pair = (Pair) o;
            return start == pair.start && end == pair.end;
        }

        @Override
        public int hashCode() {
            return Objects.hash(start, end);
        }
    }
}
