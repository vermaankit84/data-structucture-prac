package com.algo.expert.classes.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class RiverSize {
    public static void main(String[] args) {
        int [][] matrix = {
                {1, 0, 0, 1, 0},
                {1, 0, 1, 0, 0},
                {0, 0, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0}
        };

        System.out.println(riverSizes(matrix));
    }

    public static List<Integer> riverSizes(int[][] matrix) {
        List<Integer> size = new ArrayList<>();
        boolean [][] visited = new boolean[matrix.length][matrix[0].length];

        for (int i =0 ; i < matrix.length; i = i = i + 1) {
            for (int j = 0; j < matrix[0].length; j = j + 1) {
                if (visited[i][j]) {
                    continue;
                }

                traverseNodes(i, j, matrix, visited, size);
            }
        }

        return size;
    }

    private static void traverseNodes(int i, int j, int[][] matrix, boolean[][] visited, List<Integer> size) {
        int currentSize = 0;
        Stack<Integer[]> nodes = new Stack<>();
        nodes.add(new Integer[]{i, j});

        while (!nodes.isEmpty()) {
            Integer[] current_node = nodes.pop();
            i = current_node[0];
            j = current_node[1];

            if (visited[i][j]) {
                continue;
            }

            visited[i][j] = true;

            if (matrix[i][j] == 0) {
                continue;
            }
            currentSize = currentSize + 1;
            nodes.addAll(getUnvistedNodes(i ,j, matrix, visited));
        }

        if (currentSize > 0) {
            size.add(currentSize);
        }
    }

    private static List<Integer[]> getUnvistedNodes(int i, int j, int[][] matrix, boolean[][] visited) {
        List<Integer[]> unvistedNodes = new ArrayList<>();

        if (i > 0 && !visited[i - 1][j]) {
            unvistedNodes.add(new Integer[] { i - 1, j});
        }

        if (j > 0 && !visited[i][j - 1]) {
            unvistedNodes.add(new Integer[] { i, j - 1});
        }

        if (i < matrix.length - 1 && !visited[i + 1][j]) {
            unvistedNodes.add(new Integer[] { i + 1, j});
        }

        if (j < matrix[0].length - 1 && !visited[i][j + 1]) {
            unvistedNodes.add(new Integer[] { i, j + 1});
        }

        return unvistedNodes;
    }
}
