package com.algo.expert.designpatterns.structural.Bridge;

public class OnState implements State {
    @Override
    public void moveState() {
        System.out.println("On State");
    }

    @Override
    public void hardPressed() {
        System.out.println("stete changed please fo not hard press");
    }
}
