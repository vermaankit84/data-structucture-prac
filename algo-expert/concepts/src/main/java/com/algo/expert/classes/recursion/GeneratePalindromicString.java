package com.algo.expert.classes.recursion;

import java.util.*;

public class GeneratePalindromicString {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(generate_palindromic_decompositions("abracadabra")));
    }


    static String[] generate_palindromic_decompositions(String s) {

        List<String> result = new ArrayList<>();

        helper(s.toCharArray(), 0, new char[s.length() * 2], 0, result);

        return result.toArray(new String[result.size()]);
    }

    //j keeps track of where to insert at for the slate
    static void helper(char[] arr, int index, char[] slate, int j, List<String> result) {

        if(index == arr.length) {
            result.add(new String(slate, 0, j-1));
            return;
        }

        for(int i = index; i < arr.length; i++) {

            slate[j] = arr[i];
            j++;

            if(isPalindrome(arr, index, i)) {
                slate[j] = '|';
                helper(arr, i + 1, slate, j + 1, result);
            }

        }
    }


    static boolean isPalindrome(char[] arr, int left, int right) {

        while(left < right) {
            if(arr[left] != arr[right]) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
