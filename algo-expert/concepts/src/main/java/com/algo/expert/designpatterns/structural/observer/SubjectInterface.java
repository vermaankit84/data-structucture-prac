package com.algo.expert.designpatterns.structural.observer;

public interface SubjectInterface {
    void register(Observer observer);
    void unregister(Observer observer);
    void notify(int notified);
}
