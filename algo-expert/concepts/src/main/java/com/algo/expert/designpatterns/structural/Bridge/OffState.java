package com.algo.expert.designpatterns.structural.Bridge;

public class OffState implements State {
    @Override
    public void moveState() {
        System.out.println("Off state");
    }

    @Override
    public void hardPressed() {
        System.out.println("offline please do no press");
    }
}
