package com.algo.expert.designpatterns.structural.visitor;

public class ConcreteVisitor implements Visitor {
    @Override
    public void visitTheElement(CompositeEmployee employee) {

        System.out.println(employee.getName() + " from " + employee.getDept()  + " is eligible for promotion "
                + (employee.getYearsOfExperiance() > 15));
    }

    @Override
    public void visitTheElement(SimpleEmployee employee) {
        System.out.println(employee.getName() + " from " + employee.getDept()  + " is eligible for promotion "
                + (employee.getYearsOfExperiance() > 12));
    }
}
