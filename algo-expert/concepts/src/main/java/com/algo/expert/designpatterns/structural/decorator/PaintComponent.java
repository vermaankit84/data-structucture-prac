package com.algo.expert.designpatterns.structural.decorator;

public class PaintComponent extends AbstractDecorator {
    public  void makeHouse() {
        super.makeHouse();
        paint();
    }

    private void paint() {
        System.out.println("paint the wall");
    }
}
