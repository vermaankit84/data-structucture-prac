package com.algo.expert.designpatterns.behavioral.startegy;

public class Aeroplane extends Vehicle {
    public Aeroplane() {
        super(new AirTrnsport());
    }

    @Override
    public void showMe() {
        System.out.println("i am in aeroplane");
    }
}
