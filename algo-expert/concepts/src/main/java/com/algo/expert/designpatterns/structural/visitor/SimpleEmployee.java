package com.algo.expert.designpatterns.structural.visitor;

public class SimpleEmployee implements Employee {
    private final String name;
    private final String dept;
    private final int yearsOfExperiance;

    public SimpleEmployee(String name, String dept, int yearsOfExperiance) {
        this.name = name;
        this.dept = dept;
        this.yearsOfExperiance = yearsOfExperiance;
    }

    public String getName() {
        return name;
    }

    public String getDept() {
        return dept;
    }

    public int getYearsOfExperiance() {
        return yearsOfExperiance;
    }

    @Override
    public void printStructures() {
        System.out.println(getName() + " works in " + getDept() + " experiance " + getYearsOfExperiance() + " years");
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitTheElement(this);
    }
}
