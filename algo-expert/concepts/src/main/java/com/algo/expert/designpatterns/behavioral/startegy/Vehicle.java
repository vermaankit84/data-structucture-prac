package com.algo.expert.designpatterns.behavioral.startegy;

public abstract class Vehicle {
    private TransportMedium medium;

    public Vehicle(final TransportMedium medium) {
        this.medium = medium;
    }

    public void showTransportMedium() {
        medium.transport();;
    }



    public void commonJob() {
        System.out.println("We all use transport");
    }

    public abstract void showMe();
}
