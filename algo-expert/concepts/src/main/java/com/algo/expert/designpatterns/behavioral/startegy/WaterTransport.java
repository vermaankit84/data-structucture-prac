package com.algo.expert.designpatterns.behavioral.startegy;

public class WaterTransport implements TransportMedium{

    @Override
    public void transport() {
        System.out.println("transporting in water");
    }
}
