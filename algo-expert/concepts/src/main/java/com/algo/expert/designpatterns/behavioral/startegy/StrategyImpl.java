package com.algo.expert.designpatterns.behavioral.startegy;

public class StrategyImpl {
    public static void main(String[] args) {
        System.out.println("------------------");
        Vehicle context = new Boat(new WaterTransport());
        context.showMe();
        context.showTransportMedium();
        System.out.println("------------------");

        context = new Aeroplane();
        context.showMe();
        context.showTransportMedium();
        System.out.println("------------------");
    }
}
