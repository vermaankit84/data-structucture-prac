package com.algo.expert.easy.goldman;

import java.util.*;

public class PrintAnagrams {
    public static void main(String[] args) {
       String words[] = {"act","god","cat","dog","tac"};
       List<List<String>> output = Anagrams(words);
        System.out.println(output);
    }

    public static List<List<String>> Anagrams(String[] string_list) {
        Map<String, List<String>> map = new HashMap<>();
        for (String s : string_list) {
            char[] characters = s.toCharArray();
            Arrays.sort(characters);
            List<String> anagram;
            if (map.containsKey(new String(characters))) {
                anagram = map.get(new String(characters));
            } else {
                anagram = new ArrayList<>();
            }
            anagram.add(s);
            map.put(new String(characters), anagram);
        }

        return new ArrayList<>(map.values());
    }


}
