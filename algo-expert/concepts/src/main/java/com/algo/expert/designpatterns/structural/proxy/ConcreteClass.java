package com.algo.expert.designpatterns.structural.proxy;

public class ConcreteClass extends Subject {
    @Override
    public void doWork() {
        System.out.println("in concrete do work");
    }
}
