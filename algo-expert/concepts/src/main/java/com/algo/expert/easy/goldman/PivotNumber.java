package com.algo.expert.easy.goldman;

import java.util.Arrays;

public class PivotNumber {
    public static void main(String[] args) {
       int [] nums = {1, 7, 3, 6, 5, 6};

       System.out.println(findPivot(nums));
    }

    private static int findPivot(int[] nums) {
        int sum  = Arrays.stream(nums).sum();
        int temp = 0;
        for (int i = 0; i < nums.length; i++) {
            sum = sum - nums[i];
            if (temp == sum) return i;
            temp = temp + nums[i];
        }

        return -1;
    }
}
