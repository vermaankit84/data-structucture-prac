package com.algo.expert.classes.trees;

import java.util.*;

public class HorizotalTraversal {
    public static void main(String[] args) {
        TreeNode treeNode = TreeUtils.buildTree();
        Queue<Pair> queue = new LinkedList<>();
        queue.add(new Pair(treeNode, 0));
        Map<Integer, List<Integer>> map = new TreeMap<>();

        while (!queue.isEmpty()) {
            Pair pair = queue.poll();
            TreeNode current_node = pair.node;
            int current_hd = pair.current_hd;
            int current_element = current_node.value;

            List<Integer> current_hd_list =  map.get(current_hd);
            if (current_hd_list == null) {
                current_hd_list = new ArrayList<>();
            }
            current_hd_list.add(current_element);
            map.put(current_hd, current_hd_list);

            if (current_node.getLeft()!=null) {
                queue.add(new Pair(current_node.left, current_hd - 1));
            }

            if (current_node.getRight()!=null) {
                queue.add(new Pair(current_node.right, current_hd +1));
            }

        }

        System.out.println(map);
    }

    static class Pair {
        TreeNode node;
        int current_hd;


        public Pair(TreeNode node, int current_hd) {
            this.current_hd = current_hd;
            this.node = node;
        }
    }
}
