package com.algo.expert.classes.graphs.traversals;

import com.algo.expert.classes.graphs.GraphUtils;

import java.util.*;

public class DFSTraversals {

    private static Integer time = 0;
    public static void dfS(Graph graph, Set<Integer> nodes) {
        List<Integer> response = new ArrayList<>();
        boolean[] visited = new boolean[nodes.size() + 1];
        int [] arrival = new int[nodes.size() + 1];
        int [] dep = new int[nodes.size() + 1];
        for (int vertex: nodes) {
            dfsTraversal(vertex, visited, response,  graph.adjList, arrival, dep);
        }

        System.out.println(response);

        System.out.println(Arrays.toString(arrival) + "  " + Arrays.toString(dep));
       // return response;
    }





    public static void dfsTraversal(int vertex, boolean[] visited, List<Integer> response, List<Integer> []adjList,  int [] arrival,  int [] dep) {
        if (!visited[vertex]) {
            response.add(vertex);
        }
        arrival[vertex] =  time++;
        visited[vertex] = true;
        for (int neibhour : adjList[vertex]) {
            if (!visited[neibhour]) {
                dfsTraversal(neibhour, visited, response, adjList, arrival, dep);
            }
        }
        dep[vertex] = time++;
    }

    public static void main(String[] args) {
        int n = 8;

        int[][] edges = {{0,1}, {0,2}, {1,3}, {1,4}, {2, 5}, {0, 7}, {3, 4}};;


        Graph graph = new Graph(n);
        Set<Integer> nodes = new HashSet<>();
        for (int[] edge : edges) {
            nodes.add(edge[0]);
            nodes.add(edge[1]);
            graph.addEdge(edge[0], edge[1]);
        }

        dfS(graph, nodes);
    }
}
