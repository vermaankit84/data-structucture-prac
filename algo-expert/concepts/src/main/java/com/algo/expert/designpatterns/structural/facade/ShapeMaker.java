package com.algo.expert.designpatterns.structural.facade;

public class ShapeMaker {
   private final Shape circle = new Circle();
   private final Shape rectangle = new Rectangle();

   public void  drawCircle() {
       circle.draw();
   }

   public void drawRectangle() {
       rectangle.draw();
   }
}
