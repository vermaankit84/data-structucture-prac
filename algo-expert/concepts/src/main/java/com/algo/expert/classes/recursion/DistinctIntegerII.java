package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DistinctIntegerII {
    public static void main(String[] args) {
        List<Integer> list = Lists.newArrayList(1, 2,3);
        List<List<Integer>> result = new ArrayList<>();
        Stack<Integer> stack = new Stack<>();
        distinctIntegerII(list, 0, stack, result);
        System.out.println(result);
    }

    private static void distinctIntegerII(List<Integer> list, int index, Stack<Integer> partial, List<List<Integer>> result) {
        if (index == list.size()) {
            result.add(new ArrayList<>(partial));
            return;
        }

        for (int i = index; i <list.size(); i++) {

            partial.push(list.get(i));
            distinctIntegerII(list, i + 1, partial, result);
            partial.pop();
        }
    }
}
