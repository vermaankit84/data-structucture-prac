package com.algo.expert.classes.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class YoungestCommonAncestor {
    public static void main(String[] args) {
        AncestralTree A = new AncestralTree('A');

        AncestralTree B = new AncestralTree('B');
        AncestralTree C = new AncestralTree('C');


        AncestralTree D = new AncestralTree('D');
        AncestralTree I  = new AncestralTree('I');
        D.addAsAncestor(new AncestralTree[] {new AncestralTree('H'), I});

        C.addAsAncestor(new AncestralTree[] {new AncestralTree('F'), new AncestralTree('G')});

       AncestralTree E =  new AncestralTree('E');
        B.addAsAncestor(new AncestralTree[] {D, E});

        A.addAsAncestor(new AncestralTree[] {B ,C});

        System.out.println(getYoungestCommonAncestor(A, E, I).name);

    }

    public static AncestralTree getYoungestCommonAncestor(AncestralTree topAncestor, AncestralTree descendantOne, AncestralTree descendantTwo) {
        List<AncestralTree> descendentOneLevel = new ArrayList<>();
        Queue<AncestralTree> queue = new LinkedList<>();
        queue.add(descendantOne);

        while (!queue.isEmpty()) {
            AncestralTree currentNode = queue.poll();
            descendentOneLevel.add(currentNode);

            if (currentNode.ancestor!=null) {
                queue.add(currentNode.ancestor);
            }
        }


        List<AncestralTree> descendentTwoLevel = new ArrayList<>();
        queue.add(descendantTwo);

        while (!queue.isEmpty()) {
            AncestralTree currentNode = queue.poll();
            descendentTwoLevel.add(currentNode);

            if (currentNode.ancestor!=null) {
                queue.add(currentNode.ancestor);
            }
        }



        while (descendentOneLevel.size() > descendentTwoLevel.size()) {
            descendentTwoLevel.add(0, new AncestralTree('1'));
        }

        while (descendentOneLevel.size() < descendentTwoLevel.size()) {
            descendentOneLevel.add(0, new AncestralTree('1'));
        }


        System.out.println(descendentOneLevel);
        System.out.println(descendentTwoLevel);

        for (int i = 0; i < descendentOneLevel.size(); i++) {
            AncestralTree c1 = descendentOneLevel.get(i);
            AncestralTree c2 = descendentTwoLevel.get(i);

            if (c1.name == c2.name) {
                return c1;
            }
        }




        return null;
    }

    static class AncestralTree {
        public char name;
        public AncestralTree ancestor;

        AncestralTree(char name) {
            this.name = name;
            this.ancestor = null;
        }

        // This method is for testing only.
        void addAsAncestor(AncestralTree[] descendants) {
            for (AncestralTree descendant : descendants) {
                descendant.ancestor = this;
            }
        }

        public String toString() {
            return this.name + " ";
        }
    }
}
