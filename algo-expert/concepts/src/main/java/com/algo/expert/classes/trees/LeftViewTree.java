package com.algo.expert.classes.trees;

import com.algo.expert.classes.trees.traversal.BinaryLevel;

import java.util.*;

public class LeftViewTree {
    public static void main(String[] args) {
        TreeNode node = TreeUtils.buildTree();
        System.out.println(leftViewTree(node));
        System.out.println(rightViewTree(node));
    }

    public static List<Integer> leftViewTree(TreeNode node) {
        List<Integer> leftViewList = new ArrayList<>();
        Queue<BinaryLevel> queue = new LinkedList<>();
        queue.add(new BinaryLevel(node, 0, node.getValue()));
        List<BinaryLevel> binaryLevels = new ArrayList<>();
        while (!queue.isEmpty()) {
            BinaryLevel binaryLevel = queue.poll();
            TreeNode temp = binaryLevel.getNode();
            int level = binaryLevel.getLevel();
            binaryLevels.add(binaryLevel);
            if (temp.getLeft()!=null) {
                TreeNode leftNode = temp.getLeft();
                queue.add(new BinaryLevel(leftNode, level + 1, leftNode.getValue()));
            }

            if (temp.getRight()!=null) {
                TreeNode rightNode = temp.getRight();
                queue.add(new BinaryLevel(rightNode, level + 1, rightNode.getValue()));
            }
        }


        Map<Integer, List<Integer>> levelMap = new LinkedHashMap<>();
        for (BinaryLevel levelInfo: binaryLevels) {
            int level = levelInfo.getLevel();
            List<Integer> levelList = levelMap.getOrDefault(level, new ArrayList<>());
            levelList.add(levelInfo.getData());
            levelMap.put(level, levelList);
        }

        for (int key : levelMap.keySet()) {
            leftViewList.add(levelMap.get(key).get(0));
        }

        return leftViewList;
    }

    public static List<Integer> rightViewTree(TreeNode node) {
        List<Integer> leftViewList = new ArrayList<>();
        Queue<BinaryLevel> queue = new LinkedList<>();
        queue.add(new BinaryLevel(node, 0, node.getValue()));
        List<BinaryLevel> binaryLevels = new ArrayList<>();
        while (!queue.isEmpty()) {
            BinaryLevel binaryLevel = queue.poll();
            TreeNode temp = binaryLevel.getNode();
            int level = binaryLevel.getLevel();
            binaryLevels.add(binaryLevel);
            if (temp.getLeft()!=null) {
                TreeNode leftNode = temp.getLeft();
                queue.add(new BinaryLevel(leftNode, level + 1, leftNode.getValue()));
            }

            if (temp.getRight()!=null) {
                TreeNode rightNode = temp.getRight();
                queue.add(new BinaryLevel(rightNode, level + 1, rightNode.getValue()));
            }
        }


        Map<Integer, List<Integer>> levelMap = new LinkedHashMap<>();
        for (BinaryLevel levelInfo: binaryLevels) {
            int level = levelInfo.getLevel();
            List<Integer> levelList = levelMap.getOrDefault(level, new ArrayList<>());
            levelList.add(levelInfo.getData());
            levelMap.put(level, levelList);
        }

        for (int key : levelMap.keySet()) {
            List<Integer> values =  levelMap.get(key);
            leftViewList.add(values.get(values.size() - 1));
        }

        return leftViewList;
    }
}
