package com.algo.expert.dp;

import java.util.HashMap;
import java.util.Map;

public class TargetSum {
    public static void main(String[] args) {
        int []arr = {2, 4};
        Map<Integer, Boolean> memo = new HashMap<>();
        System.out.println(targetSum(arr, 300, memo));
    }

    private static boolean targetSum(int []arr, int target,  Map<Integer, Boolean> memo) {
        if (memo.containsKey(target)) return true;
        if (target == 0) return true;
        if (target < 0) return false;

        for (int j : arr) {
            if (targetSum(arr, target - j, memo)) {
                memo.put(target, true);
                return true;
            }
        }
        memo.put(target, false);
        return false;
    }
}
