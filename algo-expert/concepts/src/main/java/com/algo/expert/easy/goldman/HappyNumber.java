package com.algo.expert.easy.goldman;

public class HappyNumber {
    public static void main(String[] args) {
        int number = 91;
        int sum = getSum(number);
        while ( sum >= 10) {
            sum = getSum(sum);
        }

        if (sum == 1) {
            System.out.println("happy");
        } else  {
            System.out.println("un happy");
        }
    }

    private static int getSum(int number) {
        String str = String.valueOf(number);
        int sum = 0;
        for(int i = 0; i < str.length() ; i = i + 1) {
            sum = sum + (int)Math.pow(Integer.valueOf(String.valueOf(str.charAt(i))), 2);
        }
       return  sum;
    }
}
