package com.algo.expert.dp;

public class HouseRobber_2 {
    public static void main(String[] args) {

    }

    public static int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        if (nums.length == 1) {
            return nums[0];
        }

        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }

        return Math.max(helper(nums, 0, nums.length - 1), helper(nums, 1, nums.length));
    }

    private static int helper(int[] nums, int start, int end) {
        int dp[] = new int[nums.length];
        if(start == 0) {
            dp[0] = nums[0];
            dp[1] = Math.max(nums[0], nums[1]);
        } else {
            dp[0] = 0;
            dp[1] = nums[1];
        }

        for (int i = 2; i < end; i++) {
            dp[i] = Math.max(nums[i] + dp[i - 2], dp[i - 1]);
        }

        return dp[end - 1];
    }
}
