package com.algo.expert.easy.goldman;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinimumDistanceBetweenWords {
    public static void main(String[] args) {
        System.out.println(distance("geeks for geeks contribute practice" , "geeks", "practice"));
        System.out.println(distance("the quick the brown quick brown the frog" , "quick", "frog"));
    }

    public static int distance(String s, String w1, String w2) {
        String[] split = s.split(" ");
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < split.length; i++) {
            String data = split[i];
            map.put(data, i);
        }

        return Math.abs(map.get(w1) - map.get(w2)) - 1;
    }
}
