package com.algo.expert.designpatterns.structural.proxy;

public abstract class Subject {
    public abstract void doWork();
}
