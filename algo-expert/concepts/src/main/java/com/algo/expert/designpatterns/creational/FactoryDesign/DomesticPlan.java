package com.algo.expert.designpatterns.creational.FactoryDesign;

public class DomesticPlan implements Plan{
    @Override
    public void createPlan() {
        System.out.println("in domestic plan");
    }
}
