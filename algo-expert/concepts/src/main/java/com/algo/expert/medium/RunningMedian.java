package com.algo.expert.medium;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class RunningMedian {
    public static void main(String[] args) {
        int [] a = new int[] {2, 4, 7, 1, 5, 3};
        for (int num: a) {
            add(num);
            System.out.println(getMedian());
        }
     }

    private static final Queue<Integer> minHeap = new PriorityQueue<>();
    private static final Queue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());


    private static void add(int number) {
        if (minHeap.size() == maxHeap.size()) {
            maxHeap.offer(number);
            minHeap.offer(maxHeap.poll());
        } else {
            minHeap.offer(number);
            maxHeap.offer(minHeap.poll());
        }
    }

    private static double getMedian() {
        double median;
        if (minHeap.size() > maxHeap.size()) {
            median = minHeap.peek();
        } else {
            median = (minHeap.peek() + maxHeap.peek() ) / 2d;
        }

        return median;
    }

}
