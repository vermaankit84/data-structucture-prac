package com.algo.expert.classes.graphs.traversals;

import java.util.*;

public class NumberOfConnected {
    public static void main(String[] args) {

        int n = 5;
        //int[][] edges = {{0, 1}, {1, 2}, {2, 3},  {1, 3}, {1, 4}};

        int[][] edges = {{1,3}, {0,2}, {1,3}, {0,2}};;


        Graph graph = new Graph(n);
        Set<Integer> nodes = new HashSet<>();
        for (int[] edge : edges) {
            nodes.add(edge[0]);
            nodes.add(edge[1]);
            graph.addEdge(edge[0], edge[1]);
        }
        NumberOfConnected connected = new NumberOfConnected();
        System.out.println(connected.isBipriate(graph, nodes));



    }

    public void BFS(int vertex, Graph graph, boolean[] visisted) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(vertex);
        List<Integer>[] adjList = graph.adjList;
        visisted[vertex] = true;
        while (!queue.isEmpty()) {
            int element = queue.poll();
            for (int n : adjList[element]) {
                if (!visisted[n]) {
                    visisted[n] = true;
                    queue.add(n);
                }
            }
        }
    }



    public boolean isTree(Graph graph, Set<Integer> nodes) {
        Queue<Integer> queue = new LinkedList<>();
        List<Integer>[] adjList  =  graph.adjList;
        boolean[] visited = new boolean[nodes.size()];
        Arrays.fill(visited, false);
        int node = 0;
        for (int vertex: nodes) {
            queue.add(vertex);
            visited[vertex] = true;
            node = node + 1;

            while (!queue.isEmpty()) {
                int element = queue.poll();
                for(int n: adjList[element]) {

                    // this code will check for the cycle
                    if (queue.contains(n)) {
                        return false;
                    }

                    if (!visited[n]) {
                        queue.add(n);
                        visited[n] = true;
                    }
                }
            }
        }

        return node == nodes.size();
    }


    public boolean isBipriate(Graph graph, Set<Integer> vertex) {
        int [] colors = new int[vertex.size()];
        Queue<Integer> queue = new LinkedList<>();
        List<Integer>[] adjList = graph.adjList;
        for (int v: vertex) {
            colors[v] = 1;
            queue.add(v);

            while (!queue.isEmpty()) {
                int element = queue.poll();
                for(int n: adjList[element]) {
                    if (colors[n] == 0) {
                        colors[n] = (colors[element] == 1) ? 2 : 1;
                        queue.add(n);
                    } else {
                        if (colors[n] == colors[element]) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public int getConnectCount(Graph graph, Set<Integer> nodes ) {
        int  count = 0;
        boolean [] visited = new boolean[nodes.size()];
        Arrays.fill(visited, false);
        for (int node : nodes) {
            if (!visited[node]) {
                BFS(node, graph, visited);
                count = count + 1;
            }
        }
        return count;
    }
}


class Graph {
    public int vertex ;
    public List<Integer> adjList[];

    public Graph(int vertex) {
        this.vertex = vertex;
        adjList = new ArrayList[vertex];
        for (int i = 0; i < vertex; i++) {
            adjList[i] = new ArrayList<>();
        }
    }

    public void addEdge(int start , int end) {
        adjList[start].add(end);
        adjList[end].add(start);
    }
}
