package com.algo.expert.hard;

import com.google.common.collect.Lists;

import java.util.*;

public class SameBST {
    public static void main(String[] args) {

    }



    public static boolean sameBsts_2(List<Integer> arrayOne, List<Integer> arrayTwo) {
        SortedArrayToBST.BinaryTree bst = new SortedArrayToBST.BinaryTree();
        Integer[] intArray = new Integer[arrayOne.size()];
        Node node_1 = bst.sortedArrayToBst(arrayOne.toArray(intArray), 0, arrayOne.size());
        Integer[] intArray_2 = new Integer[arrayTwo.size()];
        Node node_2 = bst.sortedArrayToBst(arrayOne.toArray(intArray_2), 0, arrayOne.size());

        List<Integer> data_list = new ArrayList<>();
        preOrder(node_1, data_list);


        List<Integer> data_list_2 = new ArrayList<>();
        preOrder(node_2, data_list_2);

        return data_list.equals(data_list_2);
    }


   static void preOrder(Node root, List<Integer> dataList) {
        if (root == null) return;

        preOrder(root.left, dataList);
        dataList.add(root.data);
        preOrder(root.right, dataList);
    }



    public static boolean sameBsts(List<Integer> arrayOne, List<Integer> arrayTwo) {
        if (arrayOne.size() == 0 && arrayTwo.size() == 0) return true;

        if (arrayOne.size() != arrayTwo.size()) return false;

        if (!Objects.equals(arrayOne.get(0), arrayTwo.get(0))) return false;



        List<Integer> leftOne = getSmaller(arrayOne);
        List<Integer> rightOne = getSmaller(arrayTwo);


        List<Integer> leftOneGreaterThan = getGreaterThanEqual(arrayOne);
        List<Integer> rightOneGreaterThan = getGreaterThanEqual(arrayTwo);


        return sameBsts(leftOne, rightOne) && sameBsts(leftOneGreaterThan, rightOneGreaterThan);
    }

    private static List<Integer> getGreaterThanEqual(List<Integer> arrayOne) {
        List<Integer> response = new ArrayList<>();
        for (int i = 1; i < arrayOne.size(); i++) {
            if (arrayOne.get(i) >= arrayOne.get(0)) {
                response.add(arrayOne.get(i));
            }
        }

        return response;
    }

    private static List<Integer> getSmaller(List<Integer> arrayOne) {
        List<Integer> response = new ArrayList<>();
        for (int i = 1; i < arrayOne.size(); i++) {
            if (arrayOne.get(i) < arrayOne.get(0)) {
                response.add(arrayOne.get(i));
            }
        }

        return response;
    }

}
