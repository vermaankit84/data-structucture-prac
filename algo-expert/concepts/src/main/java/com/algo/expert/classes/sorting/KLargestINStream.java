package com.algo.expert.classes.sorting;

import com.google.common.collect.Lists;

import java.util.*;

public class KLargestINStream {
    public static void main(String[] args) {
        System.out.println(kth_largest(2, Lists.newArrayList(4, 6), Lists.newArrayList(5, 2, 20)));
    }

    public static List<Integer> kth_largest(int k, List<Integer> initial_stream, List<Integer> append_stream) {
       Queue<Integer> minHeapQueue = new PriorityQueue<>();
       minHeapQueue.addAll(initial_stream);
        List<Integer> output = new ArrayList<>();
       while (minHeapQueue.size() > k) {
           minHeapQueue.remove();
       }

        for (int i : append_stream) {
            minHeapQueue.add(i);
            if (minHeapQueue.size() > k) {
                minHeapQueue.poll();
            }
            output.add(minHeapQueue.peek());
        }


        return output;

    }

}
