package com.algo.expert.numbers;


import java.util.Arrays;


public class SortTheSquaredArray {
    public static void main(String[] args) {
        int[] arr = {-4, -3, 0, 1, 2, 5};
        System.out.println(Arrays.toString(sortTheSquaredArray(arr)));
    }


    public static int[] sortTheSquaredArray(int[] arr) {
        int [] temp = new int[arr.length];
        int right = arr.length - 1;
        int left = 0;
        int count = arr.length - 1;
        while (left <= right) {
            if ((arr[right] * arr[right] ) < (arr[left] * arr[left])) {
                temp[count] = (arr[left] * arr[left]);
                left = left + 1;
            } else {
                temp[count] = (arr[right] * arr[right]);
                right = right - 1;
            }
            count = count - 1;
        }
        return temp;
    }

}
