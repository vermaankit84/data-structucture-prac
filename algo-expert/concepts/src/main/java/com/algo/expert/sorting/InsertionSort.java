package com.algo.expert.sorting;

import java.util.Arrays;

public class InsertionSort {
    public static void main(String[] args) {
        int arr[] = {4, 5, 6, 3, 1, 6, 7};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
    // 4, 5, 6, 3
   // decrease and conquer also
    public static void sort(int arr[]) {
        for (int i = 0; i < arr.length; i = i + 1) {
            int temp = arr[i];
            int next = i - 1;
            while (next > -1 && temp < arr[next]) {
                arr[next + 1] = arr[next];
                next = next - 1;
            }
            arr[next + 1] = temp;
        }
    }
}
