package com.algo.expert.sorting;

import java.util.Arrays;

public class BubbleSort {
    // right to left if left element is greater than swap that
    public static void main(String[] args) {
        int arr[] = {4, 5, 6, 3, 7, 8, 1, 6, 2, 8};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int arr[]) {
        int start = arr.length - 1;
        for (int i = start; i > -1 ; i--) {
            for (int j = i - 1; j > -1; j--) {
                int e1 = arr[i];
                int e2 = arr[j];
                if (e1 < e2) {
                    swap(arr, i ,j);
                }
            }
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
