package com.algo.expert.easy;

import java.util.*;
import java.util.Map;

public class CheckSum {
    public static void main(String[] args) {
        int [][] nums = new int[][] {
                {5, 2, 8 ,9},
                {3, 9, 8, 5},
                {3, 5, 6, 8}
        };

        int sum = 0;
        for (int[] num : nums) {
            int[] test = new int[2];
            for (int i = 0; i < num.length; i++) {
                int test_num = num[i];
                for (int j = i + 1; j < num.length; j++) {
                    if (getCommonFactors(test_num, num[j])) {
                        test[0] = test_num;
                        test[1] = num[j];
                    }
                }
            }
            // System.out.println(Math.abs(test[0] - test[1]));
            sum = sum + Math.abs(test[0] - test[1]);
        }

        System.out.println(sum);
    }

    private static boolean getCommonFactors(int test_num, int num) {
        Set<Integer> factorsSet = new HashSet<>();

        for (int i = 2; i <=test_num ; i = i + 1) {
            if (test_num%i == 0) {
                factorsSet.add(i);
            }
        }

        for (int i = 2; i <= num; i++) {
            if (num%i == 0) {
                if (!factorsSet.add(i)) {
                    return true;
                }
            }
        }

        return false;
    }
}
