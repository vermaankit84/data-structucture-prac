package com.algo.expert.classes.dynamic_programming;

public class MaximumPathSum {
    public static void main(String[] args) {
        int mat[][] = {{1, 3, 1},
                {1, 5, 1},
                {4, 2, 1}};
        System.out.println(max_path_sum(mat));
    }

    public static int max_path_sum(int mat[][]) {
        int[][] dp = new int[mat.length][mat[0].length];
        dp[0][0] = mat[0][0];

        for(int row = 1; row < mat.length; row = row + 1 ) {
            dp[row][0] = dp[row - 1][0] + mat[row][0];
        }

        for(int col = 1; col < mat[0].length ; col = col + 1 ) {
            dp[0][col] = dp[0][col - 1] + mat[0][col];
        }

        for(int row = 1; row < mat.length ; row = row + 1) {
            for (int col = 1; col < mat[0].length; col = col + 1) {
                dp[row][col] = mat[row][col] + Math.max(dp[row - 1][col], dp[row][col - 1]);
            }
        }

        return dp[mat.length -1][mat[0].length - 1];
    }
}
