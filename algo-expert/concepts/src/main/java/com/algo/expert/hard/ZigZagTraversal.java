package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.List;

public class ZigZagTraversal {


    public static List<Integer> zigzagTraverse(List<List<Integer>> array) {
        int row = 0;
        int col = 0;

        int height = array.size() - 1;
        int width = array.get(0).size() - 1;
        boolean isGoingDown = true;

        List<Integer> result = new ArrayList<>();

        while(!isOutOfBounds(row, col, height, width)) {
            result.add(array.get(row).get(col));
            if (isGoingDown) {
                if (col == 0 || row == height) {
                    isGoingDown = false;
                    if (row == height) {
                        col = col + 1;
                    }
                    else {
                        row ++;
                    }
                } else  {
                    row = row + 1;
                    col = col - 1;
                }
            } else  {
                if (row == 0 || col == width) {
                    isGoingDown = true;
                    if (col == width) {
                        row++;
                    } else  {
                        col++;
                    }
                } else  {
                    row = row - 1;
                    col = col + 1;
                }
            }
        }
        return result;
    }

    private static boolean isOutOfBounds(int row, int col, int height, int width) {
        return row < 0 || row > height || col < 0 || col > width;
    }
}
