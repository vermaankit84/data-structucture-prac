package com.algo.expert.popular;

public class TappingRainWater {
    public static void main(String[] args) {
        System.out.println(trap(new int[] {0,1,0,2,1,0,1,3,2,1,2,1}));
    }

    public static int trap(int[] height) {
        int trap = 0;
        for (int idx = 0; idx < height.length - 1; idx+=1) {
            trap = trap +  findMin(height, idx) - height[idx];
        }
        return trap;
    }

    private static int findMin(int[] height, int i) {
        int mLeft  = 0;
        for (int j = i ; j < height.length; j++) {
            if (height[j] > mLeft) {
                mLeft = height[j];
            }
        }
        int mRight = 0;
        for (int j = i ; j > -1 ; j--) {
            if (height[j] > mRight) {
                mRight = height[j];
            }
        }
        return Math.min(mLeft, mRight);
    }
}

class Pair {
    int rightMax;
    int leftMax;

    @Override
    public String toString() {
        return "Pair{" +
                "rightMax=" + rightMax +
                ", leftMax=" + leftMax +
                '}';
    }
}
