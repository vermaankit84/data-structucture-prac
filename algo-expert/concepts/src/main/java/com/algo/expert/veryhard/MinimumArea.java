package com.algo.expert.veryhard;

import java.util.*;
public class MinimumArea {
    public static void main(String[] args) {

    }


    public int minimumAreaRectangle(int[][] points) {
        Set<String> pointSet = createPointSet(points);
        int minimum_area_found = Integer.MAX_VALUE;

        for (int i = 0; i < points.length; i++) {
            int p2x = points[i][0];
            int p2y = points[i][1];

            for (int j = 0; j < i; j++) {
                int p1x = points[j][0];
                int p1y = points[j][1];

                if (p1x == p2x || p1y == p2y) continue;

                boolean point_1_exists = pointSet.contains(p1x + ":" + p2y);
                boolean point_2_exists = pointSet.contains(p2x + ":" + p1y);

                if (point_1_exists && point_2_exists) {
                    int currentArea = Math.abs(p1x - p2x) *  Math.abs(p2y - p1y);
                    minimum_area_found = Math.min(minimum_area_found, currentArea);
                }
            }
        }

        return minimum_area_found!= Integer.MAX_VALUE ? minimum_area_found : 0;
    }

    private Set<String> createPointSet(int[][] points) {
        Set<String> set = new HashSet<>();

        for (int[] point: points) {
            int x = point[0];
            int y = point[1];
            set.add(x + ":" + y);
        }

        return set;
    }
}


