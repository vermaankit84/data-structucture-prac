package com.algo.expert.classes.concurrency;

public class ConcurrencyRocks {
    public static synchronized void main(String[] args) {
        Thread t = new Thread(ConcurrencyRocks::bar);
        t.run();
        System.out.println("foo ");
    }

    static synchronized void bar() {
        System.out.println("bar ");
    }
}
