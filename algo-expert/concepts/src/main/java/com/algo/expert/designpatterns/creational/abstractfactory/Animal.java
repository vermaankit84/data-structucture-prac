package com.algo.expert.designpatterns.creational.abstractfactory;

public interface Animal {
    String getType();
    String makeSound();
}
