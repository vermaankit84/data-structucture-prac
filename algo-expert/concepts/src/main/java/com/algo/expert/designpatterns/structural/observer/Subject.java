package com.algo.expert.designpatterns.structural.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject implements SubjectInterface {
    private int flag;
    private final List<Observer> observerList = new ArrayList<>();

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
        notify(flag);
    }

    @Override
    public void register(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notify(int notified) {
        for (Observer ob :this.observerList) {
            ob.update(notified);
        }
    }
}
