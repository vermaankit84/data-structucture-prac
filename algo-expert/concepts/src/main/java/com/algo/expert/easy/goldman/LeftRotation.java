package com.algo.expert.easy.goldman;

import java.util.Arrays;

public class LeftRotation {
    public static void main(String[] args) {
        int d = 2;
        int[] a = {1,2,3,4,5,6};
        int[] output = {5,6,1,2,3,4};
        int[] res = arrayLeftRotation(a,d);

        if (Arrays.equals(output, res))
            System.out.println("Test Passed");
        else
            System.out.println("Test Failed");
    }

    private static int[] arrayLeftRotation(int[] a, int d) {
        int[] output = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            int next = (i + d) % a.length;
            output[next] = a[i];
        }
        return output;
    }
}
