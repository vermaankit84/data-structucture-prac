package com.algo.expert.classes.dynamic_programming;

public class RodCutting {
    public static void main(String[] args) {
        int length = 6;
        int [] prices = {0, 1, 3, 3, 8, 9, 10};

        int max_cutting_rod = max_cutting_rod(length, prices);
        System.out.println(max_cutting_rod);

        /*
            max_cutting_rod = max_cutting_rod_recursive(length, prices);
            System.out.println(max_cutting_rod);
        */
    }

    /**
     * 
     *  f(L) = max (prices[cut] + f (l - cut)) 
     */

    private static int max_cutting_rod(int length, int [] prices) {
        int dp[] = new int[length + 1];
        dp[0] = 0;

        for(int i = 1; i < length + 1; i = i + 1) {
           int result = -1;
           for(int cut = 1 ; cut < i; cut = cut + 1) {
                result = Math.max(result, prices[cut] + dp[i - cut - 1]);
           }
           dp[i] = result;
        }
        return dp[length];
    }
/*
    private static int max_cutting_rod_recursive(int length, int [] prices) {
        if (length == 0) {
            return 0;
        }
        if (length == 1) {
            return prices[1];
        }

        int result = -1;
        for( int i = 2; i < length ; i = i + 1) {
            result = Math.max(result,prices[i] + max_cutting_rod_recursive(length - i, prices));
        }
        return result;
    }
    */
} 
