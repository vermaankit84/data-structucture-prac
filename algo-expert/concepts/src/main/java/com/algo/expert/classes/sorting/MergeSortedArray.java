package com.algo.expert.classes.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSortedArray {
    public static void main(String[] args) {
        int []arr2 = {0, 3, 5, 7, 0, 0, 0};

        int []arr1 = {2, 4, 6};

        merger_first_into_second(arr1, arr2);
    }

    static void merger_first_into_second(int[] arr1, int[] arr2) {

        List<Integer> list_1 = new ArrayList<>();
        List<Integer> list_2 = new ArrayList<>();

        for (int i = 0; i < arr1.length ; i++) {
            if (arr1[i] == 0 ) {
                if (((i + 1) < arr1.length) && (arr1[i + 1] != 0)) {
                    list_1.add(arr1[i]);
                }
            } else  {
                list_1.add(arr1[i]);
            }
        }

       // System.out.println(list_1);

        for (int i = 0; i < arr2.length ; i++) {
            if (arr2[i] == 0 ) {
                if (((i + 1) < arr2.length) && (arr2[i + 1] != 0)) {
                    list_2.add(arr2[i]);
                }
            } else  {
                list_2.add(arr2[i]);
            }
        }

        //System.out.println(list_2);

        int i = 0;
        int j = 0;
        int counter = 0;
        int output[] = new int[list_1.size() +  list_2.size()];
        while (i < list_1.size() && j < list_2.size()) {
            if (list_1.get(i) < list_2.get(j)) {
                output[counter] = list_1.get(i);
                i = i + 1;
            } else  {
                output[counter] = list_2.get(j);
                j =  j+ 1;
            }
            counter = counter + 1;
        }

        while (i < list_1.size()) {
            output[counter] = list_1.get(i);
            counter = counter + 1;
            i = i + 1;
        }

        while (j  < list_2.size()) {
            output[counter] = list_2.get(j);
            counter = counter + 1;
            j = j + 1;
        }

        if (arr1.length > arr2.length) {
            for (int k = 0; k < output.length; k++) {
                arr1[k] = output[k];
            }
        } else  {
            for (int k = 0; k < output.length; k++) {
                arr2[k] = output[k];
            }
        }

    }
}
