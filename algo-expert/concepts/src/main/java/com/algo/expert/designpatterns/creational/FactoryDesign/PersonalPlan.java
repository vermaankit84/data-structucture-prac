package com.algo.expert.designpatterns.creational.FactoryDesign;

public class PersonalPlan implements Plan{
    @Override
    public void createPlan() {
        System.out.println("In Personal Plan");
    }
}
