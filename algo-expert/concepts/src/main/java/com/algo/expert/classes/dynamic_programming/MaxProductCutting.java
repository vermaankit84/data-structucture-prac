package com.algo.expert.classes.dynamic_programming;

public class MaxProductCutting {
    public static void main(String[] args) {
        int n = 10;
        System.out.println(max_product_cutting(n));
    }

    // result = Math.max(result, Math.max((i-j)*j, j*dp[i-j]));
    private static int max_product_cutting(int n) {
        int dp[] = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        for(int i = 2; i < n + 1; i = i + 1) {
            int result = - 1;
            for(int j = 0; j < i + 1; j = j + 1) {
                result = Math.max(result, Math.max((i-j)*j, j*dp[i-j]));
            }

            dp[i] = result;
        }

        return dp[n];
    }

}
