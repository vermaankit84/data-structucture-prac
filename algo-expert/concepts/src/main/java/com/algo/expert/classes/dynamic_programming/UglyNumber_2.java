package com.algo.expert.classes.dynamic_programming;


import java.util.HashMap;
import java.util.Map;

public class UglyNumber_2 {
    public static void main(String[] args) {
        //System.out.println(nthUglyNumber(11));
        //System.out.println(nthUglyNumber(1));
        System.out.println(nthUglyNumber(1352));
    }

    private  static final Map<Integer, Boolean> memo = new HashMap<>();


    public static int nthUglyNumber(int n) {
        int[]dp=new int[n+1];
        dp[1]=1;
        int f1=dp[1];
        int f2=dp[1];
        int f3=dp[1];

        for(int i=2;i<=n;i++){
            int two=2*dp[f1];
            int three=3*dp[f2];
            int five=5*dp[f3];

            int min=Math.min(two,Math.min(three,five));
            dp[i]=min;

            if(min==two){
                f1++;
            }
            if(min==three){
                f2++;
            }

            if(min==five){
                f3++;
            }
        }

        return dp[n];
    }

    private static boolean isValid(int num) {
        int temp = num;


       while(num > -1) {
           if (memo.get(num)!=null) {
               return memo.get(num);
           }
           if (num == 1) {
               memo.put(temp, true);
               return true;
           }

            if (num%5 == 0) {
                num = num/5;
            } else if (num%3 ==0) {
                num = num/3;
            } else if (num%2 == 0) {
                num = num/2;
            } else {
                memo.put(temp, false);
                return false;
            }
       }

       return true;
    }

}
