package com.algo.expert.designpatterns.structural.observer;

public interface Observer {
    void update(int updatedValue);
}
