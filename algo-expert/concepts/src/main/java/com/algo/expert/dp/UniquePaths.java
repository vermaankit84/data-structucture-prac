package com.algo.expert.dp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UniquePaths {
    public static void main(String[] args) {
        Map<Pair, Integer> memo = new HashMap<>();
        int[][] path = new int[4][8];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 7; j++) {
                path[i][j] = 0;
            }
        }
        System.out.println(uniquePaths(3, 7, memo, path));
    }

    public static int uniquePaths(int m, int n, Map<Pair, Integer> memo, int[][] path) {

        if ((m == 0) || (n == 0)) {
            return 0;
        }

        if (path[m][n] == 1) {
            return 0;
        }

        if ((m == 1) && (n == 1)) {
            return 1;
        }

        if (memo.get(new Pair(m, n))!=null) {
            return memo.get(new Pair(m , n));
        }


        memo.put(new Pair(m, n) , uniquePaths(m, n - 1, memo, path) + uniquePaths(m - 1, n, memo, path));
        return memo.get(new Pair(m, n));
    }

    static class Pair {
        int m;
        int n;

        public Pair(int m, int n) {
            this.m = m;
            this.n = n;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pair)) return false;
            Pair pair = (Pair) o;
            return m == pair.m && n == pair.n;
        }

        @Override
        public int hashCode() {
            return Objects.hash(m, n);
        }
    }

}
