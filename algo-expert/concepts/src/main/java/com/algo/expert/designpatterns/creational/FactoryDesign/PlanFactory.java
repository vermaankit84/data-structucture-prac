package com.algo.expert.designpatterns.creational.FactoryDesign;

public class PlanFactory {

    public static Plan getPlan(String plan) {
        switch (plan.toLowerCase()) {
            case "domestic":
                return new DomesticPlan();
            case "personal":
                return new PersonalPlan();
            case "business":
                return new BusinessPlan();
            default:
                return null;
        }
    }
}
