package com.algo.expert.test;

import java.util.*;

/**
 * Input:
 * [[0, 30],[5, 10],[15, 20]]
 * Output:
 *  2
 *
 *  0 , 30
 *  5, 10,
 *  15 , 20
 */
public class MeetingRooms {
    public static void main(String[] args) {
        int [][] meetings = {{7, 10}, {2, 4}};
        List<Meeting> meetingList = new ArrayList<>();
        for (int[] meeting : meetings) {
            meetingList.add(new Meeting(meeting[0], meeting[1]));
        }
        meetingList.sort(Comparator.comparingInt(m -> m.start));
        PriorityQueue<Meeting> queue = new PriorityQueue<>((o1, o2) -> Integer.compare(o2.start, o1.start));
        queue.add(meetingList.get(0));
        for (int i = 1; i < meetingList.size(); i++) {
            Meeting current_meeting = meetingList.get(i);
            Meeting top_meeting = queue.peek();
            if (top_meeting!= null && top_meeting.end > current_meeting.end) {
                queue.add(current_meeting);
            } else {
                Meeting current_top_meeting = queue.remove();
                current_top_meeting.end = current_meeting.end;
                queue.add(current_top_meeting);
            }
        }

        System.out.println(queue.size());


    }

    static class Meeting {
        int start;
        int end;
        public Meeting(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
}
