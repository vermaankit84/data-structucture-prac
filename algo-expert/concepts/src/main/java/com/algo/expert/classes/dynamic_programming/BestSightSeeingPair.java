package com.algo.expert.classes.dynamic_programming;

public class BestSightSeeingPair {
    public static void main(String[] args) {
        int values[] = {8,1,5,2,6};
        System.out.println(maxScoreSightseeingPair(values));
    }

    public static int maxScoreSightseeingPair(int[] values) {
        int max = values[0];
        int ans = 0;

        for (int i = 1; i < values.length; i++) {
            int score = max + values[i] - i;
            ans = Math.max(ans, score);
            max = Math.max(max, values[i] + i);
        }


        return ans;
    }
}
