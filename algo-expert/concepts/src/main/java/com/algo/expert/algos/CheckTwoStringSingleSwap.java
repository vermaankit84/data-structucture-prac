package com.algo.expert.algos;

import java.util.ArrayList;
import java.util.List;

public class CheckTwoStringSingleSwap {
    public static void main(String[] args) {
        System.out.println(areAlmostEqual("bank", "kanb"));
        System.out.println(areAlmostEqual("attack", "defend"));
        System.out.println(areAlmostEqual("kleb", "kleb"));
    }

    public static boolean areAlmostEqual(String s1, String s2) {
        List<Integer> cnt = new ArrayList<>();
        for(int i=0;i<s1.length() && cnt.size()<3;i++){
            if(s1.charAt(i)!=s2.charAt(i)){
                cnt.add(i);
            }
        }

        return (cnt.size()==0) || ((cnt.size()==2) && (s1.charAt(cnt.get(0))==s2.charAt(cnt.get(1)) || s1.charAt(cnt.get(1))==s2.charAt(cnt.get(0))));
    }
}
