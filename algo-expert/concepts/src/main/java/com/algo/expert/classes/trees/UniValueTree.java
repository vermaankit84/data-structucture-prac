package com.algo.expert.classes.trees;

import java.util.ArrayList;
import java.util.List;

public class UniValueTree {
    public static void main(String[] args) {
        TreeNode root = TreeUtils.buildTree();
        System.out.println(countUniValue(root));
    }

    private static  Integer countUniValue(TreeNode root) {
        List<Integer> count = new ArrayList<>();
        uniValue(root, count);
        System.out.println(count);
        return count.size();
    }

    private static boolean uniValue(TreeNode root ,List<Integer> count) {
        if (root == null) {
            return false;
        }

        boolean is_left =   uniValue(root.left, count);
        boolean is_right  = uniValue(root.right, count);
        if (!is_left || !is_right) {
            return false;
        }

        if (root.getLeft()!=null && root.getLeft().value!=root.value) {
            return false;
        }

        if (root.getRight()!=null && root.getRight().value!=root.value) {
            return false;
        }

        count.add(1);

        return true;
    }
}
