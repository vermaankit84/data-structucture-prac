package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.*;

public class DistintInteger {
    public static void main(String[] args) {
        List<Integer> list = Lists.newArrayList(1, 2,3);
        List<List<Integer>> result = new ArrayList<>();

        distintInteger(list, 0, new Stack<>(), result);
        System.out.println(result);
    }

    public static void  distintInteger(List<Integer> set, int index, Stack<Integer> partial , List<List<Integer>> results) {
        if (index == set.size()) {
            results.add(new ArrayList<>(partial));
            return;
        }

        distintInteger(set, index + 1, partial, results);
        // snadwitch pattern
            partial.push(set.get(index));
            distintInteger(set, index + 1, partial, results);
            partial.pop();
        // snadwitch pattern

    }
}
