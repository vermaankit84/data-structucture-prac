package com.algo.expert.dp;

import java.util.Arrays;

public class RodCutting {
    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{1, 4, 8, 9, 10, 17, 17, 20}));
        System.out.println(maxProfit(new int[]{1, 4}));
        System.out.println(maxProfit(new int[]{1}));
    }


    public static int maxProfit(int [] arr) {
        if (arr.length == 0) return 0;

        if (arr.length == 1) {
            return arr[0];
        }

        if (arr.length == 2) {
            return Math.max(arr[0], arr[1]);
        }

        int np[] = new int[arr.length + 1];
        np[0] = 0;
        for (int i = 1; i < arr.length + 1; i++) {
            np[i] = arr[i - 1];
        }

        int dp[] = new int[arr.length];
        dp[0] = 0;
        dp[1] = np[1];

        for (int i = 2; i < dp.length; i++) {
            dp[i] = np[i];
            int left = 1;
            int right = i - 1;
            while (left <= right) {
                if (dp[left] + dp[right] > dp[i]) {
                    dp[i] = dp[left] + dp[right];
                }
                left += 1;
                right -= 1;
            }
        }

        System.out.println(Arrays.toString(dp));
        return dp[dp.length - 1];
    }
}
