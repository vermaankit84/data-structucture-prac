package com.algo.expert.medium;

import java.util.*;

public class GenerateParenthesis {
    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));
    }

    public static List<String> generateParenthesis(int n) {
        List<String> parentThesis = new ArrayList<>();
        helper(parentThesis, "", 0, 0, n);
        return parentThesis;
    }

    private static void helper(List<String> result, String slate, int open, int close, int n) {
        if(open == n && close == n) {
            result.add(slate);
            return;
        }

        if (open < n) {
            helper(result, slate + "(", open + 1, close,  n);
        }

        if (close < open) {
            helper(result, slate + ")", open, close + 1,  n);
        }
    }
    
}
