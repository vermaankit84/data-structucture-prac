package com.algo.expert.classes.graphs;

import java.util.ArrayList;
import java.util.List;


public class LargestIsland {
    public static void main(String[] args) {
        int [][] grid = {
                { 1, 1, 0 },
                { 1, 1, 0},
                {0, 0, 1}
        };

        List<List<Integer>> matrix = new ArrayList<>();
        for (int[] ints : grid) {
            List<Integer> temp = new ArrayList<>();
            for (int anInt : ints) {
                temp.add(anInt);
            }
            matrix.add(temp);
        }

        System.out.println(max_island_size(matrix));
    }

    public static int max_island_size(List<List<Integer>> grid) {
        int max = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid.get(i).size(); j++) {
                if (grid.get(i).get(j) == 1) {
                    java.util.concurrent.atomic.AtomicInteger size = new java.util.concurrent.atomic.AtomicInteger(0);
                    helper(grid, i, j, size);
                    max = Math.max(max, size.intValue());
                }
            }
        }
        return max;
    }

    private static void helper(List<List<Integer>> grid, int row, int col, java.util.concurrent.atomic.AtomicInteger size) {
        if (row < 0 || row >= grid.size() || col < 0 || col >= grid.get(row).size() || grid.get(row).get(col) == 0) {
            return;
        }

        grid.get(row).set(col, 0);
        size.incrementAndGet();
        helper(grid, row + 1, col, size);
        helper(grid, row - 1, col, size);
        helper(grid, row, col - 1, size);
        helper(grid, row, col + 1, size);
    }
}
