package com.algo.expert.designpatterns.creational.prototype;

import java.util.HashMap;
import java.util.Map;

public class ShapeCache {
    private static final Map<String, Shape> shapeMap = new HashMap<>();

    public static Shape getShape(String id) {
        return (Shape) shapeMap.get(id).clone();
    }

    static {
        Circle circle = new Circle();
        circle.setId("1");
        shapeMap.put(circle.getId(), circle);

        Square square = new Square();
        square.setId("2");
        shapeMap.put(square.getId(), square);
    }
}
