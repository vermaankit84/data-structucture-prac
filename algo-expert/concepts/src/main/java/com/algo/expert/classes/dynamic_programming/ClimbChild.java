package com.algo.expert.classes.dynamic_programming;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

public class ClimbChild {
    public static void main(String[] args) {
        System.out.println(no_of_ways_climb_child(1, Lists.newArrayList(1, 2)));
        System.out.println(no_of_ways_climb_child(2, Lists.newArrayList(1, 2)));
        System.out.println(no_of_ways_climb_child(7, Lists.newArrayList(2, 3)));
    }

    public static long no_of_ways_climb_child(int n, List<Integer> steps) {
        long ways[] = new long[n + 1];
        ways[0] = 1;
        for(int i = 1; i < (n + 1); i = i + 1) {
            for(int step : steps) {
                    if (i - step>= 0) {
                        ways[n] = ways[n] + ways[i - step];
                    }
                }
        }
        System.out.println(Arrays.toString(ways));
        return ways[n];
    }
}
