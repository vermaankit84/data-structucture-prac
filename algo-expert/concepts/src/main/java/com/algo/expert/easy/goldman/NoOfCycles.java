package com.algo.expert.easy.goldman;

// [3, 2, 1, 0]  --> 2
public class NoOfCycles {
    public static void main(String[] args) {
        System.out.println(noOfCycles(new int[]{3, 2, 1, 0}));
        System.out.println(noOfCycles(new int[]{2, 0, 1}));
        System.out.println(noOfCycles(new int[]{1, 3, 4, 0, 2, 6, 5}));
    }

    private static int noOfCycles(int []arr) {
        Pair[] pairs = new Pair[arr.length];
        for (int i = 0; i < arr.length; i++) {
            pairs[i] = new Pair(i, arr[i]);
        }

        boolean []visited = new boolean[arr.length];
        int noOfCycles = 0;

        for (int i = 0; i < arr.length; i++) {
            if (pairs[i].getValue()!=i) {
                if (!visited[i]) {
                    findCycleExists(pairs, i, visited);
                    noOfCycles = noOfCycles + 1;
                }
            }
        }
        return noOfCycles;
    }

    // 0, 3
    private static void findCycleExists(Pair[] pair, int start, boolean[]visited) {
        visited[start] = true;
        int next = pair[start].getValue();
        if (!visited[next]) {
            findCycleExists(pair, next, visited);
        }
    }

}

class Pair {
    private final int index;
    private final int value;

    Pair(int index, int value) {
        this.index = index;
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}