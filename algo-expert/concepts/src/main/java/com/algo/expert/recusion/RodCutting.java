package com.algo.expert.recusion;

public class  RodCutting {
    public static void main(String[] args) {
        int arr[] = new int[] {1, 5, 8, 9, 10, 17, 17, 20};
        int size = arr.length;
        System.out.println("Maximum Obtainable Value is " +
                cutRod(arr, size));
    }

    private static int cutRod(int[] arr, int size) {
        int val[] = new int[size  + 1];
        val[0] = 0;
        for (int i = 1; i <= size; i++) {
            int max_val = Integer.MIN_VALUE;
            for (int j = 0; j < i; j++) {
                max_val = Math.max(max_val, arr[j] + val[i - j -1]);
            }
            val[i] = max_val;
        }
        return val[size];
    }


}
