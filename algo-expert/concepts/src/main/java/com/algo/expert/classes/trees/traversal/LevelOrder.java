package com.algo.expert.classes.trees.traversal;

import com.algo.expert.classes.trees.TreeNode;
import com.algo.expert.classes.trees.TreeUtils;

import java.util.*;
import java.util.Queue;

public class LevelOrder {
    public static void main(String[] args) {
        TreeNode root = TreeUtils.buildTree();
        levelOrder(root);
    }

    private static void levelOrder(TreeNode root) {

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        List<List<Integer>> output = new ArrayList<>();
        while (!queue.isEmpty()) {
            TreeNode temp = queue.poll();

            if (temp.getLeft()!=null) {
                queue.add(temp.getLeft());
            }

            if (temp.getRight()!=null) {
                queue.add(temp.getRight());
            }
        }

        System.out.println(output);
        //System.out.println(levels.reverse().toString().trim());
    }
}
