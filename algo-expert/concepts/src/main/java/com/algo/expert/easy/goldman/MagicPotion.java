package com.algo.expert.easy.goldman;

public class MagicPotion {
    public static void main(String[] args) {
        if (minimalSteps("ABCDABCE") == 8 && minimalSteps("ABCABCE") == 5 && minimalSteps("AAA") == 3
                && minimalSteps("AAAA") == 3 && minimalSteps("BBB") == 3 && minimalSteps("AAAAAA") == 5) {
            System.out.println("Pass");
        } else {
            System.out.println("Fail");
        }
    }

    private static Integer minimalSteps(String ingredients) {
        if (ingredients.length() == 0) return 0;

        StringBuilder magicPotion = new StringBuilder();
        magicPotion.append(ingredients.charAt(0));

        for (int i = 1; i < ingredients.length(); i++) {
            if ((i * 2 <= ingredients.length()) && (ingredients.substring(0, i).equalsIgnoreCase(ingredients.substring(i , 2 * i)))) {
                magicPotion.append("*");
                i = 2 * i - 1;
            } else  {
                magicPotion.append(ingredients.charAt(i));
            }
        }

        System.out.println(ingredients + "\t" + magicPotion);
        return magicPotion.length();
    }
}
