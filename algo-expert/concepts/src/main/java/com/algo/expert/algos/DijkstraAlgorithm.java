package com.algo.expert.algos;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DijkstraAlgorithm {
    public static void main(String[] args) {

    }
     // O(v^2 + e) | O (v)
    public int[] dijkstrasAlgorithm(int start, int[][][] edges) {
        int noOfVertices = edges.length;
        int [] minDistances = new int[noOfVertices];

        Arrays.fill(minDistances, Integer.MAX_VALUE);
        minDistances[start] = 0;

        Set<Integer> visited = new HashSet<>();

        while (visited.size() != noOfVertices) {
            int [] getVertexData = getVertexWithMinDistances(minDistances, visited);
            int vertex = getVertexData[0];
            int currentMinDistances = getVertexData[1];

            if (currentMinDistances == Integer.MAX_VALUE) {
                break;
            }

            visited.add(vertex);

            for(int[] edge : edges[vertex]) {
                int destination = edge[0];
                int distanceToDestination = edge[1];

                if (visited.contains(destination)) continue;

                int newPathDistance = currentMinDistances + distanceToDestination;
                int currentDestinationDistance = minDistances[destination];
                if (newPathDistance < currentDestinationDistance) {
                    minDistances[destination] = newPathDistance;
                }
            }
        }

        int[] finalDistances = new int[minDistances.length];
        for (int i = 0; i < finalDistances.length; i++) {
            int distance = minDistances[i];
            if (distance == Integer.MAX_VALUE) {
                finalDistances[i] = -1;
            } else  {
                finalDistances[i] = distance;
            }
        }
        return finalDistances;
    }

    private int[] getVertexWithMinDistances(int[] minDistances, Set<Integer> visited) {
        int currentMinDistance = Integer.MAX_VALUE;
        int vertex = -1;
        for (int vertexId = 0; vertexId < minDistances.length; vertexId++) {
            int distance = minDistances[vertexId];
            if (visited.contains(vertexId)) continue;

            if (distance <= currentMinDistance) {
                vertex = vertexId;
                currentMinDistance = distance;
            }
        }

        return new int[] {vertex, currentMinDistance};
    }
}
