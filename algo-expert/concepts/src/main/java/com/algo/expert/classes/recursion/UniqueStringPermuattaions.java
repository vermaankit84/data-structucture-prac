package com.algo.expert.classes.recursion;

import java.util.*;

public class UniqueStringPermuattaions {
    public static void main(String[] args) {
        String str = "ABC";
        List<String> output = getUniqueStringPermuattaions(str);

        System.out.println(output);
    }

    private static List<String> getUniqueStringPermuattaions(String str) {
        List<String> response = new ArrayList<>();
        List<String> input = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            input.add(String.valueOf(str.charAt(i)));
        }
        helper(input, 0, new Stack<>(), response);
        return response;
    }

    private static void helper(List<String> input, int idx, Stack<String> slate, List<String> response) {
        if (idx == input.size()) {
            response.add(String.join("",slate));
            return;
        }

        for (int i = idx; i < input.size(); i++) {
            swap(input, idx, i);
            slate.add(input.get(idx));
            helper(input, idx + 1, slate, response);
            slate.pop();
            swap(input, idx, i);
        }
    }

    private static void swap(List<String> input, int idx, int i) {
        Collections.swap(input, idx, i);
    }
}
