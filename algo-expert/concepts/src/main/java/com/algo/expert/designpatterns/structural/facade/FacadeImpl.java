package com.algo.expert.designpatterns.structural.facade;

public class FacadeImpl {
    public static void main(String[] args) {
        ShapeMaker maker = new ShapeMaker();
        maker.drawCircle();
        maker.drawRectangle();
    }
}
