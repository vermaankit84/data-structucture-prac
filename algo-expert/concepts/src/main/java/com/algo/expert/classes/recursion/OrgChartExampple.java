package com.algo.expert.classes.recursion;

import com.algo.expert.classes.trees.YoungestCommonAncestor;

import java.util.*;

public class OrgChartExampple {
    public static void main(String[] args) {
       OrgChart A = new OrgChart('A');

       OrgChart B = new OrgChart('B');
       OrgChart C = new OrgChart('C');

        A.addDirectReports(new OrgChart[]{B, C});

        OrgChart D = new OrgChart('D');
        OrgChart E = new OrgChart('E');
        B.addDirectReports(new OrgChart[] {D, E});

        OrgChart F = new OrgChart('F');
        OrgChart G = new OrgChart('G');
        C.addDirectReports(new OrgChart[] {F, G});

        OrgChart H = new OrgChart('H');
        OrgChart I = new OrgChart('I');
        D.addDirectReports(new OrgChart[] { H , I});

        System.out.println(getLowestCommonManager(A, E, I));


    }


    public static OrgChart getLowestCommonManager(OrgChart topManager, OrgChart reportOne, OrgChart reportTwo) {

        List<OrgChart> getParentList_report_one = new ArrayList<>();
        getParentList_report_one.add(topManager);
        helper(topManager, topManager, new Stack<>(), getParentList_report_one, reportOne.name);

        List<OrgChart> get_parentList_report_two = new ArrayList<>();
        get_parentList_report_two.add(topManager);
        helper(topManager, topManager, new Stack<>(), get_parentList_report_two, reportTwo.name);

        Collections.reverse(getParentList_report_one);
        Collections.reverse(get_parentList_report_two);



        while (getParentList_report_one.size() > get_parentList_report_two.size()) {
            get_parentList_report_two.add(0, new OrgChart('#'));
        }

        while (get_parentList_report_two.size() > getParentList_report_one.size()) {
            getParentList_report_one.add(0, new OrgChart('#'));
        }

        System.out.println(getParentList_report_one + "  " + get_parentList_report_two);

        for (int i = 0; i < getParentList_report_one.size(); i++) {
            char c1 = getParentList_report_one.get(i).name;
            char c2 = get_parentList_report_two.get(i).name;

            if (c2 == c1) {
                return getParentList_report_one.get(i);
            }
        }

        return null;
    }


    public static void  helper(OrgChart manager, OrgChart repoteee, Stack<OrgChart> slate, List<OrgChart> orgCharts,  Character reporteeName) {
        if (reporteeName == repoteee.name) {
            orgCharts.addAll(slate);
            return;
        }


        List<OrgChart> repotees = repoteee.directReports;
        for (OrgChart node : repotees) {
            slate.add(node);
            helper(manager, node, slate, orgCharts, reporteeName);
            slate.pop();
        }
    }

    static class OrgChart {
        public char name;
        public List<OrgChart> directReports;

        OrgChart(char name) {
            this.name = name;
            this.directReports = new ArrayList<OrgChart>();
        }

        public String toString() {
            return this.name + "";
        }

        // This method is for testing only.
        public void addDirectReports(OrgChart[] directReports) {
            this.directReports.addAll(Arrays.asList(directReports));
        }
    }
}
