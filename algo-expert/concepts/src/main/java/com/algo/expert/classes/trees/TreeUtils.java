package com.algo.expert.classes.trees;

/**
 *
 *             1
 *        2          3
 *      4   5      6      7
 */
public class TreeUtils {

    public static TreeNode buildTree() {
        TreeNode root = new TreeNode();
        root.value  = 1;

        TreeNode left_node_root = new TreeNode();
        left_node_root.value = 2;


        TreeNode right_node_root = new TreeNode();
        right_node_root.value = 3;

        root.left = left_node_root;
        root.right = right_node_root;


        TreeNode left_node_root_2 = new TreeNode();
        left_node_root_2.value = 4;
        left_node_root.left = left_node_root_2;

        TreeNode right_node_root_2 = new TreeNode();
        right_node_root_2.value = 5;
        left_node_root.right = right_node_root_2;


        TreeNode left_node_root_3 = new TreeNode();
        left_node_root_3.value = 6;
        right_node_root.left = left_node_root_3;

        TreeNode right_node_root_3 = new TreeNode();
        right_node_root_3.value = 7;
        right_node_root.right = right_node_root_3;


        return root;
    }
}
