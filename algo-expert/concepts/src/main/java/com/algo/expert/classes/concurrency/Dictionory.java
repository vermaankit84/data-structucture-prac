package com.algo.expert.classes.concurrency;

import java.util.HashMap;
import java.util.Map;

public class Dictionory {

    private static  Dictionory instance = null;

    private Dictionory() {}

    private final Map<String, String> dictionary = new HashMap<>();

    public static synchronized Dictionory getInstance() {
        if (instance == null) {
            instance = new Dictionory();
        }
        return instance;
    }


    public void add(String key, String value) {
        dictionary.put(key, value);
    }

    public void  update (String key, String value) {
        add(key, value);
    }

    public void delete(String key) {
        dictionary.remove(key);
    }

    public String getDefination(String key) {
        return dictionary.get(key);
    }


}
