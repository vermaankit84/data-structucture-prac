package com.algo.expert.designpatterns.structural.Bridge;

public class BridgeImpl {

    public static void main(String[] args) {
        System.out.println("Bridge Pattern Demo");
        System.out.println("Dealing with television at present");

        State p_state = new OnState();
        ElectronicGoods t_item = new Television();
        t_item.setState(p_state);
        t_item.moveToCurrentState();
        t_item.hardButtonPressed();

        State o_state = new OffState();
        t_item.setState(o_state);
        t_item.moveToCurrentState();
        t_item.hardButtonPressed();

        System.out.println("Dealing with DVD at present");

        ElectronicGoods d_item = new DVD();
        d_item.setState(p_state);
        d_item.moveToCurrentState();
        d_item.hardButtonPressed();


        d_item.setState(o_state);
        d_item.moveToCurrentState();
        d_item.hardButtonPressed();
    }
}
