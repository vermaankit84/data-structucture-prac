package com.algo.expert.classes.graphs;
import com.google.common.collect.Lists;

import java.util.*;

public class TreeGraph {
    public static void main(String[] args) {
        List<Integer> edge_start = Lists.newArrayList(0, 0, 0);
        List<Integer> edge_send = Lists.newArrayList(1, 2, 3);

        System.out.println(is_it_a_tree(4, edge_start, edge_send));
    }

    public static boolean is_it_a_tree(int node_count, List<Integer> edge_start, List<Integer> edge_end) {
        Graph graph = new Graph(node_count);
        for (int i = 0; i < edge_start.size(); i++) {
            int start = edge_start.get(i);
            int end = edge_end.get(i);
            graph.addEdge(start, end);
        }

        return is_it_a_tree(graph, node_count);
    }

    public static boolean is_it_a_tree(Graph graph, int n) {
        boolean [] visited = new boolean[n];
        Arrays.fill(visited, false);

        int [] parent = new int[n];

        int count = 0;

        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                if (count > 1) {
                    return false;
                }
                count = count + 1;
                if (bfs(i, graph.adj, visited, parent)) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean bfs(int node, LinkedList<Integer> neibhour[], boolean[] visited, int[] parent) {
        visited[node] = true;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(node);

        while (!queue.isEmpty()) {
            int parent_node = queue.poll();
            for (int current_neibhour = 0; current_neibhour < neibhour[parent_node].size(); current_neibhour++) {
                if (!visited[current_neibhour]) {
                    visited[current_neibhour] = true;
                    parent[current_neibhour] = parent_node;
                    queue.add(current_neibhour);
                } else {
                    // line to check the cycle
                    if (parent[parent_node]!= current_neibhour) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    static class Graph {
        public int size;
        public LinkedList<Integer> adj[];

        public Graph(int size) {
            this.size = size;
            adj = new LinkedList[size];
            for (int i = 0 ; i < size; i = i + 1) {
                adj[i] = new LinkedList<>();
            }
        }

        public void addEdge(int start, int end) {
            adj[start].add(end);
            adj[end].add(start);
        }

    }
}
