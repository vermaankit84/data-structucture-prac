package com.algo.expert.dp;

public class TrappingRainWater {
    public static void main(String[] args) {
        int [] heights =  {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(trap(heights));
    }

    public static int trap(int[] height) {
         int sum = 0;
         for (int i = 1; i < height.length - 1; i++) {
            int current = height[i];
            sum = sum + getRainTrap(height, current, i);
        }
        return sum;
    }

    private static int getRainTrap(int[] height, int current, int index) {
        return Math.min(getMaxLeft(height, current, index), getMaxRight(height, current, index)) - current;
    }

    private static int getMaxLeft(int[] height, int current, int index) {
        int max = current;
        for(int i = index; i > -1 ; i = i - 1) {
            max = Math.max(height[i], max);
        }
        return max;
    }
    

    private static int getMaxRight(int[] height, int current, int index) {
        int max = current;

        for (int i = index; i < height.length; i++) {
            max = Math.max(max, height[i]);
        }
        return max;
    }
}
