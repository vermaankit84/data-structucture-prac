package com.algo.expert.easy.goldman;

import java.util.Arrays;

public class CoinChange {
    public static void main(String[] args) {
        int coins[] = {1, 5, 10, 25};
        int m = coins.length;
        System.out.println("Minimum coins required is "+ minCoins(coins, m, 11) ); // Should be 2
        System.out.println("Minimum coins required is "+ minCoins(coins, m, 30) ); // Should be 2
        System.out.println("Minimum coins required is "+ minCoins(coins, m, 87) ); // Should be 6
        System.out.println("Minimum coins required is "+ minCoins(coins, m, 98) ); // Should be 8
    }

    private static int minCoins(int coins[], int m, int V) {
        int numOfCoins[] = new int[V + 1];
        Arrays.fill(numOfCoins, Integer.MAX_VALUE);
        numOfCoins[0] = 0;
        int compare = 0;
        for(int c : coins) {
            for (int i = 0; i < numOfCoins.length; i++) {
                if (c <= i) {
                    if (numOfCoins[i - c] == Integer.MAX_VALUE) {
                        compare = numOfCoins[i - c];
                    } else {
                        compare = numOfCoins[i - c] + 1;
                    }
                    numOfCoins[i] = Math.min(compare, numOfCoins[i]);
                }
            }
        }
        return numOfCoins[V]!= Integer.MAX_VALUE ? numOfCoins[V] : -1;
    }
}
