package com.algo.expert.designpatterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDatabase implements Database {
    private final List<Employee> employeeList = new ArrayList<>();

    public EmployeeDatabase() {
        employeeList.add(new Employee("Ankit", "1"));
        employeeList.add(new Employee("Sammer", "2"));
    }

    @Override
    public EmployeeIterator employeeIterator() {
        return new EmployeeIterator(employeeList);
    }
}
