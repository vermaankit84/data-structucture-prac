package com.algo.expert.arrays;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSumToZero {
    public static void main(String[] args) {
        int nums[] = {-1,0,1,2,-1,-4};
        System.out.println(threeSum(nums));
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        if (nums.length == 0) return new ArrayList<>();

        if (nums.length == 1 && nums[0] == 0) return new ArrayList<>();

        List<List<Integer>> pairs = new ArrayList<>();
        Arrays.sort(nums);
        System.out.println(Arrays.toString(nums));

        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i - 1] == nums[i]) {
                continue;
            }
            int start = i + 1;
            int end = nums.length - 1;
            while (start < end) {


                int e1 = nums[start];
                int e2 = nums[end];

                int sum = e1 + e2 + nums[i];

                if (sum == 0) {
                    List<Integer> p = new ArrayList<>();
                    p.add(nums[i]);
                    p.add(e1);
                    p.add(e2);
                    start += 1;
                    end -= 1;
                    pairs.add(p);
                    while (start < end && (nums[start] == nums[start - 1])) {
                        start += 1;
                    }

                    while (start < end && (nums[end] == nums[end + 1])) {
                        end -= 1;
                    }


                } else if (sum < 0){
                    start = start + 1;
                } else {
                    end = end - 1;
                }
            }

        }

        return pairs;
    }
}
