package com.algo.expert.classes.trees.traversal;

import com.algo.expert.classes.trees.TreeNode;

import java.util.Objects;

public class BinaryLevel {
    TreeNode node;
    int level;
    int data;

    public TreeNode getNode() {
        return node;
    }

    public void setNode(TreeNode node) {
        this.node = node;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public BinaryLevel(TreeNode node, int level, int data) {
        this.node = node;
        this.level = level;
        this.data = data;
    }

    @Override
    public String toString() {
        return "BinaryLevel{" +
                "level=" + level +
                ", data=" + data +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinaryLevel)) return false;
        BinaryLevel that = (BinaryLevel) o;
        return level == that.level && Objects.equals(node, that.node);
    }

    @Override
    public int hashCode() {
        return Objects.hash(node, level);
    }
}
