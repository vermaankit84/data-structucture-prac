package com.algo.expert.classes.dynamic_programming;

public class CountUniquePath {
    public static void main(String[] args) {
        System.out.println(count_unique_path(1, 1));
        System.out.println(count_unique_path(2, 2));
        System.out.println(count_unique_path(3, 2));
        System.out.println(count_unique_path(4, 4));
    }

    // O(m*n)
    public static int count_unique_path (int m, int n) {

       int [][] dp = new int[m + 1][n + 1];
       for(int i = 0; i < m + 1; i = i + 1) {
           for (int j = 0; j < n + 1; j = j + 1) {
               if (i == 0 || j == 0) {
                   dp[i][j] = 1;
               } else {
                   dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
               }
           }
        }

       return dp[m][n];
    }
}
