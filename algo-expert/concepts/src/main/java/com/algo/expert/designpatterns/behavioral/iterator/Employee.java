package com.algo.expert.designpatterns.behavioral.iterator;

public class Employee {
    private final String name;
    private final String id;

    public Employee(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
