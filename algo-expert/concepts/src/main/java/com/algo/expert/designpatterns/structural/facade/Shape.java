package com.algo.expert.designpatterns.structural.facade;

public interface Shape {
    void draw();
}
