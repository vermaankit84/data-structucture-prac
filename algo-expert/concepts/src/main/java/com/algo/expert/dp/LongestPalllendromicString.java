package com.algo.expert.dp;

public class LongestPalllendromicString {
    public static void main(String[] args) {
        System.out.println(longestPalindrome("cyyoacmjwjubfkzrrbvquqkwhsxvmytmjvbborrtoiyotobzjmohpadfrvmxuagbdczsjuekjrmcwyaovpiogspbslcppxojgbfxhtsxmecgqjfuvahzpgprscjwwutwoiksegfreortttdotgxbfkisyakejihfjnrdngkwjxeituomuhmeiesctywhryqtjimwjadhhymydlsmcpycfdzrjhstxddvoqprrjufvihjcsoseltpyuaywgiocfodtylluuikkqkbrdxgjhrqiselmwnpdzdmpsvbfimnoulayqgdiavdgeiilayrafxlgxxtoqskmtixhbyjikfmsmxwribfzeffccczwdwukubopsoxliagenzwkbiveiajfirzvngverrbcwqmryvckvhpiioccmaqoxgmbwenyeyhzhliusupmrgmrcvwmdnniipvztmtklihobbekkgeopgwipihadswbqhzyxqsdgekazdtnamwzbitwfwezhhqznipalmomanbyezapgpxtjhudlcsfqondoiojkqadacnhcgwkhaxmttfebqelkjfigglxjfqegxpcawhpihrxydprdgavxjygfhgpcylpvsfcizkfbqzdnmxdgsjcekvrhesykldgptbeasktkasyuevtxrcrxmiylrlclocldmiwhuizhuaiophykxskufgjbmcmzpogpmyerzovzhqusxzrjcwgsdpcienkizutedcwrmowwolekockvyukyvmeidhjvbkoortjbemevrsquwnjoaikhbkycvvcscyamffbjyvkqkyeavtlkxyrrnsmqohyyqxzgtjdavgwpsgpjhqzttukynonbnnkuqfxgaatpilrrxhcqhfyyextrvqzktcrtrsbimuokxqtsbfkrgoiznhiysfhzspkpvrhtewthpbafmzgchqpgfsuiddjkhnwchpleibavgmuivfiorpteflholmnxdwewj"));
    }

    public static String longestPalindrome(String s) {
        String[][] dp = new String[s.length() + 1][s.length() + 1];
        for (int i = 0; i < s.length() + 1; i++) {
            for (int j = i; j < s.length() + 1; j++) {
                String temp = s.substring(i, j);
                dp[i][j] = temp;
            }
        }

        int length = 0;
        String max  ="";
        for (String[] strings : dp) {
            for (int j = 0; j < dp.length; j++) {
                String str = strings[j];
                if (str != null) {
                    if (is_pallendronic(str)) {
                        if (length < str.length()) {
                            length = str.length();
                            max = str;
                        }
                    }
                }
            }
        }




        return max;
    }

    private static boolean is_pallendronic(String str) {
       return new StringBuilder(str).reverse().toString().equals(str);
    }
}
