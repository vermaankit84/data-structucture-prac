package com.algo.expert.test;

import java.util.*;

public class MergeMettings {
    public static void main(String[] args) {
        int [][] input = {
  {2, 3},
  {4, 5},
  {6, 7},
  {8, 9},
  {1, 10}
};

        // 1 10 , 2 3

        System.out.println(Arrays.deepToString(mergeOverlappingIntervals(input)));
    }

    public static int[][] mergeOverlappingIntervals(int[][] intervals) {

        List<Meetings> meeting_list = new ArrayList<>();
        for (int[] interval : intervals) {
            meeting_list.add(new Meetings(interval[0], interval[1]));
        }

        meeting_list.sort(Comparator.comparingInt(i -> i.start));
        Stack<Meetings> stack = new Stack<>();
        stack.push(meeting_list.get(0));

        for (int i = 1; i < meeting_list.size(); i = i + 1) {
            Meetings current_meeting = meeting_list.get(i);
            Meetings top = stack.peek();
            if (top.end > current_meeting.start) {
                top = stack.pop();
                if (top.end < current_meeting.end) {
                    top.end = current_meeting.end;
                }
                stack.push(top);
            } else {
                stack.push(current_meeting);
            }
        }

        int [][] merge_intervals = new int[stack.size()][2];
        int counter = 0;
        while (!stack.isEmpty()) {
            Meetings meetings = stack.pop();
            merge_intervals[counter][0] = meetings.start;
            merge_intervals[counter][1] = meetings.end;
            counter = counter + 1;
        }
        return merge_intervals;
    }

    static class Meetings {
        int start;
        int end;

        public Meetings(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public String toString() {
            return this.start + " " + this.end;
        }
    }
}
