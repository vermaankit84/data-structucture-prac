package com.algo.expert.designpatterns.creational.prototype;

public class Square extends Shape{
    public Square() {
        this.setType("square");
    }
    @Override
    public void draw() {
        System.out.println("draw square");
    }
}
