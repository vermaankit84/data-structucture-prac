package com.algo.expert.dp;

import java.util.Scanner;

public class DecodeWays {
    public static void main(String[] args) {
        DecodeWays decodeWays = new DecodeWays();
        Scanner scanner = new Scanner(System.in);
        String input =  "**";
        int sum = 0;
        for (int i = 1; i < 10; i++) {
            sum = sum + decodeWays.numDecodings(input.replaceAll("\\*", String.valueOf(i)));
        }

        System.out.println(sum);
        scanner.close();
    }



    public int numDecodings(String s) {
        int [] dp = new int[s.length() + 1];
        dp[0] = 1;
        dp[1] = s.length() == 0 ? 0 : 1;

        for (int i = 2; i < s.length() + 1; i++) {
            int oneLength = Integer.valueOf(s.substring(i - 1, i));
            int twoLength = Integer.valueOf(s.substring(i - 2, i));

            if (oneLength >= 1) {
                dp[i] = dp[i] + dp[i - 1];
            }

            if (twoLength >= 10 && twoLength <= 26) {
                dp[i] = dp[i] + dp[i - 2];
            }
        }

        return dp[s.length()];
    }
}
