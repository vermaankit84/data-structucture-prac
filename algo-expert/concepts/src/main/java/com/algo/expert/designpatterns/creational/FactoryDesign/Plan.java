package com.algo.expert.designpatterns.creational.FactoryDesign;

public interface Plan {
    void createPlan();
}
