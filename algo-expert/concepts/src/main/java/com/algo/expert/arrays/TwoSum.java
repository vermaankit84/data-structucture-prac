package com.algo.expert.arrays;

import java.util.ArrayList;
import java.util.List;

public class TwoSum {
    public static void main(String[] args) {
        int arr[] = {1, 7, 8, 9, 9, 10, 11, 30};
        int target = 18;
        List<List<Integer>> pairs = new ArrayList<>();

        int start = 0;
        int end = arr.length - 1;
        while (start < end) {
            int e1 = arr[start];
            int e2 = arr[end];
            int sum = (e2 + e1);
            if (sum == target) {
                List<Integer> pair = new ArrayList<>();
                pair.add(e1);
                pair.add(e2);
                start += 1;
                pairs.add(pair);
            } else if (sum > target) {
                end = end - 1;
            } else {
                start = start + 1;
            }
        }

        System.out.println(pairs);
    }
}
