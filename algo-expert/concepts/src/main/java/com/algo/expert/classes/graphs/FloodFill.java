package com.algo.expert.classes.graphs;

import java.util.*;

public class FloodFill {
    public static void main(String[] args) {
        int [][] image = {
                {0, 1, 3},
                {1, 1, 1},
                {1, 5, 4} };

        int pixel_row = 0;
        int pixel_coloumn = 1;
        int new_color = 2;
        ArrayList<ArrayList<Integer>> imageList = new ArrayList<>();
        for (int[] ints : image) {
            ArrayList<Integer> temp = new ArrayList<>();
            for (int anInt : ints) {
                temp.add(anInt);
            }
            imageList.add(temp);
        }
        System.out.println(flood_fill(pixel_row, pixel_coloumn, new_color, imageList));
    }

    static ArrayList<ArrayList<Integer>> flood_fill(Integer pixel_row, Integer pixel_column, Integer new_color, ArrayList<ArrayList<Integer>> image) {

        int original_color = image.get(pixel_row).get(pixel_column);

        if (original_color == new_color) {
            return image;
        }

        dfs(pixel_row, pixel_column, original_color, new_color, image);
        return image;
    }

   private static void dfs(Integer pixel_row, Integer pixel_column, Integer original_color , Integer new_color, ArrayList<ArrayList<Integer>> image) {
        image.get(pixel_row).set(pixel_column, new_color);

        if (pixel_row + 1 < image.size() && image.get(pixel_row + 1).get(pixel_column).equals(original_color)) {
            dfs(pixel_row + 1 , pixel_column, original_color, new_color, image);
        }

       if (pixel_column + 1 < image.get(pixel_row).size() && image.get(pixel_row).get(pixel_column + 1).equals(original_color)) {
           dfs(pixel_row, pixel_column + 1, original_color, new_color, image);
       }

       if (pixel_row - 1 >= 0 && image.get(pixel_row - 1).get(pixel_column).equals(original_color)){
           dfs(pixel_row - 1, pixel_column, original_color, new_color, image);
       }

       if (pixel_column - 1 >= 0 && image.get(pixel_row).get(pixel_column - 1).equals(original_color)) {
           dfs(pixel_row, pixel_column - 1, original_color, new_color, image);
       }
    }
}
