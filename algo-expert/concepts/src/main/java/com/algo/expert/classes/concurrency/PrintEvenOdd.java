package com.algo.expert.classes.concurrency;

public class PrintEvenOdd {
    private static final Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {
        Thread t1  = new Thread(() -> {
            for (int i = 1; i < 50; i = i + 2) {
                synchronized (lock) {
                    System.out.println("odd [ " + i + " ] ");
                    {
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        Thread t2  = new Thread(() -> {
            for (int i = 2; i < 50; i = i + 2) {
                synchronized (lock) {
                    System.out.println("even [ " + i + " ] ");
                    {
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });


        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}
