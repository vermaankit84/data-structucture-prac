package com.algo.expert.designpatterns.behavioral.template;
// In tempatele method implemetaion we provide some implemenatuon in the base class and subclasses will take the
// responsibility of the implemnting th corossponding details
public class TemplateImpl {
    public static void main(String[] args) {

        System.out.println("********** Template method pattern implementation ***************\n");
        BasicEng eng = new ComputerScience();
        eng.completeCourse();

        System.out.println("\n************************\n");
        eng = new Electronics();
        eng.completeCourse();
    }
}
