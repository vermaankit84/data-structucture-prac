package com.algo.expert.dp;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangle {
    public static void main(String[] args) {
        System.out.println(generate(0));
        System.out.println(generate(1));
        System.out.println(generate(3));
        System.out.println(generate(4));
        System.out.println(generate(5));
    }

    public static List<List<Integer>> generate(int numRows) {
       List<List<Integer>> response = new ArrayList<>();

        for (int i = 0; i < numRows + 1; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j < i + 1; j++) {
                if (j == 0 || j == i) {
                    row.add(1);
                } else {
                    row.add(response.get( i - 1).get(j - 1) + response.get(i - 1).get(j));
                }
            }

            response.add(row);
        }

        return  response;
    }

    /**

     List<List<Integer>> result = new ArrayList<>();
     for (int i = 0; i < numRows; i ++) {
     List<Integer> row = new ArrayList<>();
     for (int j = 0; j < i + 1; j ++) {
     if (j == 0 || j == i) {
     row.add(1);
     } else {
     row.add(result.get(i - 1).get(j - 1) + result.get(i - 1).get(j));
     }
     }
     result.add(row);
     }
     return result;
     */
}
