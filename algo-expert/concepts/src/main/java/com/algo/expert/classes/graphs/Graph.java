package com.algo.expert.classes.graphs;

import java.util.Arrays;
import java.util.LinkedList;

public class Graph {
    public int vertex;
    public LinkedList<Integer> adj[];

    Graph(int v) {
        vertex = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new LinkedList<>();
        }
    }

    public void addEdge(int start, int end) {
        adj[start].add(end);
    }

    public void BFS(int node) {
        boolean[] visited = new boolean[vertex];
        LinkedList<Integer> queue = new LinkedList<>();
        Arrays.fill(visited, false);

        visited[node] = true;
        queue.add(node);

        while (!queue.isEmpty()) {
            int element = queue.poll();
            System.out.print(element + " ");
            for (int n : this.adj[node]) {
                if (!visited[n]) {
                    visited[n] = true;
                    queue.add(n);
                }
            }

        }
    }
}
