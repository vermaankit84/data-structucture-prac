package com.algo.expert.designpatterns.behavioral.startegy;

public class Boat extends Vehicle {

    public Boat(TransportMedium medium) {
        super(medium);
    }

    @Override
    public void showMe() {
        System.out.println("i am in boat");
    }
}
