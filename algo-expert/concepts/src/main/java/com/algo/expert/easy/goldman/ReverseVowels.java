package com.algo.expert.easy.goldman;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ReverseVowels {
    public static void main(String[] args) {
        String input = "hello";
        System.out.println(reverseVowels(input));

        input = "hello world";
        System.out.println(reverseVowels(input));
    }

    private static String reverseVowels(String input) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == 'a' || c == 'i' || c == 'o' || c == 'e' || c == 'u') {
               builder.append(c);
            }
        }
        String reverseVowels = builder.reverse().toString();
        int reverseCounter = 0;
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == 'a' || c == 'i' || c == 'o' || c == 'e' || c == 'u') {
                output.append(reverseVowels.charAt(reverseCounter));
                reverseCounter = reverseCounter + 1;
            } else {
                output.append(c);
            }
        }

        return output.toString();
    }
}
