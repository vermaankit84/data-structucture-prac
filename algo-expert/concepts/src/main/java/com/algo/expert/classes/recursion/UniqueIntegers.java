package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class UniqueIntegers {
    public static void main(String[] args) {
        List<Integer> list = Lists.newArrayList(1, 2,3);
        List<List<Integer>> output = get_permutations(list);
        System.out.println(output);
    }

    private static List<List<Integer>> get_permutations(List<Integer> arr) {
        List<List<Integer>> response = new ArrayList<>();

        helper(arr, 0,  new Stack<>(), response);

        return response;
    }

    private static void helper(List<Integer> arr, int idx, Stack<Integer> slate, List<List<Integer>> response) {
        if (idx == arr.size()) {
            response.add(new ArrayList<>(slate));
            return;
        }

        for (int i = idx; i < arr.size(); i++) {
            swap(arr, idx, i);
            slate.push(arr.get(idx));
            helper(arr, idx + 1,slate, response );
            slate.pop();
            swap(arr, idx, i);
        }
    }

    static void swap(List<Integer> arr, int i , int j) {
        Collections.swap(arr, i , j);
    }
}
