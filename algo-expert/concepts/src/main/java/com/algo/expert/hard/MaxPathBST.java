package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxPathBST {
    public static void main(String[] args) {

    }

    public static int maxPathSum(BinaryTree tree) {
        return findMaxPathSum(tree).get(0);
    }

    private static List<Integer> findMaxPathSum(BinaryTree tree) {
        if (tree == null) {
            return new ArrayList<>(Arrays.asList(0, Integer.MIN_VALUE));
        }

        List<Integer> left_max_sum = findMaxPathSum(tree.left);
        int left_max_sum_branch = left_max_sum.get(0);
        int left_max_path_sum = left_max_sum.get(1);


        List<Integer> right_max_sum = findMaxPathSum(tree.right);
        int right_max_sum_branch = right_max_sum.get(0);
        int right_max_path_branch = right_max_sum.get(1);

        int max_child_sum_as_branch = Math.max(left_max_sum_branch, right_max_sum_branch);
        int max_sum_as_branch = Math.max(max_child_sum_as_branch + tree.value, tree.value);
        int max_sum_as_root_node = Math.max(max_child_sum_as_branch + tree.value + max_sum_as_branch, tree.value);
        int max_path_sum = Math.max(left_max_path_sum, Math.max(right_max_path_branch, max_sum_as_root_node));

        return new ArrayList<>(Arrays.asList(max_sum_as_branch, max_path_sum));

    }

    static class BinaryTree {
        public int value;
        public BinaryTree left;
        public BinaryTree right;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
