package com.algo.expert.codility;

import java.util.ArrayList;
import java.util.List;

public class BreakString {
    public static void main(String[] args) {
        String str = "bbbaaabbb";
        List<String> blocks = new ArrayList<>();

        StringBuilder temp = new StringBuilder();
        int length = 1;
        for (int i = 1; i < str.length(); i++) {
            char current = str.charAt(i);
            char previous = str.charAt(i - 1);
            if ((current!=previous)) {
                temp.append(previous);
                temp.append(length);
                temp.append(",");
                length = 0;
            }
            length += 1;
        }

        temp.append(str.charAt(str.length() - 1));
        temp.append(length);

        String runLength = temp.toString();


        String[] d =  runLength.split(",");
        int max = 0;
        for (String s : d) {
            String c = s.substring(0, 1);
            int count = Integer.parseInt(s.substring(1));
            max = Math.max(max, count);
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < count; j++) {
                builder.append(c);
            }
            blocks.add(builder.toString());
        }



        int sum = 0;
        for (String block : blocks) {
            int size = block.length();
            sum = sum + (max - size);
        }
        System.out.println(sum);
    }


}
