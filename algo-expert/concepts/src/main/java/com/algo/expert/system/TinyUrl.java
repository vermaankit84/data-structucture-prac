package com.algo.expert.system;

import java.util.HashMap;
import java.util.Map;

public class TinyUrl {
    public static void main(String[] args) {
        TinyUrl tinyUrl = new TinyUrl();
        String tiny = tinyUrl.encode("https://leetcode.com/problems/design-tinyurl");
        System.out.println(tinyUrl.decode(tiny));
    }

    private int counter = 0;
    private final Map<Integer, String> map = new HashMap<>();

    public String encode(String longUrl) {
        this.counter = counter + 1;
        String s = "http://tinyurl.com/";
        map.put(counter, longUrl);
        return s + counter;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        String [] url = shortUrl.split("/");
        return map.get(Integer.parseInt(url[url.length - 1]));
    }
}
