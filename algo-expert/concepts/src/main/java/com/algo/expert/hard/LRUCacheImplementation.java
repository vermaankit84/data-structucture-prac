package com.algo.expert.hard;

import sun.misc.Queue;

import java.util.*;

public class LRUCacheImplementation {
    public static void main(String[] args) {
        LRUCache cache = new LRUCache(1);
        cache.getValueFromKey("a");
        cache.insertKeyValuePair("a", 1);
        System.out.println(cache.getValueFromKey("a").value);
        cache.insertKeyValuePair("a", 9001);
        cache.insertKeyValuePair("b", 2);
        System.out.println(cache.getValueFromKey("a"));
        System.out.println(cache.getValueFromKey("b").value);
        cache.insertKeyValuePair("c", 3);
        System.out.println(cache.getValueFromKey("b"));
        System.out.println(cache.getValueFromKey("c").value);
    }

    static class LRUCache {
        int maxSize;
        Deque<String> most_resent = new ArrayDeque<>();
        Map<String, Integer> map = new HashMap<>();

        public LRUCache(int maxSize) {
            this.maxSize = Math.max(maxSize, 1);
        }

        public void insertKeyValuePair(String key, int value) {
            if (map.containsKey(key)) {
                map.put(key, value);
                most_resent.remove(key);
                most_resent.add(key);
                return;
            }
            if (map.size() >= maxSize) {
                evictData();
            }
            map.put(key, value);
            most_resent.add(key);
        }

        private void evictData() {
            String key =  most_resent.removeFirst();
            map.remove(key);
        }

        public LRUResult getValueFromKey(String key) {
            if (map.get(key)!=null) {
                LRUResult result = new LRUResult(true, map.get(key));
                most_resent.addFirst(key);
                return result;
            }
            return null;
        }

        public String getMostRecentKey() {
            return most_resent.getFirst();
        }
    }

    static class LRUResult {
        boolean found;
        int value;

        public LRUResult(boolean found, int value) {
            this.found = found;
            this.value = value;
        }
    }
}
