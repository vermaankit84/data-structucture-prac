package com.algo.expert.test;

import java.util.Arrays;
import java.util.Stack;

public class LargestSubAreaMatrix {
    public static void main(String[] args) {
        int [][] matrix = {
                {1, 1, 0, 1, 1},
                {1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1},
                {1, 1, 1, 1, 1},
                {1, 0, 1, 1, 1},
                {1, 1, 1, 1, 1}
        };

        int getMaxArea = get_max_area(matrix);
        System.out.println(getMaxArea);


        int [] input = {2, 2, 1, 2, 2};
        int [] next_smaller = next_smaller(input);
        int [] previous_smaller = previous_smaller(input);
        System.out.println(Arrays.toString(next_smaller));
        System.out.println(Arrays.toString(previous_smaller));
        int max = -1;
        for (int i = 0; i < input.length; i++) {
            int current = input[i];
            max = Math.max(max, (next_smaller[i] - previous_smaller[i]) * current);
        }

        System.out.println(max);
    }

    private static int get_max_area(int[][] matrix) {

        int max_area = -1;
        int [] input = new int[matrix[0].length];
        Arrays.fill(input, 0);
        int i = 0;
        while (i < matrix.length) {
            for (int j = 0; j < matrix[0].length; j++) {
                int current = matrix[i][j];
                if (current == 0) {
                    input[j] = 0;
                } else  {
                    input[j] = input[j] + current;
                }
            }

            int max = maximum_area_histogram(input);
            System.out.println(max);
            max_area = Math.max(max, max_area);
            i = i + 1;
        }




        return max_area;
    }


    public static int[] next_smaller (int [] arr) {
        Stack<Integer> stack = new Stack<>();
        int next_smaller[] = new int[arr.length];
        stack.push(arr.length - 1);
        next_smaller[arr.length - 1] = -1;

        for (int i = arr.length - 2; i > -1 ; i = i - 1) {
            int top = arr[stack.peek()];
            int current = arr[i];
            if (current < top) {
                while ((!stack.isEmpty())) {
                    int current_top = arr[stack.peek()];
                    if (current_top > current) {
                        stack.pop();
                    }
                }
            }

            if (!stack.isEmpty()) {
                next_smaller[i] = stack.peek();
            } else  {
                next_smaller[i] = -1;
            }
            stack.push(i);

        }

        return next_smaller;
    }

    public static int[] previous_smaller (int [] arr) {
        Stack<Integer> stack = new Stack<>();
        int previous_smaller[] = new int[arr.length];
        stack.push(0);
        previous_smaller[0] = -1;

        for (int i = 1; i < arr.length ; i = i + 1) {
            int top = arr[stack.peek()];
            int current = arr[i];
            if (current < top) {
                while ((!stack.isEmpty())) {
                    int current_top = arr[stack.peek()];
                    if (current_top > current) {
                        stack.pop();
                    }
                }
            }

            if (!stack.isEmpty()) {
                previous_smaller[i] = stack.peek();
            } else  {
                previous_smaller[i] = -1;
            }
            stack.push(i);

        }

        return previous_smaller;
    }

    // 1 1 0 1 1
    private static int maximum_area_histogram(int[] input) {
        int max_area = -1;
        for (int i = 0; i < input.length; i++) {
            max_area = Math.max(max_area, input[i]) ;
            int min = input[i];
            for (int j = i + 1; j < input.length; j++) {
                if (input[j] == 0) {
                    break;
                }

                min = Math.min(min, input[j]);
                max_area = Math.max((j - i +  1) * min , max_area);
            }
        }
        return  max_area;

    }
}
