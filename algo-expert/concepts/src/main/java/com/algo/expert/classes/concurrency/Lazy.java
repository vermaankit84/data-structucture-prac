package com.algo.expert.classes.concurrency;


public class Lazy {
    private static boolean init = false;
    private static Thread t = new Thread(() -> init = true);
    static {
        t.start();
    }

    public static void main(String[] args) {
        try {
            t.join();
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
        System.out.println(init);
    }
}
