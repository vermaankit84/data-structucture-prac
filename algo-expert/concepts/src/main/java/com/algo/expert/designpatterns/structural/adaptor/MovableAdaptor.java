package com.algo.expert.designpatterns.structural.adaptor;

public interface MovableAdaptor {
    double getSpeed();
}
