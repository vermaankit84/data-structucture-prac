package com.algo.expert.hard;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithOutDuplication {
    public static void main(String[] args) {
        System.out.println(longestSubstringWithoutDuplication("abcdabcef"));
    }

    public static String longestSubstringWithoutDuplication(String str) {
        if (str.length() == 1) return str;

        boolean isStringContainsUnique = isStringContainsUnique(str);
        if (isStringContainsUnique) return str;
        int max = Integer.MIN_VALUE;
        String response = "";
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j < str.length(); j++) {
                String test = str.substring(i, j + 1);
                boolean isRepeatedChar  = isRepetedCharacter(test);
                if (!isRepeatedChar) {
                    int length =test.length();
                    if (max < length) {
                        max = length;
                        response = test;
                    }
                }
            }
        }

        return response;
    }

    private static boolean isStringContainsUnique(String str) {
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < str.length(); i++) {
            set.add(str.charAt(i));
        }
        return set.size()  == str.length();
    }

    public static boolean isRepetedCharacter(String test) {
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < test.length(); i++) {
            if (!set.add(test.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
