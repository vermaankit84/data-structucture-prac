package com.algo.expert.designpatterns.structural.adaptor;

public interface Movable {
    double getSpeed();
}
