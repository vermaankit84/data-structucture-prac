package com.algo.expert.designpatterns.creational.abstractfactory;

public class White implements Color{
    @Override
    public String getColor() {
        return "white";
    }
}
