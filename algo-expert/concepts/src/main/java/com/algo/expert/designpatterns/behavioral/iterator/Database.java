package com.algo.expert.designpatterns.behavioral.iterator;

public interface Database {
    EmployeeIterator employeeIterator();
}
