package com.algo.expert.easy.facebook;

import java.util.*;

public class TwoNumberSum {

    int numberOfWays(int[] arr, int k) {
        Map<Integer, Integer> pairs = new HashMap<>();
        for (int j : arr) {
            pairs.put(j, pairs.getOrDefault(j, 0) + 1);
        }

        int count = 0;
        for (int j : arr) {
            if (pairs.get(k - j) != null) {
                count += pairs.get(k - j);
            }

            if (k - j == j) {
                count -= 1;
            }

        }
        return count / 2;
    }
}
