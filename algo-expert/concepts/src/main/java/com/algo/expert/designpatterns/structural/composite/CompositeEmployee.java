package com.algo.expert.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class CompositeEmployee implements Employee {

    private int empCount = 0;
    private final String name;
    private final String dept;

    private final List<Employee> controls;

    public CompositeEmployee(String name, String dept) {
        this.name = name;
        this.dept = dept;
        this.controls = new ArrayList<>();
    }

    public void addEmployee(Employee e) {
        this.controls.add(e);
    }

    public void remove(Employee e) {
        this.controls.remove(e);
    }

    @Override
    public void printStructures() {
        System.out.println(this.name + " works in " + this.dept);
        this.controls.forEach(Employee::printStructures);
    }

    @Override
    public int getEmployeeCount() {
        for (Employee e: controls) {
            empCount = empCount + e.getEmployeeCount();
        }
        return empCount;
    }
}
