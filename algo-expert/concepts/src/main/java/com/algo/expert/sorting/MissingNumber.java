package com.algo.expert.sorting;

import java.util.Arrays;

public class MissingNumber {
    public static void main(String[] args) {
        int []nums  = {3, 0, 1};
        MissingNumber missingNumber = new MissingNumber();
        missingNumber.missingNumber(nums);
        missingNumber.missingNumber(new int[]{0, 1});
    }

    public int missingNumber(int[] nums) {
        
        int array_sum = Arrays.stream(nums).sum();
        int sum = 0;
        for(int i = 0 ; i <= nums.length; i = i + 1) {
            sum = sum + i;
        }



        return (sum - array_sum);
    }
}
