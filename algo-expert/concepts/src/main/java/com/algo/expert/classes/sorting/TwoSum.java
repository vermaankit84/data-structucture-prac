package com.algo.expert.classes.sorting;

import java.util.*;

public class TwoSum {
    public static void main(String[] args) {
        int arr[] = {5,9, 1,3};
        System.out.println(isPairExists_withSet(arr, 6));
        System.out.println(isPairExists_withSet(arr, 11));
        System.out.println(isPairExists_withSorting(arr, 6));
    }

    private static boolean isPairExists_withSorting(int[] arr, int target) {
        Arrays.sort(arr);
        int start = 0;
        int end = arr.length - 1;

        while (start < end) {
            int sum = arr[start] + arr[end];
            if (sum > target) {
                end -= 1;
            } else if (sum < target){
                start += 1;
            }  else {
                return true;
            }
        }

        return false;
    }

    public static List<Integer> two_sum(List<Integer> numbers, int target) {

        List<Integer> response = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();


        for (int i = 0; i < numbers.size(); i++) {
            int rem = target - numbers.get(i);
            if (map.containsKey(rem)) {
                response.add(i);
                response.add(map.get(rem));
                return response;
            }
            map.put(numbers.get(i), i);
        }

        response.add(-1);
        response.add(-1);
        return response;

    }

    private static boolean isPairExists_withSet(int[] arr, int target) {
        Set<Integer> set = new HashSet<>();


        for (int num: arr) {
            int rem = target - num;
            if (set.contains(rem)) {
                return true;
            }
            set.add(num);
        }

        return false;
    }
}
