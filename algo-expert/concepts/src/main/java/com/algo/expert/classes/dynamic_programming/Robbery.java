package com.algo.expert.classes.dynamic_programming;

import java.util.Arrays;

public class Robbery {
    public static void main(String[] args) {
        int [] values = {6, 1, 2, 7};
        System.out.println(maxStolenValue(values));

        values = new int[]{1, 2, 4, 5, 1};
        System.out.println(maxStolenValue(values));
    }

    private static int maxStolenValue(int[] values) {
        int size = values.length;
        int dp[] = new int[size + 1];
        dp[0] = 0;
        dp[1] = Math.max(dp[0], values[0]);

        for(int i = 2; i < size + 1; i = i + 1) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + values[i - 1]);
        }

        System.out.println(Arrays.toString(dp));
        return dp[size];

    }
}
