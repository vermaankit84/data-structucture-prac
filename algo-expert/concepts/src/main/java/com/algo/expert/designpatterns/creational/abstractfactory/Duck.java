package com.algo.expert.designpatterns.creational.abstractfactory;

public class Duck implements Animal{
    @Override
    public String getType() {
        return "duck";
    }

    @Override
    public String makeSound() {
        return "squek";
    }
}
