package com.algo.expert.hardest;

public class MaxProfit {
    public static void main(String[] args) {

    }

    public static int maxProfitWithKTransactions(int[] prices, int k) {
        if (prices.length == 0) return 0;

        int[][] profits = new int[k + 1][prices.length];

        for (int i = 1; i < k + 1; i++) {
            int max = Integer.MIN_VALUE;
            for (int j = 1; j < prices.length; j++) {
                max = Math.max(max, profits[i-1][j-1] - prices[j -1]);
                profits[i][j] = Math.max(profits[i][j - 1] , max + prices[j]);
            }
        }

        return profits[k][prices.length -1];
    }
}
