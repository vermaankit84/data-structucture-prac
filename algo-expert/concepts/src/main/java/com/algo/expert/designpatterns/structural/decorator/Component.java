package com.algo.expert.designpatterns.structural.decorator;

public abstract class Component {
    public abstract void makeHouse();
}
