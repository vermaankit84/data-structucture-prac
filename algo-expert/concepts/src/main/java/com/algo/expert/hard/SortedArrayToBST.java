package com.algo.expert.hard;

public class SortedArrayToBST {






    static class BinaryTree {
         Node root;


        public Node sortedArrayToBst(Integer arr[], int start, int end) {
            if (start > end) return null;

            int mid  = (start + end)/2;
            Node node = new Node(arr[mid]);

            node.left = sortedArrayToBst(arr, start, mid - 1);
            node.right = sortedArrayToBst(arr, mid + 1, end);

            return node;
        }
    }
}

class Node {
    int data;
    Node left;
    Node right;

    Node(int data) {
        this.data = data;
        this.left = this.right = null;
    }
}