package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FourNumberSum {
    public static void main(String[] args) {

    }

    public static List<Integer[]> fourNumberSum(int[] array, int targetSum) {
        Map<Integer, List<Integer[]>> all_pair_map = new HashMap<>();
        List<Integer[]> quad = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int current_sum = array[i] + array[j];
                int diff = targetSum - current_sum;
                if (all_pair_map.containsKey(diff)) {
                    for (Integer[] p : all_pair_map.get(diff)) {
                        quad.add(new Integer[] {p[0], p[1], array[i], array[j]});
                    }
                }
            }

            for (int k = 0; k < i; k++) {
                int current_sum = array[i] + array[k];
                Integer[] pair = {array[k], array[i]};
                if (!all_pair_map.containsKey(current_sum)) {
                    List<Integer[]> group = new ArrayList<>();
                    group.add(pair);
                    all_pair_map.put(current_sum, group);
                } else {
                    all_pair_map.get(current_sum).add(pair);
                }

            }
        }
        return quad;
    }
}
