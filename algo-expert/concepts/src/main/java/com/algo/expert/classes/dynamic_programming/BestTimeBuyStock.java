package com.algo.expert.classes.dynamic_programming;

import java.util.Arrays;

public class BestTimeBuyStock {
    public static void main(String[] args) {
        int[] prices = {7, 1, 5, 3, 6, 4};
        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) {
        int [][] dp = new int[prices.length][prices.length];
        int max = 0;
        for(int buy = 0; buy < prices.length ; buy = buy + 1) {
            for (int sell = 0; sell < prices.length ; sell++) {
                if (buy == sell) {
                    dp[buy][sell] = -1;
                } else {
                    if (buy < sell) {
                        dp[buy][sell] = Math.max(max, prices[sell] - prices[buy]);
                        max = dp[buy][sell];
                    } else {
                        dp[buy][sell] = -1;
                    }
                }
            }
        }
        
        return max;
    }
}
