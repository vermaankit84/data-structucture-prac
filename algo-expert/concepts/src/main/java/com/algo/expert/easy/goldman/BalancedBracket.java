package com.algo.expert.easy.goldman;

import java.util.*;

public class BalancedBracket {

    private final static List<Character> start = new ArrayList<>();
    private final static List<Character> end = new ArrayList<>();
    private final static Map<Character, Character> map = new HashMap<>();

    static {
        start.add('{');
        start.add('(');
        start.add('[');

        end.add('}');
        end.add(')');
        end.add(']');

        map.put('}', '{');
        map.put(')', '(');
        map.put(']', '[');
    }
    public static void main(String[] args) {
        System.out.println( isBalanced( "{[()]}" ) ); // Should be YES
        System.out.println( isBalanced( "{{[[(())]]}}" ) ); // Should be YES
        System.out.println( isBalanced( "[]{}" ) ); // Should be YES
        System.out.println( isBalanced( "{[(])}" ) ); // Should be NO
        System.out.println( isBalanced( "[({}" ) ); // Should be NO
        System.out.println( isBalanced( "[]}" ) ); // Should be NO
        System.out.println( isBalanced( "[}]" ) );
    }

    private static String isBalanced(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char input = s.charAt(i);
            if (start.contains(input)) {
                stack.push(input);
            } else {
                if (end.contains(input)) {
                    if (stack.size() == 0) return "NO";
                    char peek = stack.peek();
                    char valid = map.get(input);
                    if (peek == valid) {
                        stack.pop();
                    } else {
                        return "NO";
                    }
                }
            }
        }


        return stack.isEmpty() ? "YES" : "NO";
    }
}
