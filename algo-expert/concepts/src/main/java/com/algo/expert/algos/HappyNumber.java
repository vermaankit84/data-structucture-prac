package com.algo.expert.algos;

public class HappyNumber {
    public static void main(String[] args) {
        System.out.println(isHappy(19));
        System.out.println(isHappy(2));
    }
    public static boolean isHappy(int n) {
        happy happy = new happy();
        helper(happy, n);

        return happy.isHappy;
    }

    private static void helper(happy happy, int n) {
        if (n == 1 || n == 7) {
            happy.isHappy = true;
            return;
        }

        if (n == 2 || n == 4 || n ==5 || n == 6 || n == 8 || n == 9) {
            happy.isHappy = false;
            return;
        }
        int sum = 0;
        while (n!=0) {
            int number = n%10;
            int square = number * number;
            sum = sum + square;
            n = n/10;
        }
        helper(happy, sum);
    }

    static class happy {
        boolean isHappy;
    }
}
