package com.algo.expert.designpatterns.structural.composite;

public class SimpleEmployee implements Employee {

    private final String name;
    private final String dept;

    public SimpleEmployee(String name, String dept) {
        this.name = name;
        this.dept = dept;
    }


    @Override
    public void printStructures() {
        System.out.println(this.name + " works in " + this.dept);
    }

    @Override
    public int getEmployeeCount() {
        return 1;
    }
}
