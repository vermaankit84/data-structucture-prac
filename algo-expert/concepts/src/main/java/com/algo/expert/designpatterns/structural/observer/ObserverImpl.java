package com.algo.expert.designpatterns.structural.observer;

public class ObserverImpl {
    public static void main(String[] args) {
        System.out.println("******** Observer Pattern Demo *********");

        Observer my_observer_1 = new ObserverType1("Roy");
        Observer my_observer_2 = new ObserverType1("Kevin");
        Observer my_observer_3 = new ObserverType1("Bose");

        Subject s = new Subject();
        s.register(my_observer_1);
        s.register(my_observer_2);
        s.register(my_observer_3);

        System.out.println("setting flag == 5");
        s.setFlag(5);

        s.unregister(my_observer_1);

        System.out.println("setting flag == 50");
        s.setFlag(50);
        s.register(my_observer_1);
        System.out.println("setting flag == 100");
        s.setFlag(100);
    }
}
