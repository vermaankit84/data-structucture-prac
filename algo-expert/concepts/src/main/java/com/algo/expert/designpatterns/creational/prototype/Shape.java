package com.algo.expert.designpatterns.creational.prototype;
// Prototype design pattern can be used when creation of the object is the
// heavy process now instead of changing the orriginal object create a
// clone of that object and now you can change the state of that object without hampering the
// other unaccessable
public abstract class Shape implements Cloneable {

    private String id;
    private String type;

    public abstract void draw();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return clone;
    }
}
