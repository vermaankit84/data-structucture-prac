package com.algo.expert.medium;

public class ShiftLinkedList {
    public static void main(String[] args) {

    }

    public static LinkedList shiftLinkedList(LinkedList head, int k) {
        int length = 1;
        LinkedList temp = head;
        while (temp.next!=null) {
            temp = temp.next;
            length = length + 1;
        }

        int offset = Math.abs(k)%length;
        if (offset == 0) return head;

        int new_offset = k > 0 ? length - offset : offset;
        LinkedList newTail = head;
        for (int i = 1; i < new_offset; i++) {
            newTail = newTail.next;
        }
        LinkedList newHead = newTail.next;
        newTail.next = null;
        temp.next = head;

        return newHead;
    }

    static class LinkedList {
        public int value;
        public LinkedList next;

        public LinkedList(int value) {
            this.value = value;
            next = null;
        }
    }
}
