package com.algo.expert.medium;

import java.util.Arrays;

public class RangeSearch {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(searchForRange(new int[] {1, 3, 3, 3, 4, 5, 7, 9}, 3)));

    }

    public static int[] searchForRange(int[] array, int target) {
        int[] finalRange = {-1,-1};
        searchForRange(array, target, 0, array.length - 1, true,  finalRange);
        searchForRange(array, target, 0, array.length - 1, false,  finalRange);
        return finalRange;
    }

    public static void searchForRange(int[] array, int target, int left, int right, boolean goLeft, int[] finalRange) {
        int start = left;
        int end = right;
        while (end >= start) {
            int mid  = start + (end - start)/2;
            if ( array[mid] > target) {
                end = mid - 1 ;
            } else if (array[mid]  < target) {
                start = mid + 1;
            } else  {
                if (goLeft) {
                    if (mid == 0 || array[mid - 1]!= target) {
                        finalRange[0] = mid;
                        return;
                    } else {
                        end = mid - 1;
                    }
                } else {
                    if (mid == array.length - 1 || array[mid + 1] != target) {
                        finalRange[1] = mid;
                        return;
                    } else  {
                        start = mid + 1;
                    }
                }
            }
        }
    }


}
