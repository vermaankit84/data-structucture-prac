package com.algo.expert.classes.trees.traversal;

import com.algo.expert.classes.trees.TreeNode;
import com.algo.expert.classes.trees.TreeUtils;

import java.util.Stack;

public class PreOrder {
    public static void main(String[] args) {
        TreeNode root = TreeUtils.buildTree();
        System.out.println("******* pre order traversal **********");
        preOrder(root);

        System.out.println("\n******* in order traversal **********");
        inorder(root);

        System.out.println("\n******* post order traversal **********");
        postOrder(root);

        System.out.println("\n*******iterative post order traversal **********");
        preOrderIterative(root);
    }

    private static void preOrderIterative(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);
        while (!stack.isEmpty()) {
            TreeNode temp = stack.pop();
            System.out.print(temp.getValue() + " ");
            if (temp.getRight()!=null) {
                stack.add(temp.getRight());
            }

            if (temp.getLeft()!=null) {
                stack.add(temp.getLeft());
            }
        }

    }

    private static void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }


        postOrder(root.getLeft());
        postOrder(root.getRight());
        System.out.print(root.getValue() + " ");
    }

    private static void inorder(TreeNode root) {
        if (root == null) {
            return;
        }


        inorder(root.getLeft());
        System.out.print(root.getValue() + " ");
        inorder(root.getRight());
    }

    private static void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }

        System.out.print(root.getValue() + " ");
        preOrder(root.getLeft());
        preOrder(root.getRight());
    }
}
