package com.algo.expert.designpatterns.structural.proxy;

// remote proxy, virtual proxy, protection proxy
public class ProxyDesignPatternImpl {
    public static void main(String[] args) {
        Subject s1 = new ProxyClass("Ankit");
        s1.doWork();

        Subject s2 = new ProxyClass("Priya");
        s2.doWork();
    }
}
