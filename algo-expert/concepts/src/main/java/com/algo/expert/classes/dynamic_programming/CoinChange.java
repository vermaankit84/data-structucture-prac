package com.algo.expert.classes.dynamic_programming;


public class CoinChange {
    public static void main(String[] args) {
        int [] coins = {3, 5};
        System.out.println(coin_change(coins, 10));
        System.out.println(coin_change(coins, 11));
        System.out.println(coin_change(coins, 19));
    }

    private static int coin_change(int[] coins, int target) {

        int dp[] = new int[target + 1];
        dp[0] =  0;

        for(int i = 1; i < target + 1; i = i  + 1) {
            for(int coin : coins) {
                int min_value = Integer.MAX_VALUE;
                if ((i - coin) >= 0) {
                    min_value = Math.min(min_value, dp[i - coin]);
                    dp[i] = min_value + 1;
                }
            }
        }

       return dp[target] == Integer.MAX_VALUE ? - 1 : dp[target];
    }
}
