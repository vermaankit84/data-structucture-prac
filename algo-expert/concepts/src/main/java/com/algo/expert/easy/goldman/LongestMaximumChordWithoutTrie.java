package com.algo.expert.easy.goldman;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestMaximumChordWithoutTrie {
    public static void main(String[] args) {
        Map<String, String> dict = new HashMap<>();
        dict.put("are", "are");
        dict.put("area", "area");
        dict.put("base", "base");
        dict.put("cat", "cat");
        dict.put("cater" ,"cater");
        dict.put("basement", "basement");

        String input = "caterer";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));
        input = "basement";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));

        input = "are";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));

        input = "arex";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));

        input = "basemexz";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));

        input = "xyz";
        System.out.print(input + ":   ");
        System.out.println(getMatchingPrefix(input, dict));
    }

    private static String getMatchingPrefix(String input, Map<String, String> dict) {
        for (int i = input.length() - 1; i > -1; i -= 1) {
            String temp = input.substring(0, i + 1);
            if (dict.containsKey(temp)) {
                return dict.get(temp);
            }
        }

        return "";
    }
}
