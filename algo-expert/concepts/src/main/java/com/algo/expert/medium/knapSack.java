package com.algo.expert.medium;

import com.algo.expert.designpatterns.structural.decorator.PaintComponent;

import java.util.*;

public class knapSack {
    public static void main(String[] args) {
        int [][] items = {
                {1, 2},
                {4, 3},
                {5, 6},
                {6, 7}
        };

        System.out.println(knapsackProblem(items, 10));
    }

    public static List<List<Integer>> knapsackProblem(int[][] items, int capacity) {
        int dp[][] = new int[items.length + 1][capacity + 1];
        for (int i = 1; i <items.length + 1 ; i++) {
            int curr_weight = items[i - 1][1];
            int curr_profit = items[i - 1][0];

            for (int curr_capacity = 0; curr_capacity < capacity + 1; curr_capacity++) {
                if (curr_weight > curr_capacity) {
                    dp[i][curr_capacity] = dp[i - 1][curr_capacity];
                } else {
                    dp[i][curr_capacity] = Math.max(dp[i - 1][curr_capacity] , dp[i - 1][curr_capacity - curr_weight] + curr_profit);
                }
            }
        }
        return getItems(dp, items, dp[items.length][capacity]);
    }

    private static List<List<Integer>> getItems(int[][] dp, int[][] items, int weight) {
        List<List<Integer>> sequence = new ArrayList<>();
        List<Integer> weights = new ArrayList<>();
        weights.add(weight);
        sequence.add(weights);
        sequence.add(new ArrayList<>());
        int i = dp.length - 1;
        int c = dp[0].length - 1;

        while (i > 0) {
            if (dp[i][c] != dp[i - 1][c]) {
                sequence.get(1).add(0, i - 1);
                c = c - items[i - 1][1];
            }
            i = i - 1;
            if (c == 0) {
                break;
            }
        }

        return sequence;
    }
}
