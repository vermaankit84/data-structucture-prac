package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.List;

public class ZipLinkedList {
    public static void main(String[] args) {

    }

    public static class LinkedList {
        public int value;
        public LinkedList next;
        public LinkedList head;
        public LinkedList(int value) {
            this.value = value;
            this.next = null;
        }
    }

    public LinkedList zipLinkedList(LinkedList linkedList) {
        if (linkedList.next == null || linkedList.next.next == null) return linkedList;

        return interweave(linkedList, reverse(split(linkedList)));
    }

    private LinkedList interweave(LinkedList firstHalf, LinkedList secondhalf) {
        LinkedList firstItr = firstHalf;
        LinkedList secondItr = secondhalf;

        while (firstItr!=null && secondItr!=null) {
            LinkedList firsthalfNext = firstItr.next;
            LinkedList secondhalfNext = secondItr.next;

            firstItr.next = secondItr;
            secondItr.next = firsthalfNext;

            firstItr = firsthalfNext;
            secondItr = secondhalfNext;
        }

        return firstHalf;
    }


    private LinkedList reverse(LinkedList split) {
        LinkedList previousNode = null;
        LinkedList currentNode = split;

        while (currentNode!=null) {
            LinkedList nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        return previousNode;
    }

    private LinkedList split(LinkedList linkedList) {
        LinkedList slow = linkedList;
        LinkedList fast = linkedList;

        while (fast!=null && fast.next!=null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        assert slow != null;
        LinkedList secondhalf = slow.next;
        slow.next = null;
        return secondhalf;

    }
}
