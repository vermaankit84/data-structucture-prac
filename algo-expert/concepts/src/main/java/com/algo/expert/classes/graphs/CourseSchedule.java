package com.algo.expert.classes.graphs;


import java.util.*;

public class CourseSchedule {
    public static void main(String[] args) {
        int n = 4;
        int [][] input = { { 1 ,0 }, {2, 0}, {3, 1}, {3, 2}};
        List<List<Integer>> prerequisites = new ArrayList<>();

        for (int[] i: input) {
            List<Integer> temp = new ArrayList<>();
            temp.add(i[0]);
            temp.add(i[1]);
            prerequisites.add(temp);
        }
        System.out.println(course_schedule(n , prerequisites));
    }

    private static  int timestamp = 0;

    static List<Integer> course_schedule(int n, List<List<Integer>> prerequisites) {
        Graph graph = new Graph(n);
        for (List<Integer> prerequisite : prerequisites) {
            int n1 = prerequisite.get(0);
            int n2 = prerequisite.get(1);

            graph.addEdge(n1, n2);
        }

        return course_schedule(graph, n);
    }

    static List<Integer> course_schedule(Graph graph, int n) {
        boolean [] visisted =  new boolean[n];
        Arrays.fill(visisted, false);

        int [] arrival = new int[n];
        Arrays.fill(arrival, -1);

        int [] departure = new int[n];
        Arrays.fill(departure, -1);

        List<Integer> schedule = new ArrayList<>();

        LinkedList<Integer>[] adj = graph.adj;
        for (int vertex = 0; vertex < n; vertex = vertex + 1) {
            if (!visisted[vertex]) {
                if (dfs_has_cycle(vertex, arrival, departure, visisted, schedule, adj)) {
                    List<Integer> temp = new ArrayList<>();
                    temp.add(-1);
                    return temp;
                }
            }
        }


        return schedule;
    }

    private static boolean dfs_has_cycle(int vertex, int[] arrival, int[] departure, boolean[] visisted, List<Integer> schedule, LinkedList<Integer>[] adj) {
        visisted[vertex] = true;
        arrival[vertex] = timestamp++;

        for (int i = 0; i < adj[vertex].size(); i = i + 1) {
            int neibhour = adj[vertex].get(i);
            if (!visisted[neibhour]) {
                visisted[neibhour] = true;
                if (dfs_has_cycle(neibhour, arrival, departure, visisted, schedule, adj)) {
                    return true;
                }
            } else {
                if (departure[neibhour] == -1) {
                    return true;
                }
            }
        }

        departure[vertex] = timestamp++;
        schedule.add(vertex);
        return false;
    }

    static class Graph {
       public int size;
       public LinkedList<Integer> adj[];

       public Graph(int size) {
           this.size = size;
           this.adj = new LinkedList[size];
           for (int i = 0; i < size; i++) {
               adj[i] = new LinkedList<>();
           }
       }

       public void addEdge(int start, int end) {
           this.adj[start].add(end);
       }
    }
}
