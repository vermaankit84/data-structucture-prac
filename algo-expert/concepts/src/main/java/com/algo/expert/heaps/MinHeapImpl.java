package com.algo.expert.heaps;

import java.util.*;

public class MinHeapImpl {
    public static void main(String[] args) {

    }

    static class MinHeap {
        List<Integer> heap = new ArrayList<Integer>();
        private final PriorityQueue<Integer> queue = new PriorityQueue<>();

        public MinHeap(List<Integer> array) {
            heap = buildHeap(array);
        }

        public List<Integer> buildHeap(List<Integer> array) {
            List<Integer> heap = new ArrayList<>();
            PriorityQueue<Integer> tempQueue = new PriorityQueue<>();
            queue.addAll(array);
            tempQueue.addAll(array);

            while (!tempQueue.isEmpty()) {
                heap.add(tempQueue.poll());
            }
            return heap;
        }

        public void siftDown(int currentIdx, int endIdx, List<Integer> heap) {
            // Write your code here.
        }

        public void siftUp(int currentIdx, List<Integer> heap) {
            // Write your code here.
        }

        public int peek() {
            return queue.peek()!=null ? queue.peek() : 0;
        }

        public int remove() {
            return queue.peek()!=null ? queue.remove() : 0;
        }

        public void insert(int value) {
            queue.offer(value);
        }
    }
}