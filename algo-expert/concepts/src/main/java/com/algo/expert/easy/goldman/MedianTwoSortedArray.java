package com.algo.expert.easy.goldman;

import java.util.Arrays;

public class MedianTwoSortedArray {
    public static void main(String[] args) {
        int arr1[] = {1, 3, 8, 9, 9, 15};
        int arr2[] = {7, 11, 13, 13, 18, 19, 21, 25};

        int combined[] = new int[arr1.length + arr2.length];
        int counter = 0;
        for (int j : arr1) {
            combined[counter] = j;
            counter += 1;
        }

        for (int j : arr2) {
            combined[counter] = j;
            counter += 1;
        }
        Arrays.sort(combined);
        int mid = (combined.length)/2;
        double median = 0;
        if (mid%2 == 0) {
            median = (combined[mid] + combined[mid + 1])/2d;
        } else  {
            median = combined[mid];
        }

        System.out.println(median);


        System.out.println(optimized(arr1, arr2));
    }

    private static double optimized(int[] arr1, int[] arr2) {
        if (arr1.length > arr2.length) {
            return optimized(arr2, arr1);
        }

        int start = 0;
        int end = arr1.length;
        while (start <= end) {
            int p_x = (start + end) / 2;
            int p_y = (arr1.length + arr2.length + 1)/2 - p_x;

            int max_left_a_1 = p_x == 0 ? Integer.MIN_VALUE : arr1[p_x - 1];
            int min_right_a_1 = p_x == 0 ? Integer.MAX_VALUE : arr1[p_x];

            int max_left_a_2 = p_y == 0 ? Integer.MIN_VALUE : arr2[p_y - 1];
            int min_right_a_2 = p_y == 0 ? Integer.MAX_VALUE : arr2[p_y];


            if (max_left_a_1 <= min_right_a_2 && max_left_a_2 <=  min_right_a_1) {
                System.out.println("Found");
                if ((arr1.length + arr2.length) % 2 == 0) {
                    return Math.max(Math.max(max_left_a_1, max_left_a_2), Math.min(min_right_a_1, min_right_a_2));
                } else {
                    return Math.max(max_left_a_1, max_left_a_2);
                }
            } else if (max_left_a_1 > min_right_a_2) {
                end = p_x - 1;
            } else {
                start = p_x + 1;
            }
        }

        throw new UnsupportedOperationException("ha ha wrong algo impl");
    }

}
