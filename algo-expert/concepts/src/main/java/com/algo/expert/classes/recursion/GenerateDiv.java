package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.List;

public class GenerateDiv {
    public static void main(String[] args) {
        System.out.println(generateDivTags(3));
    }

    public static ArrayList<String> generateDivTags(int numberOfTags) {
        ArrayList<String> response = new ArrayList<>();
        helper(numberOfTags, "", 0, 0, response);
        return response;
    }

    private static void helper(int n, String s, int open, int close, ArrayList<String> response) {
        if (open == n && close == n) {
            response.add(s);
            return;
        }


        if (open < n) {
            helper(n, s + "<div>", open + 1, close, response);
        }

        if (close < open) {
            helper(n, s + "</div>", open, close + 1, response);
        }
    }
}
