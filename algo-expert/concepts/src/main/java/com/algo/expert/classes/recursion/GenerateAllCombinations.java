package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.*;

public class GenerateAllCombinations {
    public static void main(String[] args) {
        List<Integer> input = Lists.newArrayList(1, 2, 3, 4);
        System.out.println(generate_all_combinations(input, 3));

        input = Lists.newArrayList(1, 1, 1, 1);
        System.out.println(generate_all_combinations(input, 2));
    }

    public static List<List<Integer>> generate_all_combinations(List<Integer> arr, int target) {
       Collections.sort(arr);
       List<List<Integer>> response = new ArrayList<>();
        helper(arr, target, new ArrayList<>(), response, 0);
        return response;
    }

    private static void helper(List<Integer> input, int target, List<Integer> slate, List<List<Integer>> response, int idx) {
        if (target < 0) return;

        if (target == 0) {
            response.add(new ArrayList<>(slate));
            return;
        }

        if (idx >= input.size()) {
            return;
        }

        for (int i = idx; i < input.size(); i++) {
            int current = input.get(i);
            if (i > idx && input.get(i - 1) == current)
                continue;
            slate.add(current);
            helper(input, target - current, slate, response, i + 1);
            slate.remove(slate.size() - 1);
        }
    }

}
