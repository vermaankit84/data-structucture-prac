package com.algo.expert.hard;

import java.util.Arrays;

public class CycleInGraph {

    public int WHITE = 1;
    public int GREY = 2;
    public int BLACK = 3;

    public boolean cycleInGraph(int[][] edges) {
       int  noOfNodes = edges.length;
       int [] colors = new int[noOfNodes];

        Arrays.fill(colors, WHITE);

        for (int i = 0; i < noOfNodes; i = i + 1) {
            if (colors[i] != WHITE) continue;

            if (traverseAndColorNodes(i, edges, colors)) {
                return true;
            }
        }

        return false;
    }

    private boolean traverseAndColorNodes(int i, int[][] edges, int[] colors) {
        colors[i] = GREY;
        int[] neighbours = edges[i];
        for (int n: neighbours) {
            int neighbours_color = colors[n];
            if (neighbours_color == GREY) return true;

            if (neighbours_color == BLACK) continue;

            if (traverseAndColorNodes(n, edges, colors)) {
                return true;
            }
        }

        colors[i] = BLACK;
        return false;
    }
}
