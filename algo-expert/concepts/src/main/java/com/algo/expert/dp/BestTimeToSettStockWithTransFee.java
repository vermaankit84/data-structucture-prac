package com.algo.expert.dp;

public class BestTimeToSettStockWithTransFee {
    public static void main(String[] args) {
        int prices[] = {1,3,2,8,4,9};
        int trans_fee = 2;
        System.out.println(maxProfit(prices, trans_fee));

    }

    public  static int maxProfit(int[] prices, int fee) {
       int cash = 0;
       int hold = -prices[0];

        for (int i = 1; i < prices.length; i++) {
            cash = Math.max(cash, prices[i] + hold - fee);
            hold = Math.max(hold, cash - prices[i]);
        }

        return Math.max(cash, hold);
    }
}
