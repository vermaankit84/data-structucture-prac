package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.*;

public class PossibleToAchine {
    public static void main(String[] args) {
        System.out.println(check_if_sum_possible(Lists.newArrayList(-50L), 5L));
    }

    static Boolean check_if_sum_possible(ArrayList<Long> arr, Long k) {
        Collections.sort(arr);
        Long[] cnadidates = new Long[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            cnadidates[i] = arr.get(i);
        }
        List<List<Long>> combinations =combinationSum(cnadidates, k);
        System.out.println(combinations);
        return combinations.size() > 0;
    }

    public static List<List<Long>> combinationSum(Long[] candidates, Long target) {
        List<List<Long>> result=new LinkedList<>();
        List<Long> temp=new LinkedList<>();
        combina(candidates,0,target,0L,temp,result);
        return result;
    }
    static void combina(Long[] candidates,int start,Long target,Long sum, List<Long> temp,List<List<Long>> result)
    {
        if(sum>target)
            return;
        if(sum.equals(target))
            result.add(new LinkedList<Long>(temp));

        for(int i=start;i<candidates.length;i++)
        {
            temp.add(candidates[i]);
            combina(candidates,i,target,sum+candidates[i],temp,result);
            temp.remove(temp.size()-1);
        }
    }
}
