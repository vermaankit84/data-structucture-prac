package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class GenerateSubsets {
    public static void main(String[] args) {
        List<Integer> list = Lists.newArrayList(1, 2,3);
        System.out.println(generateSubsets(list));
    }

    private static List<List<Integer>> generateSubsets(List<Integer> dataSet) {
        List<List<Integer>> subsets = new ArrayList<>();
        helper(0, dataSet, new Stack<>(), subsets);
        return subsets;
    }

    private static void helper(int idx,  List<Integer> dataSet, Stack<Integer> partial, List<List<Integer>> subsets) {
        if (idx == dataSet.size()) {
            subsets.add(new ArrayList<>(partial));
            return;
        }

        helper(idx + 1, dataSet, partial, subsets);
        partial.push(dataSet.get(idx));
        helper(idx + 1, dataSet, partial, subsets);
        partial.pop();
    }
}
