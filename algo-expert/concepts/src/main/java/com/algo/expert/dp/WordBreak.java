package com.algo.expert.dp;

import java.util.List;

import com.google.common.collect.Lists;

public class WordBreak {
    public static void main(String[] args) {
        System.out.println(wordBreak("leetcode", Lists.newArrayList("leet", "code")));
        System.out.println(wordBreak("applepenapple", Lists.newArrayList("apple", "pen")));
        System.out.println(wordBreak("catsandog", Lists.newArrayList("cats","dog","sand","and","cat")));
    }

    public static boolean wordBreak(String s, List<String> wordDict) {
       boolean[] dp = new boolean[s.length() + 1];
       dp[0] = true;
       for (int i = 1; i < s.length() + 1; i++) {
           for (int j = 0; j < i; j++) {
               String current = s.substring(j, i);
               if (wordDict.contains(current) && dp[j]) {
                   dp[i] = true;
                   break;
               }
           }
       }
       return dp[s.length()];
    }

    
}
