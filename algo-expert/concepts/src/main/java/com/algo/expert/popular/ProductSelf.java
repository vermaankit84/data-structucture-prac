package com.algo.expert.popular;

import java.util.Arrays;

public class ProductSelf {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(productExceptSelf(new int[] {1,2,3,4})));
        System.out.println(Arrays.toString(productExceptSelf(new int[] {-1,1,0,-3,3})));
        System.out.println(Arrays.toString(productExceptSelf(new int[] {2, 3, -2, 4})));
        // System.out.println(Arrays.toString(productExceptSelf(new int[] {})));
    }

    public static int[] productExceptSelf(int[] nums) {
        int left[] = new int[nums.length];
        Arrays.fill(left, 1);
        for (int i = 1; i < nums.length; i++) {
            left[i] = left[i - 1] * nums[i - 1];
        }

        int right[] = new int[nums.length];
        Arrays.fill(right, 1);
        for (int i = nums.length - 2; i > - 1 ; i--) {
            right[i] = right[ i + 1] * nums[i + 1];
        }

        for (int i = 0; i < nums.length; i++) {
            nums[i] = left[i] * right[i];
        }

        return nums;
    }
}
