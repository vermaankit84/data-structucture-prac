package com.algo.expert.easy.goldman;

import java.util.Vector;

public class LargestTreeForest {
    public static void main(String[] args) {
        int V = 5;
        Vector<Integer> adj[] = new Vector[V + 1];
        for (int i = 0; i <= 5; i++) {
            adj[i] = new Vector<>();
        }

        addEdge(adj, 0, 1);
        addEdge(adj, 0, 2);
        addEdge(adj, 3, 4);
        addEdge(adj, 0, 4);
        addEdge(adj, 3, 5);
        System.out.println(largestTree(adj, V));
    }

    private static void addEdge(Vector<Integer> adj[], int u, int v)
    {
        adj[u].add(v);
        adj[v].add(u);
    }

    private static int DFSUtil(int u, Vector<Integer> adj[], Vector<Boolean> visited) {
        visited.add(u, true);
        int sz = 1;

        for (int i = 0; i < adj[u].size(); i++) {
            if (!visited.get(adj[u].get(i))) {
                sz = sz + DFSUtil(adj[u].get(i), adj, visited);
            }
        }
        return sz;
    }

    private static int largestTree(Vector<Integer> adj[], int V) {
        Vector<Boolean> visted = new Vector<>();
        for (int i = 0; i < V; i++) {
            visted.add(false);
        }

        int answer = 0;

        for(int u =0; u < V; u = u + 1) {
            if (!visted.get(u)) {
                answer = Math.max(answer, DFSUtil(u, adj, visted));
            }
        }

        return answer;
    }
}
