package com.algo.expert.dp;

import java.util.Arrays;

public class BestSum {
    public static void main(String[] args) {
        int[] arr = {2, 4, 3, 7};
        int[] r = new int[arr.length];
        System.out.println(Arrays.toString(targetSum(arr, 8, r)));
    }

    private static int[] targetSum(int[] arr, int target, int[]r) {
        if (target == 0) return r;
        if (target < 0) return null;
        int length = -1;
        for (int j : arr) {
            int response[] = targetSum(arr, target - j, r);
            if (response != null && Arrays.stream(response).sum()!=0) {
                if (length < response.length) {
                    length = response.length;
                    for (int i = 0; i < response.length; i++) {
                        r[i] = response[i];
                    }
                }
            }
        }

        return r;
    }
}
