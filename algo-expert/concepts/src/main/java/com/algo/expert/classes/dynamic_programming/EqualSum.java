package com.algo.expert.classes.dynamic_programming;

import java.util.*;

public class EqualSum {
    public static void main(String[] args) {
        int nums[] = {10, -3, 7, 2, 1, 3};
        int sum = Arrays.stream(nums).sum()/ 2;

        System.out.println(can_partation(nums, sum));
        
        
    }


    private static boolean can_partation(int input[], int total) {
        boolean T[][] = new boolean[input.length + 1][total + 1];
        for (int i = 0; i <= input.length; i++) {
            T[i][0] = true;
        }

        for (int i = 1; i <= input.length; i++) {
            for (int j = 1; j <= total; j++) {
                if (j - input[i - 1] >= 0) {
                    T[i][j] = T[i - 1][j] || T[i - 1][j - input[i - 1]];
                } else {
                    T[i][j] = T[i-1][j];
                }
            }
        }
        return T[input.length][total];
    }
}
