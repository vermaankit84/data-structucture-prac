package com.algo.expert.easy.goldman;

import java.util.*;

public class DictioneryProblem {

    public static void main(String[] args) {
        pass();
    }

    public static void pass() {
        Dictionary dict = new Dictionary(new String[] { "to", "toe", "toes", "doe", "dog", "god", "dogs", "banana" });
        System.out.println("toe".equals(longestWord("toe", dict)));
        System.out.println("to".equals(longestWord("to", dict)));
        System.out.println("do".equals(longestWord("doe", dict)));
    }

    private static String longestWord(String data, Dictionary dict) {
        for (int i = data.length(); i > -1; i--) {
            String test = data.substring(0, i);
            String response = dict.getValue(test);
            if (response!=null) return response;
        }
       return null;
    }
}

class Dictionary {

    private final Map<String, String> dict = new HashMap<>();

    public Dictionary(String[] entries) {
       Arrays.stream(entries).forEach(e -> dict.put(e, e));
    }

    public String getValue(String word) {
        return dict.get(word);
    }

}
