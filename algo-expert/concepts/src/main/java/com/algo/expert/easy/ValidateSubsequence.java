package com.algo.expert.easy;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;

public class ValidateSubsequence {

    public static void main(String[] args) {
       System.out.println(isValidSubsequence( Lists.newArrayList(5, 1, 22, 25, 6, -1, 8, 10),
                Lists.newArrayList(1, 6, -1, 10)));
    }

    public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {

        int seq_idx = 0;
        int arr_idx = 0;

        while (seq_idx < sequence.size() && arr_idx < array.size()) {
            if (Objects.equals(array.get(arr_idx), sequence.get(seq_idx))) {
                seq_idx ++;
            }
            arr_idx++;
        }

        return seq_idx == sequence.size();
    }
}
