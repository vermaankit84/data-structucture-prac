package com.algo.expert.designpatterns.structural.composite;

public class CompositeImpl {
    public static void main(String[] args) {
        System.out.println("demo of the composite pattern");

        Employee math_1 = new SimpleEmployee("A", "Math - dept");
        Employee math_2 = new SimpleEmployee("B", "Math - dept");


        Employee cs_1 = new SimpleEmployee("A", "CS - dept");
        Employee cs_2 = new SimpleEmployee("B", "CS - dept");

        CompositeEmployee hod_maths = new CompositeEmployee("C1", "Maths-dept");
        hod_maths.addEmployee(math_1);
        hod_maths.addEmployee(math_2);

        CompositeEmployee hod_cs = new CompositeEmployee("C2", "CS-Dept");
        hod_cs.addEmployee(cs_1);
        hod_cs.addEmployee(cs_2);

        CompositeEmployee principal = new CompositeEmployee("P", "Main");
        principal.addEmployee(hod_maths);
        principal.addEmployee(hod_cs);

        System.out.println("testing the structute of the principal");
        principal.printStructures();

        System.out.println("total no of people working under prinicpal " + principal.getEmployeeCount());
    }
}
