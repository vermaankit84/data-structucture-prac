package com.algo.expert.designpatterns.structural.Bridge;

public abstract class ElectronicGoods {

    private State state;


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void moveToCurrentState() {
        System.out.println("this item is functioning at");
        state.moveState();
    }

    public void  hardButtonPressed() {
        state.hardPressed();
    }
}
