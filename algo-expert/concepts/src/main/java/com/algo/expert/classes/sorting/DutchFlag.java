package com.algo.expert.classes.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class DutchFlag {
    public static void main(String[] args) {
        dutch_flag_sort(new char[]{ 'B', 'R', 'G'});
    }

    public static void dutch_flag_sort(char[] balls) {
       /*Map<Character, Integer> countMap = new HashMap<>();
        for(char c : balls) {
            countMap.put(c , countMap.getOrDefault(c, 0) + 1);
        }

        int count_red = countMap.getOrDefault('R', 0);
        int counter = 0;
        if (count_red!=0) {
            while (counter < count_red) {
                balls[counter] = 'R';
                counter = counter + 1;
            }
        }


        int count_green = countMap.getOrDefault('G', 0);
        if (count_green!=0) {
            while (counter < (count_green + count_red)) {
                balls[counter] = 'G';
                counter = counter + 1;
            }
        }


        int count_blue = countMap.getOrDefault('B', 0);
        if (count_blue!=0) {
            while (counter < (count_green + count_red + count_blue)) {
                balls[counter] = 'B';
                counter = counter + 1;
            }
        }


        System.out.println(Arrays.toString(balls));
*/


        int i = 0;
        int r = -1;
        int g = -1;

        while (i < balls.length) {
            if (balls[i] == 'R') {
                r = r + 1;
                char temp = balls[r];
                balls[r] = balls[i];
                balls[i] = temp;
            } else if (balls[i] == 'G') {
                r = r + 1;
                char temp = balls[r];
                balls[r] = balls[i];
                balls[i] = temp;
                g = g + 1;
                temp = balls[g];
                balls[g] = balls[i];
                balls[i] = temp;
            }
            i = i + 1;
        }


        System.out.println(Arrays.toString(balls));
    }
}
