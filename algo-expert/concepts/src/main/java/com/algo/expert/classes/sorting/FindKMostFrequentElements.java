package com.algo.expert.classes.sorting;

import com.google.common.collect.Lists;

import java.util.*;

public class FindKMostFrequentElements {
    public static void main(String[] args) {
        ArrayList<Integer> arr = Lists.newArrayList(1, 2, 1, 2, 3, 1);
        System.out.println(find_top_k_frequent_elements(arr, 1));
    }

    public static ArrayList<Integer> find_top_k_frequent_elements(ArrayList<Integer> arr, Integer k) {
        Map<Integer, Integer> frequency = new HashMap<>();

        arr.forEach(d -> frequency.put(d , frequency.getOrDefault(d, 0 )  + 1));

        Queue<Pair> queue = new PriorityQueue<>((o1, o2) -> Integer.compare(o2.count, o1.count));
        frequency.forEach((key, value) -> queue.add(new Pair(key, value)));
        ArrayList<Integer> top = new ArrayList<>();
        int counter = 0;
        while (counter < k && !queue.isEmpty()) {
            top.add(queue.poll().s);
            counter = counter + 1;
        }
        return top;
    }

    private static class Pair {
        Integer s;
        int count;

        public Pair(Integer s, int count) {
            this.s = s;
            this.count = count;
        }
    }
}
