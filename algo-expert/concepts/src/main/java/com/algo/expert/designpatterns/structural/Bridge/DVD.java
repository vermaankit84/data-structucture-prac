package com.algo.expert.designpatterns.structural.Bridge;

public class DVD extends ElectronicGoods {
    public void doublePress() {
        hardButtonPressed();
        hardButtonPressed();
    }
}
