package com.algo.expert.easy.goldman;

import java.util.*;

public class CountNoOfTreeForest {
    public static void main(String[] args) {
        int edges[][] = {{0, 1}, {0, 2}, {3, 4}, {0, 4}, {3, 5}};

        int V = 5;
        Vector<Integer> adj[] = new Vector[V + 1];
        for (int i = 0; i <= V; i++) {
            adj[i] = new Vector<>();
        }

        for (int [] edge: edges) {
            addEdge(edge[0], edge[1], adj);
        }

        System.out.println(getCount(V, adj));
    }

    private static int getCount(int V,  Vector<Integer> adj[]) {
        Boolean[] visited = new Boolean[V + 1];
        for (int i = 0; i <= V; i++) {
            visited[i] = false;
        }

        int count = 0;
        for (int u = 0; u < V; u++) {
            if (!visited[u]) {
                DFSUtil(u, visited, adj);
                count += 1;
            }
        }

        return count;
    }

    private static void DFSUtil(int u, Boolean[] visited, Vector<Integer>[] adj) {
        visited[u] = true;


        for (int i = 0; i < adj[u].size(); i++) {
            if (!visited[adj[u].get(i)]) {
                DFSUtil(adj[u].get(i), visited, adj);
            }
        }
    }

    private static void addEdge(int u, int v, Vector<Integer>[] adj) {
        adj[u].add(v);
        adj[v].add(u);
    }


}
