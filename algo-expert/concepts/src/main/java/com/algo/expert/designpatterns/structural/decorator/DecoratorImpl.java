package com.algo.expert.designpatterns.structural.decorator;

public class DecoratorImpl {
    public static void main(String[] args) {
        ConcreteHouse house = new ConcreteHouse();
        house.makeHouse();

        System.out.println("________________________________");
        FloorComponent floorComponent = new FloorComponent();
        floorComponent.setComponent(house);
        floorComponent.makeHouse();
        System.out.println("________________________________");
        
        PaintComponent paintComponent = new PaintComponent();
        paintComponent.setComponent(house);
        paintComponent.makeHouse();;
    }
}
