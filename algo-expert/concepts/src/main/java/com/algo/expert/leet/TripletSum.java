package com.algo.expert.leet;

import java.util.*;

public class TripletSum {
    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        if (nums.length == 0) return new ArrayList<>();
        if (nums.length== 1 && nums[0] == 0) return new ArrayList<>();
        List<List<Integer>> response = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(i, nums[i]);
        }

        List<Integer> list = new ArrayList<>();
        for (int num : nums) {
            if (!list.contains(num)) {
                list.add(num);
            }
        }
       
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                for (int k = j + 1; k < list.size(); k++) {
                    if (list.get(i) + list.get(j) + list.get(k) == 0) {
                        List<Integer> l = new ArrayList<>();
                        l.add(nums[i]);
                        l.add(nums[j]);
                        l.add(nums[k]);
                        response.add(l);
                    }
                }
            }
        }

        return response;
    }

    static class Triplet implements Comparable<Integer>{
        int i1;
        int i2;
        int i3;

        @Override
        public int compareTo(Integer o) {
            return 0;
        }
    }
}
