package com.algo.expert.test;

import java.util.Arrays;
import java.util.Stack;

public class MaximumAreaHistogram {
    public static void main(String[] args) {
        int [] arr = {4, 2, 1, 5, 6, 3, 2, 4, 2};
        int max_area = maximum_area_histogram(arr);
        System.out.println(max_area);
    }

    public static int maximum_area_histogram(int[] arr) {
        int max_area = - 1;

        int[] previous_smaller = previous_smaller(arr);
        System.out.println(Arrays.toString(previous_smaller));

        int[] next_smaller = next_smaller(arr);
        System.out.println(Arrays.toString(next_smaller));
        for (int i = 0; i < arr.length; i++) {
            int left = previous_smaller[i];
            int right = next_smaller[i];
            if (left!=0 && right!=0) {
                max_area = Math.max( (right - left - 1) * arr[i] , max_area);
            }
        }

        return max_area;
    }

    private static int[] next_smaller(int [] arr) {
        Stack<Integer> stack = new Stack<>();
        int next_smaller[] = new int[arr.length];
        stack.push(arr.length - 1);
        next_smaller[arr.length - 1] = 9;

        for (int i = arr.length - 2; i > -1 ; i = i - 1) {
            int top = arr[stack.peek()];
            int current = arr[i];
            if (current < top) {
                while ((!stack.isEmpty()) && (arr[stack.peek()] > current )) {
                    stack.pop();
                }
            }

            if (!stack.isEmpty()) {
                next_smaller[i] = stack.peek();
            } else  {
                next_smaller[i] = -1;
            }
            stack.push(i);

        }

        return next_smaller;
    }

    private static int[] previous_smaller(int [] arr) {
        int prev_smaller[] = new int[arr.length];
        prev_smaller[0] = -1;

        Stack<Integer> stack = new Stack<>();
        stack.add(0);
        for (int i = 1; i < arr.length; i++) {
            int top = arr[stack.peek()];
            int current = arr[i];
            if (top > current) {
                while (!stack.isEmpty() && (arr[stack.peek()] >= current)) {
                    stack.pop();
                }
            }

            if (!stack.isEmpty()) {
                prev_smaller[i] = stack.peek();
            } else  {
                prev_smaller[i] = -1;
            }
            stack.push(i);
            }

        return prev_smaller;
    }
}

