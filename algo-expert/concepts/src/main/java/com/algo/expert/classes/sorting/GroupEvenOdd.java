package com.algo.expert.classes.sorting;

import java.util.*;

public class GroupEvenOdd {
    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 7, 17, 7, 8, 16, 12, 12};
        /*
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j]%2 == 0) {
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                    break;
                }
            }
        }
        */
        System.out.println(Arrays.toString(arr));

        Map<String, List<Integer>> map = new HashMap<>();


        int i = 0;
        int j = -1;

        while (i < arr.length) {
            if (arr[i]%2 == 0) {
                j = j + 1;
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }

            i = i + 1;
        }


        System.out.println(Arrays.toString(arr));


    }
}
