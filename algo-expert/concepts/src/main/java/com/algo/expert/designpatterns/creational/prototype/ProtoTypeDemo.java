package com.algo.expert.designpatterns.creational.prototype;

public class ProtoTypeDemo {
    public static void main(String[] args) {
        Shape clone1 = ShapeCache.getShape("1");
        System.out.println(clone1.getType());

        Shape clone2 = ShapeCache.getShape("2");
        System.out.println(clone2.getType());
    }
}
