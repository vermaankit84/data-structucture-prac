package com.algo.expert.medium;

public class MaxSubSetSum {
    public static void main(String[] args) {

    }

    public static int maxSubsetSumNoAdjacent(int[] array) {
        if (array.length == 0) return 0;
        if (array.length == 1) return array[0];

        int []max_sums = array.clone();

        max_sums[0] = Math.max(array[0], array[1]);

        for (int i = 2; i < array.length; i = i +1 ) {
            max_sums[i] = Math.max(max_sums[i - 1], max_sums[i - 2] + array[i]);
        }


        return max_sums[max_sums.length - 1];
    }
}
