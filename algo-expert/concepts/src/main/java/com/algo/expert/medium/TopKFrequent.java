package com.algo.expert.medium;

import java.util.*;
import java.util.function.BiConsumer;

public class TopKFrequent {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(topKFrequent(new int[] {1,1,1,2,2,3}, 2)));
        System.out.println(Arrays.toString(topKFrequent(new int[] {1 }, 1)));
    }

    public static int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> count = new HashMap<>();
        Arrays.stream(nums).forEach(i -> count.put(i , count.getOrDefault(i, 1) + 1));
        PriorityQueue<Pair> pairs = new PriorityQueue<>((p1, p2) -> Integer.compare(p2.count, p1.count));
        count.forEach((key , value) -> pairs.add(new Pair(key, value)));

        int counter = 0;
        int [] response = new int[k];
        while (counter < k) {
            if(pairs.isEmpty()) {
                return response;
            }
            response[counter] = pairs.poll().element;
            counter = counter + 1;
        }

        return response;
    }

    static class Pair {
        int element;
        int count;

        public Pair(int element, int count) {
            this.count = count;
            this.element = element;
        }

        public String toString() {
            return this.element + " " + this.count;
        }
    }
}
