package com.algo.expert.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class HeapSort {
    public static void main(String[] args) {
        int arr[] = {4, 5, 6, 3, 7, 8, 1, 6, 2, 8};
        sort(arr, Comparator.reverseOrder());
        System.out.println(Arrays.toString(arr));

        sort(arr, Comparator.naturalOrder());
        System.out.println(Arrays.toString(arr));
    }

    private static void sort(int[] arr, Comparator<Integer> comparator) {
        PriorityQueue<Integer> min_queue = new PriorityQueue<>(comparator);
        for (int i: arr) {
            min_queue.offer(i);
        }
        int counter = 0;
        while (!min_queue.isEmpty()) {
            arr[counter] = min_queue.poll();
            counter += 1;
        }
    }


}
