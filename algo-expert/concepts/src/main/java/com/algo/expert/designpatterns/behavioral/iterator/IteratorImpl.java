package com.algo.expert.designpatterns.behavioral.iterator;

public class IteratorImpl {
    public static void main(String[] args) {
        EmployeeDatabase database = new EmployeeDatabase();
        EmployeeIterator itr =  database.employeeIterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
