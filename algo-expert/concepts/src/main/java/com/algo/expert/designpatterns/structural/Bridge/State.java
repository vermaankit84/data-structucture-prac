package com.algo.expert.designpatterns.structural.Bridge;

public interface State {
    void moveState();
    void hardPressed();
}
