package com.algo.expert.algos;

public class DogeAlgo {
    public static void main(String[] args) {
        int [] nums = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(maxProductSubArray(nums));
    }

    private static int maxProductSubArray(int[] nums) {
        int max_product = Integer.MIN_VALUE;
        int left = 0 , right = 0;
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            left = (left == 0 ? 1 : left) * nums[i];
            right = (right == 0 ? 1 : right) * nums[length - i - 1];
            max_product = Math.max(max_product, Math.max(left, right));
        }

        return max_product;
    }
}
