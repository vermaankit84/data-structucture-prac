package com.algo.expert.easy.facebook;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MagicalCandyBag {
    public static void main(String[] args) {
        MagicalCandyBag candyBag = new MagicalCandyBag();
        System.out.println(candyBag.maxCandies(new int[] {2, 1, 7, 4, 2}, 3));
    }

    int maxCandies(int[] arr, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());
        for (int i: arr) {
            queue.add(i);
        }
        int sum = 0;
        for (int i = 0; i < k; i++) {
            int eaten_candes = queue.peek()!=null ? queue.poll() : 0;
            sum = sum + eaten_candes;
            queue.add(eaten_candes/2);
        }

        return sum;
    }
}
