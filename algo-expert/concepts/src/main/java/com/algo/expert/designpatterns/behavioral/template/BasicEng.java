package com.algo.expert.designpatterns.behavioral.template;

public abstract class BasicEng {
    public final void completeCourse() {
        performBasicPapers();
        performAdditionalPapers();
    }

    public abstract void performAdditionalPapers();

    public  void performBasicPapers() {
        System.out.println("basic operation completed");
    }
}
