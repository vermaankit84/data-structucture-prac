package com.algo.expert.algos;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/*
             1
        2         3
    4      5    6     7

 */

public class DepthFirstSearch {
    public static void main(String[] args) {
        Tree root = new Tree(1);
        root.left = new Tree(2);
        root.left.left = new Tree(4);
        root.left.right = new Tree(5);
        root.right = new Tree(3);
        root.right.left = new Tree(6);
        root.right.right = new Tree(7);
        //System.out.println(depthFirstSearch(root));
        printLevel(root);
    }

    private static List<Integer> depthFirstSearch(Tree root) {
        if (root == null) return null;
        List<Integer> response =  new ArrayList<>();
        Stack<Tree> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Tree tree = stack.pop();
            if (tree.right!=null) {
                stack.push(tree.right);
            }
            if (tree.left!=null) {
                stack.push(tree.left);
            }
            response.add(tree.data);
        }
        return response;

    }


    static class Tree {
        int data;
        Tree left;
        Tree right;
        public Tree(int data) {
            this.data = data;
            this.left = this.right = null;
        }
     }


    static void printLevel(Tree root) {
        int h = height(root);
         for (int i = 1; i <= h; i++) {
             printCurrentLevel(root, i);
         }
     }

    private static void printCurrentLevel(Tree root, int level) {
        if (root == null) return;
        if (level == 1) {
            System.out.println("  " + root.data);
        } else {
            printCurrentLevel(root.left, level - 1);
            printCurrentLevel(root.right, level - 1);
        }
    }

    private static int height(Tree root) {
        if (root == null) return 0;
        else {
            int lHeight = height(root.left);
            int rHeight = height(root.right);

            return Math.max(lHeight, rHeight) + 1;
        }
    }
}
