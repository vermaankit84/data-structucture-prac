package com.algo.expert.classes.dynamic_programming;

import java.util.Comparator;
import java.util.PriorityQueue;

public class LargestNumberArray {
    public static void main(String[] args) {
        int [] nums = {3,30,34,5,9};
        System.out.println(largestNumber(nums));
    }

    public static String largestNumber(int[] nums) {
        if (nums == null ||  nums.length == 0) {
            return null;
        }

        if (nums.length == 1) {
            return String.valueOf(nums[0]);
        }

        PriorityQueue<String> queue = new PriorityQueue<>((s1, s2) ->
                Integer.compare(Integer.parseInt(s2.substring(0, 1)), Integer.parseInt(s1.substring(0, 1))));

        for (int num : nums) {
            queue.add(String.valueOf(num));
        }

        int counter = 0;
        while (!queue.isEmpty()) {
            nums[counter] = Integer.parseInt(queue.poll());
            counter = counter + 1;
        }

        String f_element = String.valueOf(nums[0]);
        String n_element = String.valueOf(nums[1]);

        String next_largest = largestNumber(f_element, n_element);

        for (int i = 2; i < nums.length; i++) {
            f_element = next_largest;
            n_element = String.valueOf(nums[i]);
            next_largest = largestNumber(f_element, n_element);
        }

        return next_largest;
    }

    // 2 , 10 102 210  2 1 12 21
    private static String largestNumber(String s1, String  s2) {
        if (Long.valueOf(s1.concat(s2)) > Long.valueOf(s2.concat(s1))) {
            return s1.concat(s2);
        } else {
            return s2.concat(s1);
        }
    }
}
