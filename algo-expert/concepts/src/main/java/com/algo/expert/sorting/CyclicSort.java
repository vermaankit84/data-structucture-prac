package com.algo.expert.sorting;

import java.util.Arrays;

public class CyclicSort {

    public static void main(String[] args) {
        int [] nums  = {1, 0, 3, 4, 5, 6, 7, 2};
        cyclicSort(nums);
        System.out.println(Arrays.toString(nums));
    }
    
    /*
      [0, 1, 2, 3, 4, 5, 6, 7]   
    */

    private static void cyclicSort(int[] nums) {
        int counter = 0;

        while(counter < nums.length) {
            int current_element = nums[counter];
            if (current_element!= counter) {
                int temp = nums[current_element];
                nums[current_element] = current_element;
                nums[counter] = temp;
            } else {
                counter += 1;
            }
        }
    }
}
