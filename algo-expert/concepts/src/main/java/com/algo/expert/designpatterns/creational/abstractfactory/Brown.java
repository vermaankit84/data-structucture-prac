package com.algo.expert.designpatterns.creational.abstractfactory;

public class Brown implements Color{
    @Override
    public String getColor() {
        return "Brown";
    }
}
