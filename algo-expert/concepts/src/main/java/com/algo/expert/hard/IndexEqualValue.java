package com.algo.expert.hard;

public class IndexEqualValue {
    public static void main(String[] args) {

    }

    public int indexEqualsValue(int[] array) {
        int start = 0;
        int end = array.length - 1;

        while (start <= end) {
            int mid = start + (end - start)/2;
            int value = array[mid];
            if (value < mid) {
                start = mid + 1;
            } else if ((mid == value) && (mid == 0)) {
                return mid;
            } else  if ((mid == value) && (array[mid - 1] < (mid - 1))) {
                return mid;
            } else  {
                end = mid - 1;
            }
        }
        return -1;
    }
}
