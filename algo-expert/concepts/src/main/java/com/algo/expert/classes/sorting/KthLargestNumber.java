package com.algo.expert.classes.sorting;

import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class KthLargestNumber {
    public static void main(String[] args) {
        System.out.println(kth_largest_in_an_array(Lists.newArrayList(5, 1, 10, 3, 2), 2));
        System.out.println(kth_largest_in_an_array(Lists.newArrayList(4, 1, 2, 2, 3), 4));
    }

    public static int kth_largest_in_an_array(List<Integer> numbers, int k) {
        Queue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());
        for (Integer number : numbers) {
            queue.offer(number);
        }
        int counter = 0;
        int max = 0;
        while (counter < k && !queue.isEmpty()) {
            max = queue.poll();
            counter = counter + 1;
        }

        return max;
    }
}
