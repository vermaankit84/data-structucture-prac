package com.algo.expert.classes.graphs;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConnectedBridges {
    public static void main(String[] args) {

        List<List<Integer>> connections = new ArrayList<>();
        connections.add(Lists.newArrayList(0,1));
        connections.add(Lists.newArrayList(0,2));
        connections.add(Lists.newArrayList(0,4));
        connections.add(Lists.newArrayList(1,2));
        connections.add(Lists.newArrayList(1,3));

        System.out.println(find_critical_connections(5, connections));
    }

    private static int timeStamp;
    public static List<List<Integer>> find_critical_connections( int number_of_servers, List<List<Integer>> connections) {
        Graph graph = new Graph(number_of_servers);
        for (List<Integer> connection : connections) {
            graph.addEdge(connection.get(0), connection.get(1));
        }

        List<List<Integer>> bridges = new ArrayList<>();
        boolean [] visited = new boolean[number_of_servers];
        Arrays.fill(visited, false);

        int [] discover  = new int[number_of_servers];
        int [] earliest  = new int[number_of_servers];



        dfs(0, -1, bridges, visited, discover, earliest, graph);
        if (bridges.isEmpty()) {
            List<Integer> empty = new ArrayList<>();
            empty.add(-1);
            empty.add(-1);
             bridges.add(empty);
             return bridges;
        }
        return bridges;
    }

    private static void dfs (int toVisit, int parent, List<List<Integer>> bridges, boolean [] visited, int [] discover , int[] earliest, Graph graph) {
        visited[toVisit] = true;
        discover[toVisit] = earliest[toVisit] = timeStamp++;
        List<Integer> neibhours[] = graph.adjList;
        for(int neibhour : neibhours[toVisit]) {
            if (neibhour == parent) {
                continue;
            }
            if (!visited[neibhour]) {
                dfs(neibhour, toVisit, bridges, visited, discover, earliest, graph);
                System.out.println(toVisit + " " + discover[toVisit] + " " + earliest[toVisit]);
                if (discover[toVisit] < earliest[toVisit]) {
                    List<Integer> bridge = new ArrayList<>();
                    bridge.add(toVisit, neibhour);
                    bridges.add(bridge);
                }
            }
            earliest[neibhour] = Math.min(earliest[toVisit], earliest[neibhour]);
        }
    }


    static class Graph {
        public int size ;
        public List<Integer> adjList[];

        public Graph(int size) {
            this.size = size;
            adjList = new ArrayList[size];
            for (int i = 0; i < size; i++) {
                adjList[i] = new ArrayList<>();
            }
        }

        public void addEdge(int start , int end) {
            adjList[start].add(end);
            adjList[end].add(start);
        }
    }
}
