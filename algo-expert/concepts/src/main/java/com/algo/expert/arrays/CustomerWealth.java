package com.algo.expert.arrays;

import java.util.Arrays;

public class CustomerWealth {
    public static void main(String[] args) {
        //System.out.println(maximumWealth)
    }

    public int maximumWealth(int[][] accounts) {
        int max = 0;
        for (int[] account : accounts) {
            max = Math.max(Arrays.stream(account).sum(), max);
        }

        return max;
    }
}
