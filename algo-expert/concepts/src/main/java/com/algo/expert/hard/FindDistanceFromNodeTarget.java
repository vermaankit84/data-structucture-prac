package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.List;

public class FindDistanceFromNodeTarget {
    public static void main(String[] args) {

    }


    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }

    public ArrayList<Integer> findNodesDistanceK(BinaryTree tree, int target, int k) {
        ArrayList<Integer> nodes = new ArrayList<>();
        findNodesDistanceTarget(tree, target, k, nodes);
        return nodes;
    }

    private int findNodesDistanceTarget(BinaryTree tree, int target, int k, List<Integer> nodes) {
        if (tree == null) return -1;

        if (tree.value == target) {
            addTreeNode(tree, 0, k, nodes);
            return 1;
        }

        int left = findNodesDistanceTarget(tree.left, target, k, nodes);
        int right = findNodesDistanceTarget(tree.right, target, k, nodes);

        if (left == k || right == k) {
            nodes.add(tree.value);
        }

        if (left != -1) {
            addTreeNode(tree.right, left + 1, k, nodes);
            return left + 1;
        }
        if (right != -1) {
            addTreeNode(tree.left, right + 1, k, nodes);
            return right + 1;
        }

        return -1;
    }

    private void addTreeNode(BinaryTree tree, int distance, int k, List<Integer> nodes) {
        if (tree == null) return;

        if (distance == k) {
            nodes.add(tree.value);
        } else {
            addTreeNode(tree.left, distance + 1, k, nodes);
            addTreeNode(tree.right, distance + 1, k, nodes);
        }
    }
}
