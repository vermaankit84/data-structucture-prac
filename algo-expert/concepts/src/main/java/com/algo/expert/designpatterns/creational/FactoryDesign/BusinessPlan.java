package com.algo.expert.designpatterns.creational.FactoryDesign;

public class BusinessPlan implements Plan{
    @Override
    public void createPlan() {
        System.out.println("In Business Plan");
    }
}
