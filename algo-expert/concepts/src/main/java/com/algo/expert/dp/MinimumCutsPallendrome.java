package com.algo.expert.dp;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MinimumCutsPallendrome {
    public static void main(String[] args) {
        String str = "abcdefghijklmracecaronpqrstuvwxyz";
        Map<Pair, Integer> memo = new HashMap<>();
        System.out.println("Min cuts needed for "
                + "Palindrome Partitioning is " + minPalPartion(str, 0, str.length() - 1, memo));
    }

    private static int minPalPartion(String str, int start, int end, Map<Pair, Integer> memo) {
        if (start > end || isPallendrome(str, start, end)) {
            return 0;
        }

        int ans = Integer.MAX_VALUE, count;

        for (int i = start; i < end; i++) {
            if (memo.get(new Pair(start, i))!=null && memo.get(new Pair(i + 1, end))!=null) {
                System.out.println("In memonization");
                count = memo.get(new Pair(start, i)) + memo.get(new Pair(i + 1, end)) ;
            } else {
                int r1 = minPalPartion(str, start, i, memo);
                memo.put(new Pair(start, i), r1);
                int r2 = minPalPartion(str, i + 1, end, memo) + 1;
                memo.put(new Pair(start, i), r2);
                count = r1 + r2;
            }
            ans = Math.min(count, ans);
        }
        return ans;
    }

    private static class  Pair {
        int start;
        int end;

        public Pair(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pair)) return false;
            Pair pair = (Pair) o;
            return start == pair.start && end == pair.end;
        }

        @Override
        public int hashCode() {
            return Objects.hash(start, end);
        }
    }

    private static boolean isPallendrome(String str, int start, int end) {

        while (start < end) {
            if (str.charAt(start) != str.charAt(end)) {
                return false;
            }
            start ++;
            end --;
        }

        return true;

    }
}
