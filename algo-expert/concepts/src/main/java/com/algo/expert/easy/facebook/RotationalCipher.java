package com.algo.expert.easy.facebook;

public class RotationalCipher {
    public static void main(String[] args) {
        RotationalCipher cipher = new RotationalCipher();
            System.out.println(cipher.rotationalCipher("Zebra-123", 3));
    }



    String rotationalCipher(String input, int rotationFactor) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
          char c = input.charAt(i);
          if (Character.isUpperCase(c)) {
              builder.append((char) (((int)c + rotationFactor - 65) % 26 + 65));
          } else if (Character.isLowerCase(c)) {
              builder.append((char) (((int)c + rotationFactor - 97) % 26 + 97 ));
          } else if (Character.isDigit(c)) {
              builder.append((Integer.valueOf(String.valueOf(c)) + rotationFactor) % 10 );
          } else {
              builder.append(c);
          }
        }



        return builder.toString();
    }
}
