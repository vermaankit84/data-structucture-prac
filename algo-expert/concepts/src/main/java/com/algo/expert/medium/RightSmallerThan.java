package com.algo.expert.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RightSmallerThan {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(8, 5, 11, -1, 3, 4, 2);
        System.out.println(rightSmallerThan(integers));

        System.out.println(Arrays.toString(leftGreaterThan(integers)));
        System.out.println(Arrays.toString(rightGreaterThan(integers)));
    }

    // using brute force
    public static List<Integer> rightSmallerThan(List<Integer> array) {
        List<Integer> response  = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            int count = 0;
            for (int j = i; j < array.size(); j++) {
                if (array.get(i) > array.get(j)) {
                    count = count + 1;
                }
            }
            response.add(count);
        }
        return response;
    }


    // using brute force
    public static int[] leftGreaterThan(List<Integer> array) {
        int[] response = new int[array.size()];
        response[0] = 0;
        for (int i = 1; i < array.size() - 1; i++) {
            response[i] = Math.max(array.get(i), response[i - 1]);
        }
        return response;
    }

    // using brute force
    public static int[] rightGreaterThan(List<Integer> array) {
        int[] response = new int[array.size()];
        response[array.size() - 1] = 0;
        for (int i = array.size() - 2; i > 0 ;i = i - 1) {
            response[i] = Math.max(array.get(i), response[i - 1]);
        }
        return response;
    }
}
