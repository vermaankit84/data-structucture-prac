package com.algo.expert.hard;

import java.util.*;

public class RiverSize {
    public static void main(String[] args) {

    }

    public static List<Integer> riverSizes(int[][] matrix) {
       List<Integer> sizes = new ArrayList<>();
       boolean[][] visited = new boolean[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (visited[i][j]) continue;
                traverseNode(i, j, matrix, visited, sizes);
            }
        }

        return sizes;
    }

    private static void traverseNode(int i, int j, int[][] matrix, boolean[][] visited, List<Integer> sizes) {
        int currentRiverSize = 0;
        Stack<Integer[]> nodeToExplore = new Stack<>();
        nodeToExplore.add(new Integer[]{i,j});
        while (!nodeToExplore.isEmpty()) {
            Integer[] currentNode = nodeToExplore.pop();
            i = currentNode[0];
            j = currentNode[1];
            if (visited[i][j]) continue;
            visited[i][j] = true;
            if (matrix[i][j] == 0) continue;

            currentRiverSize += 1;
            List<Integer[]> unvisitedNodes = getUnVisitedNodes(i, j, matrix, visited);
            nodeToExplore.addAll(unvisitedNodes);
        }

        if (currentRiverSize > 0) {
            sizes.add(currentRiverSize);
        }
    }

    private static List<Integer[]> getUnVisitedNodes(int i, int j, int[][] matrix, boolean[][] visited) {
        List<Integer[]> unvisitedNodes = new ArrayList<>();
        if (i > 0 && !visited[i - 1][j]) {
            unvisitedNodes.add(new Integer[] {i - 1, j});
        }

        if (j > 0 && !visited[i][j - 1]) {
            unvisitedNodes.add(new Integer[] {i, j - 1});
        }
        if (i < matrix.length - 1 && !visited[i + 1][j]) {
            unvisitedNodes.add(new Integer[] {i + 1, j });
        }
        if (j < matrix[0].length - 1 && !visited[i][j + 1]) {
            unvisitedNodes.add(new Integer[] {i , j + 1});
        }

        return unvisitedNodes;
    }
}
