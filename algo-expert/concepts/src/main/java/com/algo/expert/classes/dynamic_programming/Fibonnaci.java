package com.algo.expert.classes.dynamic_programming;

import java.util.Arrays;

public class Fibonnaci {
    public static void main(String[] args) {
        System.out.println(fibnaaci(4));
        System.out.println(fibnaaci(5));
        System.out.println(fibnaaci(23));
    }

    private static int fibnaaci(int number) {
        int [] dp = new int[number + 1];
        Arrays.fill(dp, 0);
        dp[1] = 1;
        for(int i = 2; i < number + 1; i = i + 1) {
            dp[i] = dp[i - 2] + dp[i - 1];
        }

        return dp[number];
    }
}
