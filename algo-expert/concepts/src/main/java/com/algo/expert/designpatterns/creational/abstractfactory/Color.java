package com.algo.expert.designpatterns.creational.abstractfactory;

public interface Color {
    String getColor();
}
