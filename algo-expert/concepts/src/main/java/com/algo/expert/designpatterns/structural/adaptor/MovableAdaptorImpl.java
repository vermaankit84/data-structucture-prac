package com.algo.expert.designpatterns.structural.adaptor;

public class MovableAdaptorImpl implements MovableAdaptor{
    private Movable movable;

    public MovableAdaptorImpl(Movable movable) {
        this.movable = movable;
    }

    @Override
    public double getSpeed() {
        return convert(movable.getSpeed());
    }

    private double convert(double speed) {
        return speed * 2;
    }
}
