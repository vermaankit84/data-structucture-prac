package com.algo.expert.designpatterns.behavioral.iterator;

import java.util.Iterator;
import java.util.List;

public class EmployeeIterator implements Iterator<Employee> {

    private final List<Employee> employees;

    private int position;

    public EmployeeIterator(List<Employee> employees) {
       this.employees = employees;
       this.position = 0;
    }

    @Override
    public boolean hasNext() {
        return position < this.employees.size();
    }

    @Override
    public Employee next() {
        Employee e = employees.get(position);
        position += 1;
        return e;
    }
}
