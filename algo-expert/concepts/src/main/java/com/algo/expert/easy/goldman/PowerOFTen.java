package com.algo.expert.easy.goldman;

public class PowerOFTen {
    public static void main(String[] args) {
        System.out.println(isPowerOfTen(10000));
        System.out.println(isPowerOfTen(1));
        System.out.println(isPowerOfTen(10));
        //System.out.println(isPowerOfTen(-1000));
        System.out.println(isPowerOfTen(11000));
    }

    private static boolean isPowerOfTen(int i) {
        if (i == 0) return false;
        if (i == 1) return true;

        return (int)Math.pow(10, findNoZeros(String.valueOf(i))) == i;
    }

    private static int findNoZeros(String str) {
        int noOfZeros = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '0') {
                noOfZeros += 1;
            }
        }
        return noOfZeros;
    }
}
