package com.algo.expert.algos;

public class KadeneAlgo {
    public static void main(String[] args) {
        int [] nums = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(maxSubArray(nums));
    }

    public static int maxSubArray(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }

        int max_sum = Integer.MIN_VALUE;
        int current_sum = nums[0];
        for (int i = 1; i < nums.length; i++) {
            current_sum = current_sum + nums[i];
            max_sum = Math.max(current_sum, max_sum);
            if (current_sum < 0) {
                current_sum = 0;
            }
        }
        return max_sum;
    }
}
