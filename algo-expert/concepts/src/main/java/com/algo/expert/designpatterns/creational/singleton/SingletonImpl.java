package com.algo.expert.designpatterns.creational.singleton;

public class SingletonImpl {
    private static SingletonImpl instance;

    private SingletonImpl() {}

    public static synchronized SingletonImpl getInstance() {
        if (instance == null) {
            instance = new SingletonImpl();
        }
        return instance;
    }
}
