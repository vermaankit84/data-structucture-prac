package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class DisntinctStrings {
    public static void main(String[] args) {
        String str = "xy";
        List<String> output = distinctStrings(str);
        String[] response = new String[output.size()];
        for (int i = 0; i < output.size(); i++) {
            response[i] = output.get(i);
        }

        System.out.println(Arrays.toString(response));
    }

    private static List<String> distinctStrings(String s) {
        List<String> response = new ArrayList<>();
        helper(s, 0, new Stack<String>(), response);
        return response;
    }

    private static void helper(String s, int idx, Stack<String> stack, List<String> response) {
        if (idx == s.length()) {
            response.add(String.join("", stack));
            return;
        }

        helper(s, idx + 1, stack, response);
        stack.push(String.valueOf(s.charAt(idx)));
        helper(s, idx + 1, stack, response);
        stack.pop();
    }
}
