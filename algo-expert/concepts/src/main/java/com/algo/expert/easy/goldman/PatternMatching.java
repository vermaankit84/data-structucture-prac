package com.algo.expert.easy.goldman;

public class PatternMatching {
    public static void main(String[] args) {
        String str = "ABCABCABC";


        System.out.println(findPattern(str));
    }

    private static String findPattern(String str) {
        String pattern = "";
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j < str.length(); j++) {
                String probPatterns = str.substring(i, j);
                if (isDefinatePattern(str,probPatterns)) {
                    return probPatterns;
                }
            }
        }
        return pattern;
    }

    private static boolean isDefinatePattern(String str, String pattern) {
        int length = pattern.length();
        try {
            for (int i = 0; i < str.length(); i = i + length) {
                String test = str.substring(i, i + length);
                if (!test.equals(pattern)) return false;
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
