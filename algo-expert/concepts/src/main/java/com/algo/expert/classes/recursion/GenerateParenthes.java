package com.algo.expert.classes.recursion;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
// o(4^n/root(n))
public class GenerateParenthes {
    public static void main(String[] args) {
        int n = 2;

        List<String> result = new ArrayList<>();
        helper(result, "", 0, 0, n);
        System.out.println(result);
    }

    public static void helper(List<String> result, String s, int open, int close, int n) {
        if (open == n && close == n) {
            result.add(s);
            return;
        }

        if (open < n) {
            helper(result, s + "(" , open + 1, close, n);
        }

        if (close < open) {
            helper(result, s + ')', open, close + 1, n);
        }
    }


}
