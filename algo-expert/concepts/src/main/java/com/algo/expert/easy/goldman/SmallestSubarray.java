package com.algo.expert.easy.goldman;

import java.util.Arrays;

public class SmallestSubarray {
    public static void main(String[] args) {
       int arr[] = {1, 4, 45, 6, 0, 19};
       System.out.println(smallestSubWithSum(arr, arr.length, 51));

        arr = new int[]{1, 10, 5, 2, 7};
        System.out.println(smallestSubWithSum(arr, arr.length, 9));

        arr = new int[]{1, 11, 100, 1, 0, 200, 3, 2, 1, 250};
        System.out.println(smallestSubWithSum(arr, arr.length, 280));

        arr = new int[]{1, 2, 4};
        System.out.println(smallestSubWithSum(arr, arr.length, 8));
    }


    private static int smallestSubWithSum(int arr[], int n, int x) {
        int arraySum = Arrays.stream(arr).sum();

        if (arraySum < x) {
            return  -1;
        }

        int temp[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
        }

        Arrays.sort(temp);
        int max = temp[temp.length - 1];
        if (max > x) {
            return 1;
        }

        int minLength = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
               int sum = subSet(i, j, arr);
                if (sum > x) {
                    minLength = Math.min(minLength, (j - i) + 1);
                    break;
                }
            }
        }
        return minLength == Integer.MAX_VALUE ? -1 : minLength;
    }

    private static int subSet(int i, int j, int[] arr) {
        int sum = 0;
        for (int k = i; k <=j ; k++) {
            sum = sum + arr[k];
        }
        return sum;
    }
}
