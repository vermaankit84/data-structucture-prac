package com.algo.expert.hard;

public class YoungestCommonAncestor {
    public static void main(String[] args) {

    }

    public static AncestralTree getYoungestCommonAncestor(
            AncestralTree topAncestor, AncestralTree descendantOne, AncestralTree descendantTwo) {
        int depthOne  = getDepth(descendantOne, topAncestor);
        int depthTwo  = getDepth(descendantTwo, topAncestor);

        if (depthOne > depthTwo) {
            return backTrack(descendantOne, descendantTwo, depthOne - depthTwo);
        } else  {
            return backTrack(descendantTwo, descendantOne, depthTwo - depthOne);
        }
    }

    private static AncestralTree backTrack(AncestralTree lower, AncestralTree upper, int i) {

        while (i > 0) {
            lower = lower.ancestor;
            i -= 1;
        }

        while (lower!=upper) {
            lower = lower.ancestor;
            upper = upper.ancestor;
        }

        return lower;
    }

    private static int getDepth(AncestralTree start, AncestralTree end) {
        int depth = 0;
        while (start!=end) {
            depth = depth - 1;
            start = start.ancestor;
        }
        return depth;
    }




    static class AncestralTree {
        public char name;
        public AncestralTree ancestor;

        AncestralTree(char name) {
            this.name = name;
            this.ancestor = null;
        }

        // This method is for testing only.
        void addAsAncestor(AncestralTree[] descendants) {
            for (AncestralTree descendant : descendants) {
                descendant.ancestor = this;
            }
        }
    }
}
