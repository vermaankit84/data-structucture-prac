package com.algo.expert.classes.graphs;

import java.util.*;

public class JumpGame {
    public static void main(String[] args) {
       int [] nums = {0};
        JumpGame jumpGame = new JumpGame();
        System.out.println(jumpGame.canJump(nums));


    }

    public boolean canJump(int[] nums) {
        Graph graph = new Graph(nums.length);
        for (int i = 0; i < nums.length; i++) {
            int loop_count = nums[i];
            int j = i + 1;
            while (loop_count > 0) {
                graph.add(i, j);
                j = j + 1;
                loop_count = loop_count - 1;
            }
        }

        int start_node = 0;
        int end_node = nums.length - 1;

        boolean visited[] = new boolean[nums.length];
        Arrays.fill(visited, false);

        Queue<Integer> queue = new LinkedList<>();
        queue.add(start_node);
        List<Integer>[] adList = graph.adjustList;
        visited[start_node] = true;
        while (!queue.isEmpty()) {
            int current_node = queue.poll();
            for (int child_node : adList[current_node]) {
                if (child_node < nums.length) {
                    if (!visited[child_node]) {
                        visited[child_node] = true;
                        if ((child_node == end_node)) {
                            return true;
                        }

                        queue.add(child_node);
                    }
                }
            }
        }


        return false;
    }

    static class Graph {
        int size ;
        public List<Integer> adjustList[];

        public Graph(int size) {
            this.size = size;
            adjustList = new ArrayList[size];
            for (int i = 0; i < size; i++) {
                adjustList[i] = new ArrayList<>();
            }
        }

        public void add(int start, int end) {
            adjustList[start].add(end);
        }
    }
}
