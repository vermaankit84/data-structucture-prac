package com.algo.expert.medium;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class KthLargestElement {
    public static void main(String[] args) {
        KthLargestElement largestElement = new KthLargestElement();
        System.out.println(largestElement.findKthLargest(new int[]{3, 2, 1, 5, 6, 4}, 2));
    }

    public int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());
        Arrays.stream(nums).boxed().forEach(queue::add);
        int counter = 0;
        int number = -1;
        while (counter < k) {
            if (queue.isEmpty()) {
                return -1;
            }
            number = queue.poll();
            counter = counter + 1;
        }

        return number;
    }
}
