package com.algo.expert.dp;

import java.util.Arrays;

public class AritmaticSum {
    public static void main(String[] args) {
       int [] nums = {1,2,3,4};
       System.out.println(numberOfArithmeticSlices(nums));
    }

    public static int numberOfArithmeticSlices(int[] nums) { 
        int dp[] = new int[nums.length];
        for (int i = 2; i < dp.length; i++) {
            if ((nums[i] - nums[i - 1]) == (nums[i - 1] - nums[i - 2])) {
                dp[i] = dp[i - 1] + 1;
            }
        }

        return Arrays.stream(dp).sum();
    }
}
