package com.algo.expert.medium;

public class LevenshteinDistance {

    public static void main(String[] args) {
        System.out.println(levenshteinDistance("abc", "yabcx"));
    }

    /*
         0 1 2
       0 0 1 2
       1 1
       2 2
       3 3


     */
    public static int levenshteinDistance(String str1, String str2) {
        int edits[][] = new int[str1.length() + 1][str2.length() + 1];
        for (int i = 0; i <= str1.length(); i++) {
            for (int j = 0; j <= str2.length(); j++) {
                if (i == 0) {
                    edits[i][j] = j;
                } else if (j == 0) {
                    edits[i][j] = i;
                } else if (str1.charAt(i -  1) == str2.charAt(j - 1)) {
                    edits[i][j] = edits[i - 1][j - 1];
                } else {
                    edits[i][j] = Math.min(
                            edits[i][j- 1],
                            Math.min(edits[i - 1][j], edits[i - 1][j - 1])
                    ) + 1;
                }
            }
        }



        return edits[str1.length()][str2.length()];
    }
}
