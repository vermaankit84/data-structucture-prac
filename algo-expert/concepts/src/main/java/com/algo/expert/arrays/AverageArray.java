package com.algo.expert.arrays;

import java.util.Arrays;

public class AverageArray {
    public static void main(String[] args) {
        int [] arr = new int [] { 4, 3, 3, 3 };
        int N = arr.length;


        int output[] = findOriginal(arr, N);

        System.out.println(Arrays.toString(output));
    }

    private static int[] findOriginal(int[] arr, int n) {
        int[] output = new int[n];
        output[0] = arr[0];
        int sum  = output[0];
        for (int i = 1; i < n; i++) {
            output[i] = arr[i] * (i + 1) - sum;
            sum = sum + output[i];
        }
        return output;
    }


}
