package com.algo.expert.classes.recursion;
import com.google.common.collect.Lists;

import java.util.*;
import java.util.stream.Collectors;

public class PermuteWhenDuplicates {
    public static void main(String[] args) {
        List<Integer> list = Lists.newArrayList(1, 2);
        System.out.println(get_permutations(new ArrayList<>()));
    }

    private static List<List<Integer>> get_permutations(List<Integer> arr) {
        if (arr == null || arr.isEmpty()) {
            return new ArrayList<>();
        }

        Set<String> response = new HashSet<>();
        helper(arr, 0, new Stack<>(), response);
        List<List<Integer>> output = new ArrayList<>();
        for (String s : response) {
            String[] temp = s.split(",");
            List<Integer> tempList = new ArrayList<>();
            for (String value : temp) {
                tempList.add(Integer.valueOf(value));
            }
            output.add(tempList);
        }
        return output;
    }

    private static void helper(List<Integer> arr, int idx, List<Integer> slate, Set<String> response) {
        if (arr.size() == idx) {
            response.add(slate.stream().map(String::valueOf).collect(Collectors.joining(",")));
            return;
        }

        for (int i = idx; i < arr.size(); i++) {
            Collections.swap(arr, idx, i);
            slate.add(arr.get(idx));
            helper(arr, idx + 1, slate, response);
            slate.remove(slate.size() - 1);
            Collections.swap(arr, idx, i);
        }
    }
}
