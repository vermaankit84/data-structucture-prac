package com.algo.expert.classes.recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeneratePossibleExpression {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(generate_all_expressions("222", 24)));
        System.out.println(Arrays.toString(generate_all_expressions("1234", 11)));
    }

    public  static String[] generate_all_expressions(String s, long target) {
        List<String> response = new ArrayList<>();
        char[] slate  = new char[s.length() * 2];
        char[] digit = s.toCharArray();
        int n = 0;
        for (int i = 0; i < digit.length; i++) {
            n = n * 10 + digit[i] - '0';
            slate[i] = digit[i];
            helper(slate, digit, target, i + 1, i + 1, 0, n, response);
        }


        return response.toArray(new String[0]);
    }

    private static void helper(char[] slate, char[] digit, long target, int index, int length, long prev, long current,  List<String> response) {
        if (digit.length == index) {
            if (prev + current == target) {
                response.add(new String(slate, 0, length));
            }
            return;
        }

        long n = 0;
        int j = length + 1;
        for (int i = index; i < digit.length; i++) {
            n = n * 10 + digit[i] - '0';
            slate[j++] = digit[i];
            slate[length] = '*';
            helper(slate, digit, target, i + 1, j, prev, current * n, response);
            slate[length] = '+';
            helper(slate, digit, target, i + 1, j, prev+current , n, response);
        }
    }

}
