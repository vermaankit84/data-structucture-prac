package com.algo.expert.arrays;

import java.util.Arrays;

public class MergeSortedArray {
    public static void main(String[] args) {
       int[] nums1 = {1};
       int m = 1;
       int []nums2 = {2,5,6};
       int n = 3;
       merge(nums1, m, nums2, n);
       System.out.println(Arrays.toString(nums1));
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
       if (nums1.length < nums2.length) {
           merge(nums2, n , nums1, m);
       }

       int start = 0;
       int end = nums1.length;
       int counter = 0;

       while (start < end) {
            int e1 = nums1[start];
            if (counter < n) {
                int e2 = nums2[counter];

                if (e1 < e2) {
                    start = start + 1;
                } else  {
                    if (e1 > e2) {
                        nums1[start] = e2;
                        nums2[counter] = e1;
                        counter = counter + 1;
                    } else {
                        start = start + 1;
                    }
                }
            } else {
                break;
            }
       }

        for (int i = 0; i < nums2.length; i++) {
            nums1[i + m] = nums2[i];
        }
    }
}
