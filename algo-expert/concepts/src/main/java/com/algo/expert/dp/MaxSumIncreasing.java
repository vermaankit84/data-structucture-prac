package com.algo.expert.dp;


import java.util.*;

public class MaxSumIncreasing {
    public static void main(String[] args) {
        int arr[] = {10, 70, 20, 30, 50, 11, 30};

        System.out.println(maxSumIncreasingSubsequence(arr));
    }


    public static List<List<Integer>> maxSumIncreasingSubsequence(int[] array) {

        int [] sequences = new int[array.length];
        Arrays.fill(sequences, Integer.MIN_VALUE);
        int [] sums = array.clone();
        int max_idx = 0;
        for (int i = 0; i < array.length; i++) {
            int current = array[i];
            for (int j = 0; j < i; j++) {
                int otherNum = array[j];
                if ((otherNum < current) && (sums[j] + current >= sums[i])) {
                    sums[i] = sums[j] + current;
                    sequences[i] = j;
                }
            }

            if (sums[i] >= sums[max_idx]) {
                max_idx = i;
            }
        }
        System.out.println(max_idx + " sequences " + Arrays.toString(sequences) + " " + Arrays.toString(sums)  );

        return buildSequnce(array, sequences, max_idx, sums[max_idx]);

    }

    private static List<List<Integer>> buildSequnce(int[] array, int[] sequences, int max_idx, int sums) {
        List<List<Integer>> sequence = new ArrayList<>();
        sequence.add(new ArrayList<>());
        sequence.add(new ArrayList<>());
        sequence.get(0).add(sums);
        while (max_idx!= Integer.MIN_VALUE) {
            sequence.get(1).add(0, array[max_idx]);
            max_idx = sequences[max_idx];
        }
        return sequence;
    }
}
