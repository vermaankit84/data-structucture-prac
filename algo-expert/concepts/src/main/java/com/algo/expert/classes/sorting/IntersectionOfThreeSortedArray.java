package com.algo.expert.classes.sorting;

import java.util.ArrayList;
import java.util.List;

public class IntersectionOfThreeSortedArray {
    public static void main(String[] args) {
        //System.out.println(intersectionOfThreeSortedArray(new int[]{1, 2, 3, 4, 5}, new int[]{5, 7, 8, 9}, new int[]{2, 4, 5, 5}));
    }

    public static List<Integer> intersectionOfThreeSortedArray(List<Integer> arr1, List<Integer> arr2, List<Integer> arr3) {
        int i = 0;
        int j = 0;
        int k =0;
        List<Integer> result = new ArrayList<>();
        while (i < arr1.size() && j < arr2.size() && k < arr3.size()) {
            if ((arr1.get(i) == arr2.get(j)) && (arr2.get(j) == arr3.get(k))) {
                result.add(arr1.get(i));
                i ++;
                j ++;
                k ++;
            } else {
                int minimum = Math.min(arr1.get(i), Math.min(arr2.get(j), arr3.get(k)));
                if (minimum == arr1.get(i)) {
                    i += 1;
                }

                if (minimum == arr2.get(j)) {
                    j = j + 1;
                }

                if (minimum == arr3.get(k)) {
                    k = k +1;
                }
            }
        }

        return result;
    }
}
