package com.algo.expert.designpatterns.creational.abstractfactory;

public class ColorFactory implements AbstractFactory<Color>{
    @Override
    public Color create(String type) {
        if ("white".equalsIgnoreCase(type)) {
            return new White();
        } else if ("brown".equalsIgnoreCase(type)) {
            return new Brown();
        }
        return null;
    }
}
