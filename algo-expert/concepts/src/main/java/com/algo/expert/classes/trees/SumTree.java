package com.algo.expert.classes.trees;

import java.util.ArrayList;
import java.util.List;

public class SumTree {
    public static void main(String[] args) {
        TreeNode root = TreeUtils.buildTree();
        int target = 8;
        System.out.println(isSumExits(root, target));
    }

    private static List<List<Integer>> isSumExits(TreeNode root, int target) {
        List<List<Integer>> response = new ArrayList<>();
        postOrder(root, response, new ArrayList<>(), target);
        return response;
    }

    private static void postOrder(TreeNode root, List<List<Integer>> response, List<Integer> slate, int target) {
        if (root == null) {
            return;
        }
        slate.add(root.getValue());
        postOrder(root.left, response, slate, target - root.value);
        postOrder(root.right, response, slate, target- root.value);
        if ((root.getRight() == null) && (root.getLeft() == null)) {
            if (target == root.value) {
                response.add(new ArrayList<>(slate));
            }
        }
        slate.remove(slate.size() - 1);
    }
}
