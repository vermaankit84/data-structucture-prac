package com.algo.expert.designpatterns.creational.FactoryDesign;

import java.util.Objects;

public class FactoryDesignPlanTester {
    public static void main(String[] args) {
        Objects.requireNonNull(PlanFactory.getPlan("domestic")).createPlan();
        Objects.requireNonNull(PlanFactory.getPlan("personal")).createPlan();
        Objects.requireNonNull(PlanFactory.getPlan("business")).createPlan();
    }
}
