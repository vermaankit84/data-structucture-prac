package com.algo.expert.designpatterns.structural.decorator;

public class FloorComponent extends AbstractDecorator {
    public  void makeHouse() {
        super.makeHouse();
        addFloor();
    }

    private void addFloor() {
        System.out.println("adding floor");
    }
}
