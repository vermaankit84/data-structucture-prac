package com.algo.expert.designpatterns.creational.prototype;

public class Circle extends Shape{
    public Circle() {
        this.setType("circle");
    }
    @Override
    public void draw() {
        System.out.println("draw circle");
    }
}
