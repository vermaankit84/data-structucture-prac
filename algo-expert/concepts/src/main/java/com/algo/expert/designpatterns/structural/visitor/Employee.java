package com.algo.expert.designpatterns.structural.visitor;

public interface Employee {
    void printStructures();
    void acceptVisitor(Visitor visitor);
}
