package com.algo.expert.easy.facebook;

import java.util.*;

public class LargestTripleProduct {
    public static void main(String[] args) {
        LargestTripleProduct product = new LargestTripleProduct();
        System.out.println(product.findMaxProduct(new int[] {1, 2, 3, 4, 5}));
    }

    int[] findMaxProduct(int[] arr) {
        int[] response = new int[arr.length];
        SortedSet<Integer> queue = new TreeSet<>(Comparator.reverseOrder());
        for(int i = 0; i < arr.length; i = i + 1) {
            int multi = 1;
            if (i == 2) {
                queue.add(arr[i]);
                for (Integer integer : queue) {
                    multi = multi * integer;
                }
            } else if (i > 2) {
                queue.add(arr[i]);
                queue.remove(queue.last());

                for (Integer integer : queue) {
                    multi = multi * integer;
                }
            } else  {
                queue.add(arr[i]);
                multi = -1;
            }
            response[i] = multi;
        }

       return response;

    }
}
