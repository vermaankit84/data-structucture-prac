package com.algo.expert.classes.dynamic_programming;


public class Combinations {
    public static void main(String[] args) {
        System.out.println(calculate_combination(5, 2));
       // System.out.println(calculate_combination(0, 0));
    }

    public static int calculate_combination(int n, int k) {


        int [][] dp = new int[n + 1][k + 1];



        for (int row = 0; row < n + 1; row++) {
            for (int col = 0; col <= Math.min(row, k); col++) {
                if (col == 0 || row == col) {
                    dp[row][col] = 1;
                } else {
                    dp[row][col] = dp[row - 1][col] + dp[row - 1][col - 1];
                }
            }
        }

        return dp[n][k];
    }
}
