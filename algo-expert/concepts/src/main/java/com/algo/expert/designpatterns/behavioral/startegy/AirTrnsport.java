package com.algo.expert.designpatterns.behavioral.startegy;

public class AirTrnsport implements TransportMedium {
    @Override
    public void transport() {
        System.out.println("implementing air Transport");
    }
}
