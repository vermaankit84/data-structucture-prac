package com.algo.expert.designpatterns.structural.decorator;

public class ConcreteHouse extends Component {
    @Override
    public void makeHouse() {
        System.out.println("in make house");
    }
}
