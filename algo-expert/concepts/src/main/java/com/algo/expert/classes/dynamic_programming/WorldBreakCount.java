package com.algo.expert.classes.dynamic_programming;

import com.google.common.collect.Lists;

import java.util.*;

public class WorldBreakCount {
    public static void main(String[] args) {
        List<String> dict = Lists.newArrayList("kick", "start", "kickstart", "is", "awe", "some", "awesome");
        String txt = "kickstartisawesome";

        System.out.println(wordBreakCount(dict, txt));
    }

    public static int wordBreakCount(List<String> dictionary, String txt) {
       int length = txt.length();
       int dp[] = new int[length + 1];
        dp[0] = 1;
       Set<String> dict = new HashSet<>();
        int dictMax = 0;

        for (String s : dictionary) {
            dict.add(s);
            dictMax = Math.max(dictMax, s.length());
        }

        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j <= length && (j - i) <= dictMax; j++) {
                String temp = txt.substring(i, j);
                if (dict.contains(temp)) {
                    dp[j] = dp[j] + dp[i];
                    dp[i] = dp[i]% 1000000007;
                }
            }
        }


       return dp[length];
    }
}
