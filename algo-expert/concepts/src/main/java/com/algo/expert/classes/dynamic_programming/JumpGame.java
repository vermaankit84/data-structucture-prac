package com.algo.expert.classes.dynamic_programming;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JumpGame {
    public static void main(String[] args) {
        ArrayList<Integer> maximum_jump_lengths = Lists.newArrayList(2, 3, 1, 0, 0, 7);
        System.out.println(can_reach_last_house(maximum_jump_lengths));
    }

    static Boolean can_reach_last_house(ArrayList<Integer> maximum_jump_lengths) {
         int lastGoodIndex = maximum_jump_lengths.size() -1;
         for(int i= maximum_jump_lengths.size() - 1; i > -1; i = i - 1) {
             if (i + maximum_jump_lengths.get(i) >= lastGoodIndex) {
                 lastGoodIndex = i;
             }
         }

         return lastGoodIndex == 0;
    }

}
