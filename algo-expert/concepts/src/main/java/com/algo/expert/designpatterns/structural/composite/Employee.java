package com.algo.expert.designpatterns.structural.composite;

public interface Employee {
   void printStructures();
   int getEmployeeCount();
}
