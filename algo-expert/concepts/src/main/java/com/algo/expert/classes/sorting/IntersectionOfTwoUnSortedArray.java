package com.algo.expert.classes.sorting;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IntersectionOfTwoUnSortedArray {

    public static void main(String[] args) {
        int arr1[] = {1, 2, 2, 1};
        int arr2[] = {5, 1, 2};

        System.out.println(intersectionOfTwoUnSortedArray(arr1, arr2));
    }

    public static List<Integer> intersectionOfTwoUnSortedArray(int arr1[], int arr2[]) {
        Set<Integer> set = new HashSet<>();
        List<Integer> response = new ArrayList<>();
        for (int num: arr1) {
            set.add(num);
        }

        for (int num: arr2) {
            if (!set.add(num)) {
                response.add(num);
            }
        }

        return response;
    }
}
