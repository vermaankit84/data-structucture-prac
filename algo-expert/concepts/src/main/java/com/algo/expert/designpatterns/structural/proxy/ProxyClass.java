package com.algo.expert.designpatterns.structural.proxy;

import com.google.common.collect.Lists;

import java.util.List;

public class ProxyClass extends Subject {
    private static List<String> userList = Lists.newArrayList("Ankit", "Admin");
    private final String user;
    public ProxyClass(String user) {
        this.user = user;
    }

    private static Subject subject;
    @Override
    public void doWork() {
        if (userList.contains(this.user)) {
            if (subject == null) {
                subject = new ConcreteClass();
            }

            subject.doWork();
        } else {
            System.out.println("user is not allowed to access this");
        }

    }
}
