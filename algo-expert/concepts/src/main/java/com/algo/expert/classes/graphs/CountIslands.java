package com.algo.expert.classes.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CountIslands {
    public static void main(String[] args) {
        int [][] grid = {
                { 1, 1, 0 },
                { 1, 1, 0},
                {0, 0, 1}
        };

        List<List<Integer>> matrix = new ArrayList<>();
        for (int[] ints : grid) {
            List<Integer> temp = new ArrayList<>();
            for (int anInt : ints) {
                temp.add(anInt);
            }
            matrix.add(temp);
        }

        System.out.println(count_islands(matrix));
    }

    public static int count_islands(List<List<Integer>> matrix) {
        int max = 0;
        for (int i = 0; i < matrix.size(); i++) {
            for (int j = 0; j < matrix.get(0).size(); j++) {
                if (matrix.get(i).get(j) == 1) {
                    AtomicInteger size = new AtomicInteger();
                    max = max + 1;
                    helper(matrix, i , j, size);
                    max = Math.max(max, size.get());
                }
            }
        }
        return max;
    }

    private static void helper(List<List<Integer>> matrix, int row, int column, AtomicInteger size) {
        if (row > -1 && row < matrix.size() && column > -1 && column < matrix.get(0).size()) {
            if (matrix.get(row).get(column) == 1) {
                matrix.get(row).set(column, 2);
                size.incrementAndGet();
                helper(matrix, row + 1, column, size);
                helper(matrix, row - 1, column, size);
                helper(matrix, row, column + 1, size);
                helper(matrix, row, column - 1, size);

                helper(matrix, row + 1, column + 1, size);
                helper(matrix, row - 1, column + 1, size);
                helper(matrix, row + 1, column - 1, size);
                helper(matrix, row - 1, column - 1, size);
            }
        }
    }
}
