package com.algo.expert.hard;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class ShortenPath {
    public static void main(String[] args) {
        System.out.println(shortenPath("/foo/../test/../test/../foo//bar/./baz"));
    }

    // sanitaze the path
    public static String shortenPath(String path) {

        List<String> dataList = Arrays.stream(path.split("/")).filter(d -> !d.trim().isEmpty()).
                filter(n -> !n.trim().equalsIgnoreCase(".")).
                collect(Collectors.toList());
        Stack<String> stack = new Stack<>();
        if (path.charAt(0) == '/') stack.add("");
        dataList.forEach(token -> {
            if (token.trim().equalsIgnoreCase("..")) {
                if (stack.size() == 0 || stack.get(stack.size() - 1).equalsIgnoreCase("..")) {
                    stack.add(token);
                } else if (!stack.get(stack.size() - 1).equalsIgnoreCase("")) {
                    stack.remove(stack.size() - 1);
                }
            } else {
                stack.add(token);
            }
        });

        if (stack.size() == 1 && stack.get(0).equals("")) return "/";
        return String.join("/", stack);
    }
}
