package com.algo.expert.classes.trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConstructBinaryTree {
    static int current_idx = 0;
    static BinaryTreeNode construct_binary_tree(ArrayList<Integer> inorder, ArrayList<Integer> preorder) {
        Map<Integer, Integer> position_map = new HashMap<>();

        for (int i = 0 ; i < inorder.size(); i = i + 1) {
            position_map.put(inorder.get(i), i);
        }


        return helper(preorder, 0, inorder.size() - 1, position_map);
    }

    private static BinaryTreeNode helper(ArrayList<Integer> preorder, int start, int end, Map<Integer, Integer> position_map) {
        if (start > end) {
            return null;
        }

        int root_val = preorder.get(current_idx++);
        int root_idx = position_map.get(root_val);
        if (root_idx < start || root_idx > end) {
            return null;
        }

        BinaryTreeNode root = new BinaryTreeNode(root_val);
        root.left = helper(preorder, start, root_idx - 1, position_map);
        root.right = helper(preorder, root_idx + 1, end, position_map);
        return root;
    }

    static class BinaryTreeNode {
        Integer value;
        BinaryTreeNode left;
        BinaryTreeNode right;

        BinaryTreeNode(Integer value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }
}
