package com.algo.expert.hardest;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LargestAreaUnderSkyLine {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 3, 3, 2, 4, 1, 5, 3, 2);
        System.out.println(largestRectangleUnderSkyline(list));
    }

    public static int largestRectangleUnderSkyline(List<Integer> buildings) {
        int maxArea = Integer.MIN_VALUE;

        for (int i = 0; i < buildings.size(); i++) {
            int height = buildings.get(i);
            int next = height;
            for (int j = i; j < buildings.size(); j++) {
                int nextHeight = buildings.get(j);
                if (nextHeight == 0) break;
                int min = Math.min(height, nextHeight);
                // 2  3
                if (min < next ) {
                    next = min;
                }
                int area =  next * (j - i + 1);
                maxArea = Math.max(area, maxArea);
            }
        }

        return maxArea;
    }
}
