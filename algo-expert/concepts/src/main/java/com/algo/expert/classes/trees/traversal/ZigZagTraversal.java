package com.algo.expert.classes.trees.traversal;

import com.algo.expert.classes.trees.TreeNode;
import com.algo.expert.classes.trees.TreeUtils;

import java.util.*;

public class ZigZagTraversal {
    public static void main(String[] args) {
        List<BinaryLevel>  binaryLevels = zigZagTraversal(TreeUtils.buildTree());
        Map<Integer, List<Integer>> levelMap = new LinkedHashMap<>();
        for (BinaryLevel levelInfo: binaryLevels) {
            int level = levelInfo.level;
            List<Integer> levelList = levelMap.getOrDefault(level, new ArrayList<>());
            levelList.add(levelInfo.getData());
            levelMap.put(level, levelList);
        }

        for (int key : levelMap.keySet()) {
            List<Integer> values = levelMap.get(key);
            StringBuilder builder = new StringBuilder();
            values.forEach(v -> builder.append(v).append(" "));
            if (key % 2 == 0) {
                System.out.println(builder.toString().trim());
            } else {
                System.out.println(builder.reverse().toString().trim());
            }
        }
    }

    public static List<BinaryLevel> zigZagTraversal(TreeNode root) {
        List<BinaryLevel> response = new ArrayList<>();
        Queue<BinaryLevel> queue = new LinkedList<>();
        queue.add(new BinaryLevel(root, 0, root.getValue()));

        while (!queue.isEmpty()) {
            BinaryLevel temp = queue.poll();
            int currentlevel = temp.level;
            response.add(temp);
            if (temp.getNode().getLeft()!=null) {
                TreeNode leftNode =  temp.getNode().getLeft();
                queue.add(new BinaryLevel(leftNode, currentlevel + 1, leftNode.getValue()));
            }

            if (temp.getNode().getRight()!=null) {
                TreeNode rightnode =  temp.getNode().getRight();
                queue.add(new BinaryLevel(rightnode, currentlevel + 1, rightnode.getValue()));
            }
        }
        return response;
    }


}
