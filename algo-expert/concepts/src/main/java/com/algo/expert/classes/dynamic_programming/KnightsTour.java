package com.algo.expert.classes.dynamic_programming;

import java.util.*;

public class KnightsTour {
    public static void main(String[] args) {
        System.out.println(numPhoneNumbers(1, 2));
        System.out.println(numPhoneNumbers(1, 3));
    }

    private static long numPhoneNumbers(int startdigit, int phonenumberlength) {
        List<Integer>[] neighbors = (List<Integer>[])new List[10];

        neighbors[0] = Arrays.asList(4,6);
        neighbors[1] = Arrays.asList(6,8);
        neighbors[2] = Arrays.asList(7,9);
        neighbors[3] = Arrays.asList(4,8);
        neighbors[4] = Arrays.asList(3,9,0);
        neighbors[5] = new ArrayList<>();
        neighbors[6] = Arrays.asList(1,7,0);
        neighbors[7] = Arrays.asList(2,6);
        neighbors[8] = Arrays.asList(1,3);
        neighbors[9] = Arrays.asList(4,2);


        int dp[][] = new int[phonenumberlength][10];

        for (int i = 0; i < 10; i = i + 1) {
            dp[0][i] = 1;
        }

        for (int i = 1; i < phonenumberlength; i++) {
            for (int j = 0; j < 10; j++) {
                for (int neibhour : neighbors[j]) {
                    dp[i][j] = dp[i][j] + dp[i - 1][neibhour];
                }
            }
        }

        return dp[phonenumberlength - 1][startdigit];
    }
}
