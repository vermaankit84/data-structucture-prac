package com.algo.expert.sorting;

import java.util.Arrays;

public class HeapSortWithHeapify {
    public static void main(String[] args) {
        int arr[] = {4, 5, 1, 3};
        sort(arr);
        //System.out.println(Arrays.toString(arr));
    }

    private static void sort(int[] arr) {
        int[] heap = new int[arr.length];
        buildHeap(arr, heap);
        System.out.println(Arrays.toString(heap));

        for (int i = 0; i < heap.length; i++) {
            int element = deleteElement(heap, i);
            System.out.print(element + " " +  Arrays.toString(heap));
            arr[i] = element;
        }
    }

    private static int deleteElement(int[] heap, int id) {
        int delete = heap[0];
        heap[0] = heap[heap.length - 1 - id];
        heap[heap.length - 1 - id] = delete;

        int curr_left_idx = 1;
        int curr_right_idx = 2;

        int curr_id =0;
        int parent = heap[curr_id];
        int l_child = heap[curr_left_idx];
        int r_child = heap[curr_right_idx];

        while (parent > l_child || parent > r_child) {
            if (l_child < r_child) {
                heap[0] = l_child;
                heap[curr_left_idx] = parent;
                curr_id = curr_left_idx;
            } else {
                heap[0]  = r_child;
                heap[curr_right_idx] = parent;
                curr_id = curr_right_idx;
            }

            curr_left_idx = curr_left_idx * 2 + 1;
            curr_right_idx = curr_right_idx * 2;
            if (curr_left_idx < heap.length - id - 1 && curr_right_idx < heap.length - id - 1) {
                parent = heap[curr_id];
                l_child = heap[curr_left_idx];
                r_child = heap[curr_right_idx];
            } else {
                break;
            }
        }
        return delete;
    }

    private static void buildHeap(int[] arr, int[] heap) {
        for(int idx = 0; idx < arr.length; idx += 1) {
            insertHeap(arr[idx], heap, idx);
        }
    }

    private static void insertHeap(int element, int[] heap, int id) {
        heap[id] = element;
        if (id != 0) {
            int p_id = id/2;
            int parent = heap[p_id];
            int curr_id = id;
            while (element < parent) {
                // swap the elements
                int temp = parent;
                heap[p_id] = heap[curr_id];
                heap[curr_id] = temp;
                curr_id = curr_id/2;
                p_id = p_id/2;

                parent = heap[p_id];
            }
        }
    }
}
