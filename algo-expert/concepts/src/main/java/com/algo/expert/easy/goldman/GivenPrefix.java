package com.algo.expert.easy.goldman;

public class GivenPrefix {
    public static void main(String[] args) {
        String input = "a aa aaa abca bca";
        String prefix = "xbc";

        System.out.println(findIndex(input, prefix));
    }

    private static int findIndex(String input, String prefix) {
        String[] i = input.split(" ");
        int index = 0;
        for (String s : i) {
            index = index + 1 + s.length();
            if (s.startsWith(prefix)) {
                return index - 1 - s.length() ;
            }
        }
        return (index - 1) == input.length() ? - 1 : index;
    }
}
