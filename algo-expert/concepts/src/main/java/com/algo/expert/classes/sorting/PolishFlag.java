package com.algo.expert.classes.sorting;

import java.util.Arrays;

public class PolishFlag {
    public static void main(String[] args) {
        char [] c = {'G', 'G' , 'R', 'G', 'R'};

        int i = 0;
        int j = -1;

        while (i < c.length) {
            if (c[i] == 'R') {
                j = j + 1;
                char temp = c[j];
                c[j] = c[i];
                c[i] = temp;
            }
            i =  i+ 1;
        }

        System.out.println(Arrays.toString(c));
    }
}
