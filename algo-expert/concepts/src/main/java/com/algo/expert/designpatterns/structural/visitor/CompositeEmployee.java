package com.algo.expert.designpatterns.structural.visitor;

import java.util.ArrayList;
import java.util.List;

public class CompositeEmployee implements Employee {

    private final String name;
    private final String dept;

    private final int yearsOfExperiance;
    private final List<Employee> controls;

    public CompositeEmployee(String name, String dept, int yearsOfExperiance) {
        this.name = name;
        this.dept = dept;
        this.yearsOfExperiance = yearsOfExperiance;
        this.controls = new ArrayList<>();
    }

    public void addEmployee(Employee e) {
        this.controls.add(e);
    }

    public void removeEmployee(Employee e) {
        this.controls.remove(e);
    }

    public String getName() {
        return name;
    }

    public String getDept() {
        return dept;
    }

    public int getYearsOfExperiance() {
        return yearsOfExperiance;
    }

    public List<Employee> getControls() {
        return controls;
    }

    @Override
    public void printStructures() {
        System.out.println(getName() + " works in " + getDept() + " experiance " + getYearsOfExperiance() + " years ");
        for (Employee e: this.controls) {
            e.printStructures();
        }
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitTheElement(this);
    }
}
