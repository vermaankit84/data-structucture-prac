package com.algo.expert.easy.goldman;

public class SearchSortedArray {
    public static void main(String[] args) {
        int []arr = {5, 6, 1, 2, 3, 4};
        System.out.println(1 == search(arr));

        arr = new int[]{1, 2, 3, 4};
        System.out.println(1 == search(arr));

        arr = new int[]{2, 1};
        System.out.println(1 == search(arr));
    }

    private static int search(int []arr) {

        int start = 0;
        int end = arr.length - 1;

        while (start < end) {
            int mid = start + (end - start)/2;
            int element = arr[mid];
            if (element == arr[end]) end = end - 1;
            else if (element > arr[end]) start = mid + 1;
            else end = mid;
        }

        return arr[end];
    }
}
