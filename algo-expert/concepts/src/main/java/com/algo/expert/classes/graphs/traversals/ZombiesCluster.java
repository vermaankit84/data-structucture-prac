package com.algo.expert.classes.graphs.traversals;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ZombiesCluster {
    public static void main(String[] args) {
        List<String> zombies = new ArrayList<>();
        zombies.add("1100");
        zombies.add("1110");
        zombies.add("0110");
        zombies.add("0001");
        System.out.println(zombieCluster(zombies));
    }

    public static int zombieCluster(List<String> zombies) {
        boolean [] visisted = new boolean[zombies.size()];
        int cluster = 0;
        for (int i = 0; i < zombies.size(); i++) {
            if (!visisted[i]) {
                cluster = cluster + 1;
                bfs(i, zombies, visisted);
            }
        }

        return cluster;
    }

    private static void bfs(int vertex, List<String> zombies, boolean[] visisted) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(vertex);
        visisted[vertex] = true;

        while (!queue.isEmpty()) {
            int node = queue.poll();
            for (int i = 0; i < zombies.get(node).length(); i++) {
                if (node == i) {
                    continue;
                }

                if (zombies.get(node).charAt(i) == '1' && !visisted[i]) {
                    visisted[i] = true;
                    queue.add(i);
                }
            }
        }
    }
}
