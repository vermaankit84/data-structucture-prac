package com.algo.expert.designpatterns.behavioral.startegy;

public interface TransportMedium {
    void transport();
}
