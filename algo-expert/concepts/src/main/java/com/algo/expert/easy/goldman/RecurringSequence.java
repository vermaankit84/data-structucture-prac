package com.algo.expert.easy.goldman;

import java.util.*;

public class RecurringSequence {
    public static void main(String[] args) {
        int n1 = 17;
        int n2 = 13;


        System.out.println(fractionToDecimal(n1, n2));
    }


    public static String fractionToDecimal(int nmr, int deno) {
        StringBuilder res = new StringBuilder();
        Map<Integer, Integer> map = new HashMap<>();
        int rem = nmr%deno;

        while (rem!=0 && !map.containsKey(rem)) {
            map.put(rem, res.length());
            rem = rem * 10;
            int residual = rem/deno;
            res.append(residual);
            rem = rem%deno;
        }

        System.out.println(rem + "  " + map + " " + res);

        if (rem == 0) return "";
        if (map.containsKey(rem)) {
            return res.substring(map.get(rem));
        }

        return "";
    }
}
