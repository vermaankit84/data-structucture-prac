package com.algo.expert.hard;

import java.util.*;

/**
 * [
 *   {
 *     "gym": false,
 *     "school": true,
 *     "store": false
 *   },
 *   {
 *     "gym": true,
 *     "school": false,
 *     "store": false
 *   },
 *   {
 *     "gym": true,
 *     "school": true,
 *     "store": false
 *   },
 *   {
 *     "gym": false,
 *     "school": true,
 *     "store": false
 *   },
 *   {
 *     "gym": false,
 *     "school": true,
 *     "store": true
 *   }
 * ]
 */

/*
["gym", "school", "store"]
 */
public class ApartmentHunting {
    public static void main(String[] args) {
        List<Map<String, Boolean>> blocks = new ArrayList<>();

        Map<String, Boolean> m1 = new HashMap<>();
        m1.put("gym", false);
        m1.put("school", true);
        m1.put("store", false);

        Map<String, Boolean> m2 = new HashMap<>();
        m2.put("gym", true);
        m2.put("school", false);
        m2.put("store", false);

        Map<String, Boolean> m3 = new HashMap<>();
        m3.put("gym", true);
        m3.put("school", true);
        m3.put("store", false);

        Map<String, Boolean> m4 = new HashMap<>();
        m4.put("gym", false);
        m4.put("school", true);
        m4.put("store", false);

        Map<String, Boolean> m5 = new HashMap<>();
        m5.put("gym", false);
        m5.put("school", true);
        m5.put("store", true);

        blocks.add(m1);
        blocks.add(m2);
        blocks.add(m3);
        blocks.add(m4);
        blocks.add(m5);

        String[] reqs = new String[3];
        reqs[0] = "gym";
        reqs[1] = "school";
        reqs[2] = "store";

        System.out.println(apartmentHunting(blocks, reqs));
    }

    public static int apartmentHunting(List<Map<String, Boolean>> blocks, String[] reqs) {
        int[] maxDistance = new int[blocks.size()];
        Arrays.fill(maxDistance, Integer.MIN_VALUE);

        for (int i = 0; i < blocks.size(); i++) {
            for (String r: reqs) {
                int closestDistance = Integer.MAX_VALUE;
                for (int j = 0; j < blocks.size(); j++) {
                    if (blocks.get(j).get(r)) {
                        closestDistance = Math.min(closestDistance, distanceBetween(i,j));
                    }
                }

                maxDistance[i] = Math.max(maxDistance[i], closestDistance);
            }
        }
        return getIdxAtMinValue(maxDistance);
    }

    private static int getIdxAtMinValue(int[] maxDistance) {
        int idxAtMinValue = 0;
        int minValue = Integer.MAX_VALUE;
        for (int i = 0; i < maxDistance.length; i++) {
            int currentValue = maxDistance[i];
            if (currentValue < minValue) {
                minValue = currentValue;
                idxAtMinValue = i;
            }
        }

        return idxAtMinValue;
    }

    private static int distanceBetween(int i, int j) {
        return Math.abs(i - j);
    }


}
