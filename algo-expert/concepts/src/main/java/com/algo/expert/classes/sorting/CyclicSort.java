package com.algo.expert.classes.sorting;

import java.util.Arrays;

public class CyclicSort {
    public static void main(String[] args) {
        int nums[] = {4, 1, 2, 2};
        sort(nums);
        System.out.println(Arrays.toString(nums));


    }

    // [ 1, 2, 3, 4, 5 ]
    private static void sort(int[] nums) {
        for (int idx = 0; idx < nums.length; idx++) {
            int curr = nums[idx];
            if (curr < nums.length) {
                if (curr == idx + 1) {
                    continue;
                }

                int temp = nums[curr - 1];
                nums[curr - 1] = curr;
                nums[idx] = temp;
            } else {
                int temp = nums[nums.length - 1];
                nums[nums.length - 1] = curr;
                nums[idx] = temp;
            }
        }
    }


}
