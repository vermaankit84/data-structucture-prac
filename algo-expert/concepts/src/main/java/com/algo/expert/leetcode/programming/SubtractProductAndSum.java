package com.algo.expert.leetcode.programming;

public class SubtractProductAndSum {
    public static void main(String[] args) {
        System.out.println(subtractProductAndSum(234));
        System.out.println(subtractProductAndSum(152));
    }

    public static int subtractProductAndSum(int n) {
        int product = 1;
        int sum  = 0;

        while (n > 0) {
            int number  = n%10;
            product = product * number;
            sum = sum + number;
            n = n/10;
        }

        return (product - sum);
    }
}
