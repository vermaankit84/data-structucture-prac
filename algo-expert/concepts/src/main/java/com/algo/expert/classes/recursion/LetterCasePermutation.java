package com.algo.expert.classes.recursion;

import java.util.*;

public class LetterCasePermutation {
    public static void main(String[] args) {
        List<String> result = new ArrayList<>();
        letterCasePermuation(0, "a1z", "", result);
        System.out.println(result);
    }
    // ø(2^n * n);
    private static void letterCasePermuation(int index, String str, String partial,  List<String> result) {
        // Base Case
        if (index == str.length()) {
            result.add(partial);
            return;
        }
        char charAtIdx = str.charAt(index);
        if (Character.isDigit(charAtIdx)) {
            letterCasePermuation(index + 1, str, partial + str.charAt(index), result);
        } else {
            letterCasePermuation(index + 1, str, partial + String.valueOf(str.charAt(index)).toUpperCase(), result);
            letterCasePermuation(index + 1, str, partial + String.valueOf(str.charAt(index)).toLowerCase(), result);

        }
    }
}
