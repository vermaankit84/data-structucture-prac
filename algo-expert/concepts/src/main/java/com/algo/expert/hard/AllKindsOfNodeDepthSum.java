package com.algo.expert.hard;

import java.util.Stack;

public class AllKindsOfNodeDepthSum {
    public static void main(String[] args) {

    }

    public static int allKindsOfNodeDepths(BinaryTree root) {
        int sumOfAllDepths = 0;
        Stack<BinaryTree> stack = new Stack<>();
        stack.add(root);
        while (!stack.isEmpty()) {
            BinaryTree tree = stack.remove(stack.size() - 1);
            if (tree == null) continue;
            sumOfAllDepths = sumOfAllDepths + nodeDepth(tree, 0);
            stack.add(tree.left);
            stack.add(tree.right);
        }
        return sumOfAllDepths;
    }

    public static int nodeDepth(BinaryTree tree, int depth) {
        if (tree == null) return 0;

        return depth + nodeDepth(tree.left, depth + 1) + nodeDepth(tree.right, depth + 1);
    }

    static class BinaryTree {
        int value;
        BinaryTree left;
        BinaryTree right;

        public BinaryTree(int value) {
            this.value = value;
            left = null;
            right = null;
        }
    }
}
