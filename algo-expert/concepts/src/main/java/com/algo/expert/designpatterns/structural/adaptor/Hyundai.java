package com.algo.expert.designpatterns.structural.adaptor;

public class Hyundai implements Movable {

    MovableAdaptor adaptor = new MovableAdaptorImpl(new Bugatti());
    @Override
    public double getSpeed() {
        return adaptor.getSpeed();
    }

    public static void main(String[] args) {
        Movable m = new Hyundai();
        System.out.println(m.getSpeed());
    }
}


