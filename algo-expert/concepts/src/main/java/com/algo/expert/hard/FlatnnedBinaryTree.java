package com.algo.expert.hard;

import java.util.ArrayList;
import java.util.List;

public class FlatnnedBinaryTree {
    public static void main(String[] args) {

    }

    public static BinaryTree flattenBinaryTree(BinaryTree root) {
        List<BinaryTree> inOrderList = getInOrder(root, new ArrayList<>());
        for (int i = 0; i < inOrderList.size() -1; i++) {
            BinaryTree leftNode = inOrderList.get(i);
            BinaryTree rigtNode = inOrderList.get(i + 1);
            leftNode.right = rigtNode;
            rigtNode.left = leftNode;
        }
        return inOrderList.get(0);
    }

    private static List<BinaryTree> getInOrder(BinaryTree tree, List<BinaryTree> binaryTrees) {
        if (tree!=null) {
            getInOrder(tree.left, binaryTrees);
            binaryTrees.add(tree);
            getInOrder(tree.right, binaryTrees);
        }

        return binaryTrees;
    }

    // This is the class of the input root. Do not edit it.
    static class BinaryTree {
        int value;
        BinaryTree left = null;
        BinaryTree right = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
