package com.algo.expert.classes.graphs.traversals;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class KnightsMove {
    public static void main(String[] args) {

    }

    static int find_minimum_number_of_moves(int rows, int cols, int start_row, int start_col, int end_row, int end_col) {
        int[][] moves = {{2,1}, {2,-1}, {-2,1}, {-2,-1}, {1,2},{1,-2}, {-1,2}, {-1,-2}};
        boolean[][] visited = new boolean[rows][cols];

        Queue<int []> queue = new LinkedList<>();
        int [] cur_pos = new int[] {start_row, start_col};
        queue.add(cur_pos);
        int minimum_moves = 0;
        visited[cur_pos[0]][cur_pos[1]] = true;

        while (!queue.isEmpty()) {
            int batchSize = queue.size();
            for (int i = 0 ; i < batchSize; i = i + 1) {
                cur_pos = queue.remove();
                if (cur_pos[0] == end_row && cur_pos[1] == end_col) {
                    return minimum_moves;
                }

                for (int [] nextMoves : moves) {
                    int new_row = cur_pos[0] + nextMoves[0];
                    int new_col = cur_pos[1] + nextMoves[1];
                    if (new_row < rows && new_col < cols && new_row >= 0 && new_col >= 0) {
                        if (!visited[new_row][new_col]) {
                            int [] nextPos = new int[] {new_row, new_col};
                            queue.add(nextPos);
                            visited[new_row][new_col] = true;
                        }
                    }
                }
            }
            minimum_moves = minimum_moves + 1;
        }
        return -1;
    }

}
