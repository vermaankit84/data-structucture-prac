package com.algo.expert.easy.goldman;

public class SnowPack {
    public static void main(String[] args) {
        int arr[]   = {3, 0, 2, 0, 4};
        int output = 7;
        System.out.println(tappingSnow(arr) == output);
        arr = new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        output = 6;
        System.out.println(tappingSnow(arr) == output);
    }

    private static int tappingSnow(int[] arr) {
        int sum = 0;
        int[] max_left = new int[arr.length];
        max_left[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            max_left[i] = Math.max(max_left[i - 1], arr[i]);
        }

        int[] max_right = new int[arr.length];
        max_right[arr.length - 1] = arr[arr.length - 1];
        for (int i = arr.length - 2; i > -1; i = i - 1) {
            max_right[i] = Math.max(max_right[i + 1], arr[i]);
        }


        for (int i = 0; i < arr.length; i++) {
            sum = sum + Math.min(max_left[i], max_right[i]) - arr[i];
        }
        return sum;
    }

    private static int findMinimum(int idx, int[] arr) {
        int max_right = arr[idx];
        for (int i = idx + 1; i < arr.length; i++) {
            max_right = Math.max(max_right, arr[i]);
        }

        int max_left = arr[idx];
        for (int i = idx - 1; i > -1; i--) {
            max_left = Math.max(max_left, arr[i]);
        }

        return Math.min(max_left, max_right);
    }
}
