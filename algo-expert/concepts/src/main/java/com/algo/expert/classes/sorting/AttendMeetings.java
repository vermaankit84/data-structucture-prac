package com.algo.expert.classes.sorting;

import com.google.common.collect.Lists;

import java.util.*;

public class AttendMeetings {
    public static void main(String[] args) {

        List<Integer> m1 = Lists.newArrayList(1, 5);
        List<Integer> m2 = Lists.newArrayList(4, 8);
        //List<Integer> m3 = Lists.newArrayList(10, 15);
        List<List<Integer>> meetingsList = Lists.newArrayList(m1, m2);

        System.out.println(can_attend_all_meetings(meetingsList));

    }

    public static int can_attend_all_meetings(List<List<Integer>> intervals) {
        List<Pair> meetings = new ArrayList<>();

        intervals.forEach(timings -> {
           int start =  timings.get(0);
           int end =  timings.get(1);

           Pair p = new Pair();
           p.start = start;
           p.end = end;
           meetings.add(p);
        });

        meetings.sort(Comparator.comparingInt(o -> o.start));
        for (int i = 1; i < meetings.size(); i++) {
            Pair previous = meetings.get(i - 1);
            Pair current = meetings.get(i);
            if (previous.end > current.start) {
                return 0;
            }
        }

        return 1;
    }

    private static class Pair {
        int start;
        int end;
    }
}
