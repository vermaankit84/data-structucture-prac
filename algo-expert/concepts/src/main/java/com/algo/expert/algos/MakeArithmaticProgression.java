package com.algo.expert.algos;

import java.util.Arrays;

public class MakeArithmaticProgression {
    public static void main(String[] args) {
        int [] nums = {3,5,1};
        System.out.println(canMakeArithmeticProgression(nums));
    }

    public static boolean canMakeArithmeticProgression(int[] arr) {
        if (arr.length < 2) {
            return true;
        }

        Arrays.sort(arr);
        int r_1 = arr[0];
        int r_2 = arr[1];
        int diff = arr[1] - arr[0];

        for(int i = 2; i < arr.length; i = i + 1) {
            if ((arr[i] - arr[i - 1]) == diff) {
                continue;
            }
            return false;
        }

        return true;
    }
}
