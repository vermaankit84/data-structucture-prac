package com.algo.expert.dp;

public class MaximizeExpression {
    public static void main(String[] args) {

    }

    public int maximizeExpression(int[] array) {
        if (array.length < 4) return 0;

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                for (int k = j + 1; k < array.length; k++) {
                    for (int l = k + 1; l < array.length; l++) {
                        int value = array[i] - (array[j]) + array[k] - (array[l]);
                        max = Math.max(value, max);
                    }
                }
            }
        }

        return max;
    }
}
