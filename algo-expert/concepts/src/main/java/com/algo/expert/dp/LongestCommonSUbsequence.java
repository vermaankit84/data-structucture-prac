package com.algo.expert.dp;

import java.util.*;

public class LongestCommonSUbsequence {
    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("ZXVVYZW", "XKYKZPW"));
    }

    public static List<Character> longestCommonSubsequence(String str1, String str2) {
        int[][] dp = new int[str2.length() + 1][str1.length() + 1];

        for (int i = 1; i < str2.length() + 1; i++) {
            for (int j = 1; j < str1.length() + 1; j++) {
                if (str2.charAt(i - 1) == str1.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        for (int[] ints : dp) {
            System.out.println(Arrays.toString(ints));
        }
        return buildSequence(dp, str1);
    }

    private static List<Character> buildSequence(int[][] dp, String str1) {
        List<Character> sq = new ArrayList<>();
        int i = dp.length - 1;
        int j = dp[0].length - 1;

        while (i!= 0 && j!=0) {
            if (dp[i][j] == dp[i - 1][j]) {
                i -= 1;
            } else if (dp[i][j] == dp[i][j - 1]) {
                j -= 1;
            } else {
                sq.add(0, str1.charAt( j - 1));
                i = i - 1;
                j = j - 1;
            }
        }


        return sq;
    }
}
