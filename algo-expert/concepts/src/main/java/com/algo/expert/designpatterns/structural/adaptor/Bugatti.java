package com.algo.expert.designpatterns.structural.adaptor;

public class Bugatti implements Movable {
    @Override
    public double getSpeed() {
        return 133d;
    }
}
