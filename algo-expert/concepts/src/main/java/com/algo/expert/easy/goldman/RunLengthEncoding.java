package com.algo.expert.easy.goldman;

public class RunLengthEncoding {
    public static void main(String[] args) {
        System.out.println(runLengthEncoding("aaabbbc"));
        System.out.println(runLengthEncoding("aaaa"));
        System.out.println(runLengthEncoding("a"));
    }

    public static String runLengthEncoding(String string) {
        StringBuilder builder = new StringBuilder();
        int length = 1;
        for (int i = 1; i < string.length(); i++) {
            char current = string.charAt(i);
            char previous = string.charAt(i - 1);
            if ((current!=previous) || length == 9) {
                builder.append(previous);
                builder.append(length);
                length = 0;
            }
            length += 1;
        }

        builder.append(string.charAt(string.length() - 1));
        builder.append(length);
        return builder.toString();
    }
}
