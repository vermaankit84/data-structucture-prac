package com.algo.expert.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntersectionTwoArrays {
    public static void main(String[] args) {
        int [] nums1 = {4, 9,5};
        int [] nums2 = {9,4,9,8 ,4};

        System.out.println(Arrays.toString(intersect(nums1, nums2)));
    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0, j = 0, index = 0;
        while(i < nums1.length && j < nums2.length){
            if(nums1[i] == nums2[j]){
                nums1[index++] = nums2[j];
                i++;
                j++;
            } else if(nums1[i] < nums2[j]){
                i++;
            } else{
                j++;
            }
        }
        return Arrays.copyOf(nums1, index);
    }
}
