package com.algo.expert.hardest;

public class MaxProduct {
    public static void main(String[] args) {
        System.out.println(maxProd(10));
    }

    public static int maxProd(int n)
    {
        if (n == 0 || n == 1) return 0;

        int max_val = 0;
        for (int i = 1; i < n; i++)
            max_val = Math.max(max_val,
                    Math.max(i * (n - i),
                            maxProd(n - i) * i));

        // Return the maximum of all values
        return max_val;
    }
}
