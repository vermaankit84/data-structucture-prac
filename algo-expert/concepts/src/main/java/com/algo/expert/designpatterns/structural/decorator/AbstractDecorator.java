package com.algo.expert.designpatterns.structural.decorator;

public abstract class AbstractDecorator extends Component {
    private Component component;

    public void setComponent(Component component) {
        this.component = component;
    }

    public void makeHouse() {
        if(component!=null) {
            component.makeHouse();;
        }
    }
}
