package com.algo.expert.dp;
/*
"psnw"
"vozsh"
 */
public class IsSubsequence {
    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("ace", "abcdef"));
        System.out.println(longestCommonSubsequence("abc", "abc"));
        System.out.println(longestCommonSubsequence("aec", "abcdef"));
        System.out.println(longestCommonSubsequence("psnw", "vozsh"));
    }

    public static int longestCommonSubsequence(String text1, String text2) {

        int dp[][] = new int[text1.length() + 1][text2.length() + 1];

        dp[0][0] = 0;

        int result = 0;
        for (int i = 1; i < text1.length() + 1; i++) {
            for (int j = 1; j < text2.length(); j++) {
                if (text1.charAt( i - 1) == text2.charAt( j - 1)) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }

                result = Math.max(result, dp[i][j]);
            }
        }

        return result;
    }
}
