package com.algo.expert.recusion;

public class MaximumProductCutting {
    public static void main(String[] args) {
        System.out.println(maxProd_DP(10));
    }

    public static int maxProd(int n) {
        if (n == 0 || n == 1) return 0;

        int max_val = 0;

        for (int i = 1; i < n; i++) {
            int temp =  Math.max(i * (n - i),  maxProd(n - i) * i);
            max_val =  Math.max(max_val, temp);
        }

        return max_val;
    }


    public static  int maxProd_DP(int n) {
        if (n == 2) return 1;
        int [] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (j >= i) {
                    dp[j] = Math.max(dp[j], dp[j - i] * i);
                }
            }
        }

        return dp[n];
    }
}
