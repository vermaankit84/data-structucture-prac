package com.algo.expert.hard;

import com.google.common.collect.Lists;

import java.util.*;

public class ValidateTheeNodes {
    public static void main(String[] args) {


    }

    public boolean validateThreeNodes(BST nodeOne, BST nodeTwo, BST nodeThree) {
        if (isDesendent(nodeOne, nodeTwo)) {
            return isDesendent(nodeThree, nodeTwo);
        }

        if (isDesendent(nodeTwo, nodeThree)) {
            return isDesendent(nodeOne, nodeTwo);
        }

        return false;
    }


    public boolean isDesendent(BST nodeOne, BST target) {
        while (nodeOne!=null && nodeOne!=target) {
            nodeOne = (target.value < nodeOne.value) ? nodeOne.left : nodeOne.right;
        }

        return nodeOne == target;
    }



    @Deprecated
    public boolean validateThreeNodes_deprecated(BST nodeOne, BST nodeTwo, BST nodeThree) {
        List<Integer> l1 = Lists.newArrayList(1);
        List<Integer> l2 = Lists.newArrayList(1, 2);
        List<Integer> l3 = Lists.newArrayList(1, 2, 3);

        int min_size = Math.min(l1.size() , Math.min(l2.size(), l3.size()));

        if (l1.size() == min_size) {
            for (int element : l1) {
                if (!l2.contains(element)) return false;
                if (!l3.contains(element)) return false;
            }
            return true;
        }
        if (l2.size() == min_size) {
            for (int element : l2) {
                if (!l1.contains(element)) return false;
                if (!l3.contains(element)) return false;
            }
            return true;

        }
        for (int element : l3) {
            if (!l2.contains(element)) return false;
            if (!l3.contains(element)) return false;
        }
        return true;

    }

    static class BST {
        public int value;
        public BST left = null;
        public BST right = null;

        public BST(int value) {
            this.value = value;
        }
    }



}
